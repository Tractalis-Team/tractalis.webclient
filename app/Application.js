/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('TractalisWebclient.Application', {
	extend: 'Ext.app.Application',

	name: 'TractalisWebclient',

	requires: [
		// because extjs 6.2 needs it on safari for whatever reason...
		// desptite the fact that we nowhere use it... 
		// and cannot figure it out by its own... pure drama
		'Ext.layout.component.ProgressBar',
		'TractalisWebclient.helper.ConfigurationHelper',
		'TractalisWebclient.helper.AzureCertificateHelper',
		'TractalisWebclient.helper.UnitHelper',
		'TractalisWebclient.backend.PositionReceiver',
		'TractalisWebclient.backend.PositionHistoryReceiver',
		'TractalisWebclient.backend.AzureTablesProxy',
		'TractalisWebclient.helper.AnalyticsHelper',
		'TractalisWebclient.helper.StoreLoadingBarier',
		'TractalisWebclient.helper.TeamFollower',
		'TractalisWebclient.helper.NetworkLoadingIndicator',

		'TractalisWebclient.helper.FilterHelper',
		'TractalisWebclient.helper.teamfilters.BaseFilter',
		'TractalisWebclient.helper.teamfilters.StoreFilter',
		'TractalisWebclient.helper.teamfilters.FulltextFilter',
		'TractalisWebclient.helper.teamfilters.Top5Filter',
		'TractalisWebclient.helper.teamfilters.CategoryFilter',
		'TractalisWebclient.helper.teamfilters.TeamEnabledFilter',
		'TractalisWebclient.helper.teamfilters.SoleTeamFilter',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.BaseLayersCreator',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.FixPointsCreator',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.PositionsLayerController',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.ExternalLayersCreator',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.YouAreHereLayer',

		'TractalisWebclient.view.main.components.socialmedia.facebook.ApiResolver'
	],

	stores: [
		'Teams',
		'Positions',
		'FixPoints',
		'Participants',
		'TeamCategories',
		'PositionHistories'
	],

	views: [
		'main.components.headerbar.HeaderBar',
		'main.components.mapview.leaflet.MapContainer',
		'main.components.chart.ElevationChart',
		'main.components.headerbar.MetroNavigationContainer',
		'main.components.metrobutton.MetroButton',
		'main.components.teams.list.TeamList',
		'main.components.teams.filter.TeamFilter',
		'main.components.teams.filter.categories.CategoryFilter',
		'main.components.teams.details.TeamDetails',
		'main.components.teams.details.members.TeamMembers',
		'main.components.sponsors.SponsorView',
		'main.components.socialmedia.SocialMediaPanel',
		'main.components.socialmedia.twitter.TwitterPanel',
		'main.components.socialmedia.facebook.FacebookPanel',
		'main.components.socialmedia.facebook.LikeButtons'
	],

	/**
	 * Is called when the application launches
	 */
	launch: function() {
		var me = this,
			positionsStore = Ext.getStore('Positions'),
			positionHistoriesStore = Ext.getStore('PositionHistories'),
			single = { single: true };

		StoreLoadingBarier.attachAllStores();

		PositionReceiver.setStore(positionsStore);

		PositionHistoryReceiver.setStore(positionHistoriesStore);
		PositionHistoryReceiver.setUpdateInterval(Configuration.get('webservice/interval'));

		// 1st load is automatic
		positionHistoriesStore.on('load', function(){
			PositionHistoryReceiver.startFetching();
		}, me, single);

		positionsStore.on('load', function(store, records) {
			PositionReceiver.setUpdateInterval(Configuration.get('webservice/interval'));

			PositionReceiver.copyRanks(records);
			PositionReceiver.startFetching();
		}, me, single);

		// wait for all stores to load and then start loading filters
		Ext.GlobalEvents.on('allStoresLoad', function() {
			FilterHelper.loadFilters();
		}, me, {
			single: true
		});
	}
});
