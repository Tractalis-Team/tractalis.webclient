Ext.define('TractalisWebclient.view.main.components.teams.details.TeamDetailsModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.team-details',

	data: {
		team: null,
		teamPosition: null
	},

	formulas: {
		/**
		 * This formula retrieves a proper renderable caption for the team ranking.
		 * This means that zero-values are mapped to emptychars.
		 *
		 * @param  {Function} get The function which is used to retrieve viewmodel-values
		 * @return {String} The actual value which can be rendered
		 */
		teamRankCaption: function(get) {
			var rank = get('team.Rank');

			// TODO: what the heck is this magic number?
			return rank === Number.MAX_VALUE || !Configuration.get('race/includeRanks') ? '-' : rank;
		},

		teamRawDistance: function(get){
			return this.encodeKilometers(get('team.OdoMileage'));
		},

		teamDistance: function(get){
			return this.encodeKilometers(get('team.Distance'));
		},

		teamDistanceLeft: function(get){
			return this.encodeKilometers(get('team.DistanceLeft'));
		},

		teamAltitude: function(get){
			return this.encodeMeters(get('team.Altitude'));
		},

		teamSpeed: function(get){
			return this.encodeSpeed(get('team.Speed'));
		},

		teamDistanceToNextTs: function(get){
			return this.encodeKilometers(get('team.DistanceToNextTs'));
		},

		teamToNextTeam: function(get){
			return this.encodeKilometers(get('team.ToNextTeam'));
		},

		teamToPrevTeam: function(get){
			return this.encodeKilometers(get('team.ToPrevTeam'));
		},

		teamSwissX: function(get){
			// return Y bc. the librairy calculates wrong values for X
			return WGStoCHy(get('teamPosition.Latitude'), get('teamPosition.Longitude'));
		},

		teamSwissY: function(get){
			// return X bc. the librairy calculates wrong values for Y
			return WGStoCHx(get('teamPosition.Latitude'), get('teamPosition.Longitude'));
		},

		teamGpxTrackLink: function(get) {
			if (!get('hqMode')) {
				return '';
			}

			return Ext.util.Format.format('https://data-access.tractalis.com/api/path/gpx?eventId={0}&teamId={1}&teamNr={2}&_dc={3}', Configuration.get('race/id'), get('team.TeamId'), get('team.StartNumber'), Date.now());
		},

		teamTimeStamp: function(get) {
			var ts = get('team.TimeStamp');

			if (ts) {
				return ts.replace(/T/, " ").replace(/(\.\d{0,9})?Z/, " (UTC)")
			}

			return "";
		}
	},

	/**
	 * Encodes the specified value into kilometers and adds the suffix
	 * @param {Number} value
	 * @returns {String} formatted value
	*/
	encodeKilometers: function(value){
		value = UnitHelper.kilometers(value);

		return Ext.util.Format.format('{0} {1}', Ext.util.Format.number(value, '0.00'), UnitHelper.getKilometerSuffix());
	},

	/**
	 * Encodes the specified value into meters and adds the suffix
	 * @param {Number} value
	 * @returns {String} formatted value
	*/
	encodeMeters: function(value){
		value = UnitHelper.meters(value);

		return Ext.util.Format.format('{0} {1}', Ext.util.Format.number(value, '0.00'), UnitHelper.getMeterSuffix());
	},

	/**
	 * Encodes the specified speed value into km/h or mp/h and adds the suffix
	 * @param {Number} value
	 * @returns {String} formatted value
	*/
	encodeSpeed: function(value){
		value = UnitHelper.kilometers(value);

		return Ext.util.Format.format('{0} {1}', Ext.util.Format.number(value, '0.00'), UnitHelper.getSpeedFormat());
	}
});
