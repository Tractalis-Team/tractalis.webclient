/**
 * This store represents all categories which are used in this race. Keep in mind that this store depends on the TractalisWebclient.store.Teams which fills it with data.
 */
Ext.define('TractalisWebclient.store.TeamCategories', {
	extend: 'Ext.data.Store',

	require: [
		'TractalisWebclient.model.TeamCategory'
	],

	model: 'TractalisWebclient.model.TeamCategory',
	autoLoad: true,
	pageSize: 0,

	sorters: 'SortingPriority',

	proxy: {
		type: 'memory'
	},

	/**
	 * This methods toggles the 'Enabled'-Field of all entries to true / false at once
	 *
	 * @param {Boolean} enabled True if all should be enabled, otherwhise false
	 * @param {Boolean} silent True if no message should be spread around
	 */
	enableAll: function(enabled, silent) {
		var me = this,
			silent = (typeof silent === 'undefined') ? false : silent;

		if (!silent) {
			me.beginUpdate();
		}

		me.each(function(record) {
			// if not set yet, we set it
			if (record.get('Enabled') !== enabled) {
				record.set('Enabled', enabled, {
					silent: true
				});
			}
		}, me);

		if (!silent) {
			me.endUpdate();
		}
	}
});