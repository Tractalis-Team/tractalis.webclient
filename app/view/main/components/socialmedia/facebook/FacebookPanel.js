Ext.define('TractalisWebclient.view.main.components.socialmedia.facebook.FacebookPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.facebook',

	id: 'facebook-panel',
	faulted: false,

	config: {
		facebookUrl: 'https://www.facebook.com/tractalis',

		cls: 'facebook-panel',
		html: '<div class="facebookContainer"></div>'
	},

	tmpl: new Ext.Template([
		'<div class="fb-page"',
		'data-href="{facebookUrl}"',
		'data-tabs="timeline"',
		'data-small-header="true"',
		'data-adapt-container-width="true"',
		'data-hide-cover="false"',
		'data-width="{width}px"',
		'data-height="{height}px"',
		'data-show-facepile="false">',
		'</div>'
	]),

	initComponent: function () {
		var me = this;

		me.callParent(arguments);
		me.on('afterrender', function () {
			FacebookApiResolver.resoveApi()
				.then(me.generateContent.bind(me))
				.catch(me.onError.bind(me));
		});
	},

	onError: function (msg) {
		this.faulted = true;

		this.getEl()
			.selectNode('.facebookContainer')
			.innerHTML = '<div class="load-error"><h3>Whoooops...</h3><p>Error occured while loading Facebook widget</p><p><small>If tracking protection is enabled in your browser, disable it to load social media streams as it blocks them.</small></p></div>';
	},

	generateContent: function (width, height) {
		var me = this,
			container = me.getEl().dom.querySelector('.facebookContainer');

		if (!width) width = me.getEl().getWidth();
		if (!height) height = me.getEl().getHeight();

		container.innerHTML = me.tmpl.apply({
			facebookUrl: me.getFacebookUrl(),
			width: width,
			height: height
		});

		if (window.FB) {
			FB.XFBML.parse(container);
		}
	},

	/**
	 * Handles resize event of the container.
	 *
	 * @param {Number} w new width
	 * @param {Number} h new height
	 * @param {Number} oW old width
	 * @param {Number} oH old height
	 */
	onResize: function (w, h, oW, oH) {
		this.callParent(arguments);

		// thank you facebook for need of recreation of this crap on every resize :P
		// I will quote this bullshit to entertain next generations, so here it goes:
		// The Page plugin works with responsive, fluid and static layouts. You can use media queries or other methods to set the width of the parent element, yet:
		// The plugin will determine its width on page load
		// It will not react changes to the box model after page load.
		// If you want to adjust the plugin's width on window resize, you manually need to rerender the plugin. 
		if(!this.faulted) {
			this.generateContent(w, h);
		}
	}
});