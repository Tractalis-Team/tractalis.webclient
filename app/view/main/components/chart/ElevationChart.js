Ext.define("TractalisWebclient.view.main.components.chart.ElevationChart", {
    extend: "Ext.panel.Panel",
    alias: "widget.elevation-chart",
    id: "elevationChart",
    height: '25%',
    width: '100%',
    split: {
        collapsible: true
    },
    html: '<div id="elevationChartView" class="elevationChartView"><svg></svg></div>',

    listeners: {
        resize: function () {
            var me = this;
            me.chartRef.draw();
        },
        afterRender: function () {
            var me = this,
                bgImage = Configuration.get('elevation/bgImage', ''),
                ecView;
            if (!bgImage) return;

            ecView = me.getEl().getById('elevationChartView');
            ecView.addCls('with-bg-image');
            ecView.applyStyles('background-image: url(' + bgImage + ');')
        }
    },

    initComponent: function () {
        var me = this,
            useImperialUnits = !Configuration.get('race/isMetric', true),
            scaleDistanceTo = Configuration.get('elevation/scaleDistanceTo', null),
            bgImage = Configuration.get('elevation/bgImage', '');

        me.initConfig();
        me.callParent(arguments);
        me.chartRef.ctx.parentElId = 'elevationChartView';
        me.chartRef.ctx.distanceInMiles = useImperialUnits;
        me.chartRef.ctx.elevationInFeet = useImperialUnits;
        me.chartRef.ctx.chartOpacity = bgImage ? 0.85 : 1; // hack but it looks better :P

        if (scaleDistanceTo) {
            me.chartRef.ctx.scaleDistanceTo = scaleDistanceTo
        }

        Ext.QuickTips.disable();

        Ext.Ajax.request({
            url: Configuration.get('gis/layers')[0].url,
            dataType: 'application/xml',
            success: function (rsp) {
                me.chartRef.ctx.kml = rsp.responseXML ? rsp.responseXML : me.readXml(rsp.responseText);
                me.chartRef.draw();
                Ext.getStore('Positions').on('load', me.positionsUpdated, me);
            },
            failure: function (rsp) {
                console.log('server-side failure with status code ' + rsp.status);
            },
            disableCaching: false
        });

        me.chartRef.ctx.mousedownCb = function (e) {
            me.fireEvent('elevationChartMouseDown', e);
        };

        me.chartRef.ctx.mousemoveCb = function (e) {
            me.fireEvent('elevationChartMouseMove', e);
        };

        me.chartRef.ctx.mouseleaveCb = function (e) {
            me.fireEvent('elevationChartMouseLeave', e);
        };
    },

    readXml: function (xml) {
        var doc;

        if (window.DOMParser) {
            doc = (new DOMParser()).parseFromString(xml, "text/xml");
        } else if (window.ActiveXObject) {
            doc = new ActiveXObject("Microsoft.XMLDOM");
            doc.loadXML(xml);
        }

        return doc;
    },

    // TODO: move to controller
    positionsUpdated: function (store, records, successful) {
        var me = this,
            positions = [],
            i, len, rec;

        for (i = 0, len = records.length; i < len; i++) {
            rec = records[i];

            if (rec.isActive() && !me.isFilteredOut(rec.get('TeamId'))) {
                positions.push({
                    id: rec.get('TeamId'),
                    dist: rec.get('Distance'),
                    lng: rec.get('Longitude'),
                    lat: rec.get('Latitude'),
                    alt: rec.get('Altitude')
                });
            }
        }

        me.chartRef.ctx.participantsPositions = positions;
        me.chartRef.updatePositions();
    },

    // TODO: this one was copy-pased from PositionsLayerController
    isFilteredOut: function (teamId) {
        var teams = Ext.getStore('Teams');

        // store is filtered, findExact cannot find it as it searches through
        // filtered data but team can retrieved byId as it searches through entire data set
        return (teams.isFiltered() && teams.findExact('TeamId', teamId) === -1 && teams.getById(teamId) !== null);
    },

    chartRef: ElevationChart
});