/**
 * This Model represents Fix Points like Timestations etc. on the Map.
 */
Ext.define('TractalisWebclient.model.FixPoint', {
	extend: 'Ext.data.Model',

	idProperty: 'Id',

	fields: [{
		name: 'Id',
		type: 'auto'
	}, {
		name: 'Longitude',
		type: 'number',
		mapping: 'Lon'
	}, {
		name: 'Latitude',
		type: 'number',
		mapping: 'Lat'
	}, {
		name: 'Altitude',
		type: 'number',
		mapping: 'Alt'
	}, {
		name: 'Name',
		type: 'string'
	}, {
		name: 'TypeName',
		type: 'string'
	}, {
		name: 'Type',
		type: 'number'
	}, {
		name: 'ImageUrl',
		type: 'string',
		defaultValue: null
	}, {
		name: 'TargetUrl',
		type: 'string',
		defaultValue: null
	} ]
});