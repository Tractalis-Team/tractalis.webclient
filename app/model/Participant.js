/**
 * This Model represents single Teams which are rendered in the Tree.
 */
Ext.define('TractalisWebclient.model.Participant', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'Name', type: 'auto' },
        { name: 'TeamName', type: 'auto' },
        { name: 'TeamId', reference: 'Teams' },

        // Example data could be '1980-01-01T00:00:00Z' which is basically
        // the azure format which is the ISO 8601 Dateformat
        { name: 'Birthday', type: 'date', dateFormat: 'c' },
        { name: 'Age', type: 'int' }
    ]
});
