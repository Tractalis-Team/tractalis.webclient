/**
 * Viewcontroller for {TractalisWebclient.view.main.components.headerbar.HeaderBar}
 */
Ext.define('TractalisWebclient.view.main.components.headerbar.HeaderBarController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.main-components-headerbar',

	requires: [
		'TractalisWebclient.helper.NetworkLoadingIndicator'
	],

	/**
	 * Initializes the View
	 */
	init: function () {
		var me = this,
			vm = me.getViewModel();

		document.title = 'TRACTALIS - ' + Configuration.get('race/name', 'Live Tracking Solutions');

		vm.set('raceName', Configuration.get('race/name'));
		vm.set('raceLogoUrl', Configuration.get('race/logo'));
		vm.set('raceWebsiteUrl', Configuration.get('race/website', ''));
		vm.set('tractalisLogoUrl', Configuration.get(
			'app/logo',
			'https://tractalisstorage.blob.core.windows.net/events/Resources/Common/html5/Trans-100-256-01-ws-html5.png'
		));

		// start loading indicator stuff
		me.initializeLoadingIndicator(LoadingIndicator);
	},

	/**
	 * Initialize loading indicator
	 * @param loadingIndicator
	 */
	initializeLoadingIndicator: function (loadingIndicator) {
		var me = this;
		var vm = me.getViewModel();

		loadingIndicator.on('start', function () {
			vm.set('isLoading', true);
		}, me);

		loadingIndicator.on('end', function () {
			vm.set('isLoading', false);
		}, me);
	}
});
