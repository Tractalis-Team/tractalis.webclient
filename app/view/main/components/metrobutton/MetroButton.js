/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('TractalisWebclient.view.main.components.metrobutton.MetroButton', {
    extend: 'Ext.button.Button',
    alias: 'widget.metro-button',
    
    config: {
        width: 130,
        height: 130
    }
});
