/**
 * This is the View-Controller which is directly coupled to the {TractalisWebClient.view.main.Main} -View
 */
Ext.define('TractalisWebclient.view.main.MainController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.main',

	control: {
		'main-header-bar': {
			showMenu: 'collapseMenu'
		},
		'L-map-container': {
			teamDetailsRequested: 'showTeamDetails'
		},
		'header-metro-navigation-container': {
			buttonClicked: 'menuButtonClicked'
		},
		'team-list': {
			rowclick: 'teamListRowClicked',
			selectionchange: 'teamListSelectionChanged',
			hideMyPosition: 'hideMyPosition',
			showMyPosition: 'showMyPosition'
		},
		'team-details': {
			showTeamMember: 'teamDetailsMembersClicked',
			hide: 'teamDetailsClosed'
		},
		'elevation-chart': {
			elevationChartMouseDown: 'focusMapOn',
			elevationChartMouseMove: 'showGhostOnMap',
			elevationChartMouseLeave: 'hideGhost'
		}
	},

	config: {
		teamMembers: null
	},

	mapContainer: null,

	/**
	 * Initializes the ViewController
	 */
	init: function() {
		var me = this,
			hqRte = Configuration.get('race/hqDecoration');

		if(hqRte) {
			Ext.app.route.Router.connect(hqRte, {action: 'performOfficialsLoginDirect'}, me);
			Ext.app.route.Router.connect("", {action: 'clearOfficialsLogin'}, me);
		}

		Ext.GlobalEvents.on('hideMyPosition', this.hideMyPosition, this);
		Ext.GlobalEvents.on('showMyPosition', this.showMyPosition, this);
		Ext.GlobalEvents.on('gelocation-unavailable', () => {
			me.getViewModel().set('geolocationAvailable', false),
			me.getViewModel().set('geolocationFailed', true)
		});
		Ext.GlobalEvents.on('gelocation-failed', () => {
			me.getViewModel().set('geolocationFailed', true);
		});

		Ext.GlobalEvents.on('gelocation-failed-reset', () => {
			me.getViewModel().set('geolocationFailed', false);
		});


		TeamFollower.setTeamStore(Ext.getStore('Teams'));
		me.mapContainer = Ext.getCmp('mapViewContainer')

		me.showSponsors();
	},


	/**
	 * Shows the metro navigation bar
	 */
	collapseMenu: function() {
		const vm = this.getViewModel()

		vm.set('menuCollapsed', !vm.get('menuCollapsed'));

		setTimeout(function() {
			this.toggleBodyClickHandler(true);
		}, 0);
	},


	/**
	 * Performs login direct without AUTH
	 */
	performOfficialsLoginDirect: function() {
		var teamsStore = Ext.getStore('Teams'),
			proxy = teamsStore.getProxy(),
			initialFilter = proxy.getInitialConfig('filter').replace(/\(/g, '\\(').replace(/\)/g, '\\)'),
			currentFilter = proxy.getFilter();

		// remove all filters from the teams proxy and try to refetch all the data again
		proxy.setFilter(currentFilter.replace(new RegExp(initialFilter + '( and )?', ''), ''));
		teamsStore.load();
		this.getViewModel().set('hqMode', true);
	},

	hideMyPosition: function() {
		this.mapContainer.fireEvent('hideYouAreHere');
	},

	showMyPosition: function(follow) {
		this.mapContainer.fireEvent('showYouAreHere', follow);
	},

	focusMapOn: function(elevationPt) {
		this.mapContainer.fireEvent('showCoords', elevationPt.gps.lng, elevationPt.gps.lat);
	},

	showGhostOnMap: function(elevationPt) {
		this.mapContainer.fireEvent('showGhostOn', elevationPt.gps.lng, elevationPt.gps.lat);
	},

	hideGhost: function(elevationPt) {
		this.mapContainer.fireEvent('hideGhost');
	},

	clearOfficialsLogin: function() {
		var vm = this.getViewModel(),
			teamsStore, proxy, initialFilter, currentFilter;

		if(vm.get('hqMode') === true) {
			teamsStore = Ext.getStore('Teams');
			proxy = teamsStore.getProxy();
			initialFilter = proxy.getInitialConfig('filter');
			currentFilter = proxy.getFilter();

			currentFilter = initialFilter + (!currentFilter ? '' : ' and ' + currentFilter);
			// remove all filters from the teams proxy and try to refetch all the data again
			proxy.setFilter(currentFilter);
			teamsStore.load();
			vm.set('hqMode', false);
		}
	},

	/**
	 * Team List row was clicked, dispatch
	 *
	 * @param {Ext.grid.Panel} grid
	 * @param {TractalisWebclient.model.Team} record
	 */
	teamListRowClicked: function(grid, record) {var teamId = record.get('TeamId');
		var teamId = record.get('TeamId');
		
		Ext.getCmp('mapViewContainer').fireEvent('teamSelected', teamId);
		
		if (!record.get('Enabled')) return;
		this.showTeamDetails(teamId);
	},

	/**
	 * Hides the metro navigation bar menu
	 */
	hideMenu: function() {
		var me = this;

		me.getViewModel().set('menuCollapsed', true);
		me.toggleBodyClickHandler(false);
	},

	/**
	 * Adds or removes the click handler to the body
	 * to remove the metro navigation bar when clicking outside
	 * the panel
	 *
	 * @param {bool} enableClickHandler
	 */
	toggleBodyClickHandler: function(enableClickHandler) {
		var me = this;

		if (enableClickHandler) {
			Ext.getBody().on('click', me.hideMenu, me);
		} else {
			Ext.getBody().un('click', me.hideMenu, me);
		}
	},

	/**
	 * Shows the details for the specific Team

	 * @param {Number} teamId
	 */
	showTeamDetails: function(teamId) {
		var position = Ext.getStore('Positions').getById(teamId),
			team = Ext.getStore('Teams').getById(teamId);

		// the actual team entity can be fetched through the position
		const vm = this.getViewModel();
		vm.set('selectedTeamPosition', position);
		vm.set('selectedTeam', team);

		this.closeTeamMembers();

		// and show the team details panel
		var details = Ext.getCmp('teamDetailsPanel');

		if (details.isHidden()) {
			details.show();
		}
	},

	teamDetailsClosed: function() {
		this.mapContainer.fireEvent('teamSelected', undefined);
		
		const vm = this.getViewModel();
		vm.set('selectedTeamPosition', null);
		vm.set('selectedTeam', null);
	},

	teamListSelectionChanged: function(grid, teams) {
		if(teams && teams.length > 0) {
			const team =  teams[0];
			const teamId =  team.get('TeamId');
			this.mapContainer.fireEvent('teamSelected', teamId);
			if (team.get('Enabled')) {
				this.showTeamDetails(teamId);
			}
		} else {
			var details = Ext.getCmp('teamDetailsPanel');

			if (!details.isHidden()) {
				details.hide();
			}
		}
	},

	/**
	 * Menu button was clicked
	 *
	 * @param {String} identifier the id of the clicked button
	 */
	menuButtonClicked: function(identifier) {
		alert(identifier);
	},

	/**
	 * User has requested to see the members of the specified team
	 *
	 * @param {TractalisWebclient.model.Team} team
	 */
	teamDetailsMembersClicked: function(team) {
		// if opened on a desktop, we add a timeout to destroy it when the user didnt hover for a specific amount of time
		var timeout = null;

		if (!Ext.os.is.Phone) {
			timeout = setTimeout(function() {
				teamMembers.destroy();
			}, 4000);
		}

		var teamMembers = this.createTeamMembers(team);

		teamMembers.getEl().on('mouseenter', function() {
			if (timeout !== null) {
				clearTimeout(timeout);
				timeout = null;
			}
		});

		teamMembers.getEl().on('mouseleave', function() {
			teamMembers.destroy();
		});

		teamMembers.on('destroy', function() {
			Ext.getStore('Participants').clearFilter();
		});
	},

	/**
	 * Checks whether an instance of the team member view is open in the current scope,
	 * if yes, the window is immediately closed.
	 */
	closeTeamMembers: function() {
		if (this.getTeamMembers() !== null) {
			this.getTeamMembers().destroy();
		}
	},

	/**
	 * Open the team members view for the specified team
	 * @param {TractalisWebclient.model.Team} team
	 *
	 * @returns {Ext.panel.Panel} the created members instance
	 */
	createTeamMembers: function(team) {
		var teamDetails = Ext.getCmp('teamDetailsPanel');

		// go through the participants and sort them out
		// come mek we sort dem oooout
		// ju qift bima nanes juve, nejser i kom provimet e matmatikas e dy lidhje skom
		// vend se me msue une qetu po boj qesi sende paldihje haj medet haj
		Ext.getStore('Participants').filterBy(function(record) {
			return record.get('TeamId') === team.get('TeamId');
		});

		// on a phone we position the team members exactly on the beginning
		// over the teamdetails
		var positionOffsetX = Ext.os.is.Phone ? 0 : teamDetails.getWidth();
		var width = Ext.os.is.Phone ? teamDetails.getWidth() : 400;

		var teamMembers = Ext.create({
			xtype: 'team-members',
			team: team,
			height: teamDetails.getHeight(),
			width: width,
			x: teamDetails.getX() + positionOffsetX,
			y: teamDetails.getY()
		});

		this.setTeamMembers(teamMembers);
		this.getTeamMembers().show();

		return this.getTeamMembers();
	},

	/**
	 * Show sponsors views
	 */
	showSponsors: function() {
		var me = this,
			sponsors = Configuration.get('race/sponsors', []).filter(function(sponsor){ return !sponsor.isMain; }),
			width = 114, // magic number which is width of sponsor panel
			defaultX = window.innerWidth - width;

		// when no sponsors were defined, quit
		if (sponsors.length === 0) {
			return;
		}

		var sponsorsWindow = Ext.create({
			xtype: 'sponsor-view',
			width: width,
			height: 'auto',
			x: defaultX,
			y: window.innerHeight / 2,
			sponsors: sponsors,
			positionOver: this.mapContainer
		});

		sponsorsWindow.show();
	}
});
