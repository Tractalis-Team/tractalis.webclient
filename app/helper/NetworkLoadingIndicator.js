/**
 * This helper class provides some functionallities for GAL (Google Analytics) which can be used in the Sencha Application.
 */
Ext.define('TractalisWebclient.helper.NetworkLoadingIndicator', {
	singleton: true,
	alternateClassName: 'LoadingIndicator',

	mixins: {
		observable: 'Ext.util.Observable'
	},

	config: {
		/**
		 * @cfg {Number} counter
		 * Contains the count of current active ajax requests
		 */
		counter: 0,

		/**
		 * @cfg {Timeout} timeout
		 * Timeout handler to fix never-ending loading spins
		 */
		timeout: null
	},

	/**
	 * @private
	 * @returns {undefined}
	 */
	constructor: function () {
		var me = this;

		// initialize observable stuff for this class
		me.mixins.observable.constructor.call(me);

		// create the automatic properties
		me.initConfig(me.config);

		Ext.Ajax.on('beforerequest', me.requestStarting, me);
		Ext.Ajax.on('requestcomplete', me.requestEnding, me);
		Ext.Ajax.on('requestexception', me.requestEnding, me);
	},

	/**
	 * Request is starting
	 */
	requestStarting: function (connection, request) {
		// check for request type
		// if it's not one of the usual suspects, quit counting immediately
		if (typeof request.method === 'undefined') {
			return;
		}

		var me = this;

		// increment loading "stack" counter (like reference counting in Garbage Collectors)
		// i hate to go to school
		me.setCounter(me.getCounter() + 1);

		// start loading
		if (me.getCounter() === 1) {
			// we do not need to fire the startloading each time an ajax request starts
			// only for the first request
			me.fireEvent('start');

			// create a timeout to disable the indicator in each time
			if (me.getTimeout() !== null) {
				me.setTimeout(window.setTimeout(me.cancel, 30000));
			}
		}
	},

	/**
	 * Request is ending
	 */
	requestEnding: function () {
		var me = this;

		me.setCounter(me.getCounter() - 1);

		// make sure that the counter does not slip below 0
		if (me.getCounter() < 0) {
			me.setCounter(0);
		}

		// stop loading
		if (me.getCounter() === 0) {
			// when all requests have finished, we know that loading is done
			// so fuckoff at this particular place
			me.fireEvent('end');
			me.stopTimeout();
		}
	},

	/**
	 * Stops the timeout
	 */
	stopTimeout: function(){
		var me = this;

		if (me.getTimeout() === null) return;

		window.clearTimeout(me.getTimeout());
		me.setTimeout(null);
	},

	/**
	 * Cancels loading indicator in each case
	 */
	cancel: function(){
		var me = this;

		// cancels loading
		me.setCounter(0);
		me.setTimeout(null);

		me.fireEvent('end');
	}
});