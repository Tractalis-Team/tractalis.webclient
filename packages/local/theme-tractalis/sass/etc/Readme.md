# theme-tractalis/sass/etc

This folder contains miscellaneous SASS files. Unlike `"theme-tractalis/sass/etc"`, these files
need to be used explicitly.
