/**
 * This view shows a list of sponsors which are defined in the configuration.
 */
Ext.define("TractalisWebclient.view.main.components.sponsors.SponsorView", {
	extend: "Ext.window.Window",
	alias: 'widget.sponsor-view',

	config: {
		sponsors: [],
		positionOver: Ext.getBody()
	},

	cls: 'sponsor-view',
	title: 'Sponsors',
	closable: false,
	header: false,

	/**
	 * @method initComponent
	 * @inheritdoc
	 * @return {void}
	 */
	initComponent: function() {
		var me = this,
			sponsors = me.getSponsors();

		me.callParent(arguments);
		me.add(me.generateSponsorItems(sponsors));

		me.getPositionOver().on('resize', me.resetPosition, me, { priority: -100 });
	},

	/**
	 * Generates the single sponsor items based on the specified list of sponsors
	 *
	 * @param {Array} sponsors List of sponsors to convert into items
	 */
	generateSponsorItems: function(sponsors) {
		var items = [],
			allImagesLoaded = [],
			me = this,
			prom;

		sponsors.forEach(function(sponsor) {
			prom = new Promise(function(resolve, reject){
				items.push({
					xtype: 'image',
					src: sponsor.image,
					alt: sponsor.name,
					listeners: {
						click: {
							element: 'el',
							fn: function(e) {
								me.openSponsorLink(sponsor.url);
							}
						},
						load: {
							element: 'el',
							single: true,
							fn: function() {
								resolve();
							}
						}
					}
				});
			});
			allImagesLoaded.push(prom);
		});

		Promise.all(allImagesLoaded).then(function(){
			me.updateLayout();
			me.resetPosition();
		});

		return items;
	},

	resetPosition: function() {
		var me = this,
			mapBBox = me.getPositionOver().getBox(),
			hasDimensions = me.el ? true : false;
			width = hasDimensions ? me.getWidth() : 114,
			height = hasDimensions ? me.getHeight() : 75 * me.getSponsors().length,
			x = mapBBox.width + mapBBox.x - width,
			y = (mapBBox.height - height) / 2;

		me.setPosition(x, y);
	},

	/**
	 * Opens specified sponsors link
	 */
	openSponsorLink: function(url) {
		// track click as well
		ga('send', 'event', 'sponsors', 'click', 'banner', url);

		// open new tab
		window.open(url, '_blank');
	}
});
