var ElevationChart = (function () {
    var ctx = {
        yTickValues: [],
        linePoints: [],
        distanceInMiles: false,
        elevationInFeet: false,
        chartOpacity: 1,
        participants: [],
        participantsPositions: [],
        fixPoints: [{
            x: 0,
            name: '',
            icons: []
        }, {
            x: 5,
            name: '',
            icons: []
        }],
        lowestPoint: Number.POSITIVE_INFINITY,
        lowestPointOnGraph: Number.POSITIVE_INFINITY,
        highestPoint: Number.NEGATIVE_INFINITY,
        highestPointOnGraph: Number.NEGATIVE_INFINITY
    },
    Y_AXIS_TICK_COUNT = 5,
    X_AXIS_TICK_COUNT = 10;

    function draw() {
        parseKml(ctx);
        if (ctx.linePoints.length === 0) return;
        createContext();
        setViewSize(ctx);
        clearChartView(ctx);
        createLineFunction(ctx);
        appendYAxes(ctx);
        appendXAxis(ctx);
        appendTopLine(ctx);
        appendFixPoints(ctx);
        appendLinePath(ctx);
        appendParticipants(ctx);
        appendIndicator(ctx);
        appendXUnit(ctx);
        appendYUnits(ctx);
    }

    function createContext() {
        ctx.yTickValues = calculateYTicks(ctx);
        ctx.view = d3.select('#' + ctx.parentElId + ' > svg');
        ctx.parentEl = document.getElementById(ctx.parentElId);
        ctx.width = ctx.parentEl.offsetWidth;
        ctx.height = ctx.parentEl.offsetHeight;
        ctx.margin = {
            top: ctx.height * 0.2,
            right: ctx.width * 0.06,
            bottom: ctx.height * 0.15,
            left: ctx.width * 0.06
        };
        ctx.xTopLine = ctx.margin.top * 0.2;
        ctx.xScale = createScaleFunction(true, ctx);
        ctx.yScale = createScaleFunction(false, ctx);
        ctx.firstPoint = ctx.linePoints[0];
        ctx.lastPoint = ctx.linePoints[ctx.linePoints.length - 1];
        ctx.minChartViewX = ctx.xScale(ctx.firstPoint ? ctx.firstPoint.x : 0);
        ctx.maxChartViewX = ctx.xScale(ctx.lastPoint ? ctx.lastPoint.x : 0);
        ctx.widthChartView = ctx.maxChartViewX - ctx.minChartViewX;
        ctx.elevationUnit = ctx.elevationInFeet ? 'ft' : 'm';
        ctx.distanceUnit = ctx.distanceInMiles ? 'mi' : 'km';
        ctx.color = {
            blue: 'rgb(68,108,175)',
            darkblue: '#003366',
            orange: 'orange',
            grey: 'lightgrey'
        };
    }

    function setViewSize(ctx) {
        ctx.view.attr('width', ctx.width + 'px')
            .attr('height', ctx.height + 'px');
    }

    function clearChartView(ctx) {
        ctx.view.selectAll("*").remove();
    }

    function createScaleFunction(isXScale, ctx) {
        var rangeMin, rangeMax, domain;

        if (isXScale) {
            rangeMin = ctx.margin.left;
            rangeMax = ctx.width - ctx.margin.right;
            domain = ctx.linePoints.map(function (d) {
                return d.x;
            });
        } else {
            rangeMin = ctx.height - ctx.margin.top;
            rangeMax = ctx.margin.bottom;
            domain = ctx.linePoints.map(function (d) {
                return d.y;
            });
        }

        return d3.scale
            .linear()
            .range([rangeMin, rangeMax])
            .domain([d3.min(domain), d3.max(domain)]);
    }

    function createLineFunction(ctx) {
        ctx.lineFunc = d3.svg.line()
            .x(function (d) {
                return ctx.xScale(d.x);
            })
            .y(function (d) {
                return ctx.yScale(d.y);
            })
            .interpolate('linear');
    }

    function appendYAxes(ctx) {
        appendAxis(ctx, true);
        appendAxis(ctx, false);
    }

    function createAxis(ctx, rightAxis) {
        return d3.svg.axis()
            .scale(ctx.yScale)
            .tickSize(rightAxis ? ctx.width - ctx.margin.left - ctx.margin.right : 0)
            .tickValues(ctx.yTickValues)
            .orient(rightAxis ? 'right' : 'left')
    }

    function appendAxis(ctx, rightAxis) {
        var axisEl = ctx.view.append('svg:g')
            .attr('transform', 'translate(' + ctx.margin.left + ', ' + -(ctx.margin.bottom - ctx.margin.top) + ')')
            .call(createAxis(ctx, rightAxis));

        axisEl.selectAll('text')
            .attr('x', rightAxis ? ctx.width - ctx.margin.left - ctx.margin.right + 8 : -8)
            .attr('dy', -4)
            .style('fill', ctx.color.darkblue)
            .style('font', '10px sans-serif');

        axisEl.selectAll('path')
            .attr('fill', 'none');

        axisEl.selectAll('line')
            .style('stroke-linejoin', 'round')
            .style('stroke-width', '0.7')
            .style('stroke', ctx.color.grey)
            .style('shape-rendering', 'auto');
    }

    function appendXAxis(ctx) {
        var axisEl = ctx.view.append('svg:g')
            .attr('transform', 'translate(0, ' + (ctx.height - ctx.margin.bottom) + ')')
            .call(d3.svg.axis()
                .scale(ctx.xScale)
                .tickSize(3)
                .tickValues(calculateXTicks())
                .orient('bottom')
            );

        axisEl.selectAll('text')
            .style('fill', ctx.color.darkblue)
            .style('font', '10px sans-serif');

        axisEl.selectAll('path')
            .attr('fill', 'none');

        axisEl.selectAll('line')
            .style('stroke-width', '0');
    }

    function calculateXTicks() {
        var tickVal = ctx.lastPoint.x / X_AXIS_TICK_COUNT, // last point x is total distance
            ticks = [0],
            i;

        for (i = 1; i <= X_AXIS_TICK_COUNT; i++) {
            ticks.push(tickVal * i);
        }

        return ticks;
    }

    function calculateYTicks(ctx) {
        var tickDelta =  (ctx.highestPointOnGraph - ctx.lowestPointOnGraph) / Y_AXIS_TICK_COUNT,
            ticks = [ctx.lowestPointOnGraph],
            currentTick = ctx.lowestPointOnGraph,
            i;

        for (i = 1; i <= Y_AXIS_TICK_COUNT; i++) {
            currentTick += tickDelta; 
            ticks.push(currentTick);
        }

        return ticks;
    }

    function appendLinePath(ctx) {
        ctx.linePath = ctx.view.append('svg:path')
            .style('opacity', 0)
            .attr('stroke', ctx.color.blue)
            .attr('transform', 'translate(0, ' + -(ctx.margin.bottom - ctx.margin.top) + ')')
            .transition()
            .duration(800)
            .style('opacity', ctx.chartOpacity)
            .style('shape-linejoin', 'round')
            .style('shape-rendering', 'optimizeSpeed')
            .attr('d', ctx.lineFunc(ctx.linePoints))
            .attr('stroke-width', 0.3)
            .attr('fill', ctx.color.blue);
    }

    function appendTopLine(ctx) {
        ctx.view.append('svg:g')
            .append('line')
            .attr('x1', ctx.margin.left)
            .attr('y1', ctx.xTopLine)
            .attr('x2', ctx.width - ctx.margin.right)
            .attr('y2', ctx.xTopLine)
            .style('stroke-linejoin', 'round')
            .style('stroke-width', '0.7')
            .style('stroke', ctx.color.grey)
            .style('shape-rendering', 'auto');
    }

    function appendParticipants(ctx) {
        var positions = ctx.view.selectAll('svg > g.position').data(ctx.participants, function (d) {
                return d.gps.id;
            }),
            gEl = positions.enter().append('g')
            .attr('class', 'position');

        gEl.append('circle')
            .attr('transform', 'translate(0, ' + -(ctx.margin.bottom - ctx.margin.top) + ')')
            .attr('r', 4)
            .attr('fill', function (d) {
                return d.color;
            })
            .style('stroke', ctx.color.darkblue)
            .style('stroke-width', 1.5)
            .style('opacity', 0.9)
            .attr('cx', function (d) {
                return ctx.xScale(d.x);
            })
            .attr('cy', function (d) {
                return ctx.yScale(getDataModelPointPosY(ctx, d.x));
            });

        positions.select('circle')
            .attr('cx', function (d) {
                return ctx.xScale(d.x);
            })
            .attr('cy', function (d) {
                return ctx.yScale(getDataModelPointPosY(ctx, d.x));
            });

        positions.exit().remove();
    }

    function findPrevAndNextPoint(ctx, x) {
        var i,
            pts = ctx.linePoints,
            ptsLength = pts.length;

        if (ptsLength < 2) return;

        for (i = 1; i < ptsLength; i++) {
            if (pts[i].x >= x) {
                return {
                    next: pts[i],
                    prev: pts[i - 1]
                }
            }
        }
    }

    function appendFixPoints(ctx) {
        var gEl = ctx.view.selectAll('svg')
            .data(ctx.fixPoints)
            .enter()
            .append('g');

        gEl.append('line')
            .attr('x1', function (d) {
                return ctx.xScale(d.x);
            })
            .attr('y1', ctx.xTopLine)
            .attr('x2', function (d) {
                return ctx.xScale(d.x);
            })
            .attr('y2', ctx.height - ctx.margin.bottom)
            .style('stroke', ctx.color.grey)
            .style('stroke-linejoin', '0.7')
            .style('shape-rendering', 'auto');

        gEl.append('text')
            .text(function (d) {
                return d.name;
            })
            .style('font', '12px sans-serif')
            .attr('x', function (d) {
                //calculate x position to center text against line
                return ctx.xScale(d.x) - this.getComputedTextLength() / 2;
            })
            .attr('y', ctx.height - ctx.margin.bottom * 0.6);

        //todo[prm]: Do not add icon for now
        // gEl.append('use')
        //     //.attr('transform', 'scale(0.4, 0.4)')
        //     .attr('xlink:href', '#icon-restaurant')
        //     .style('background-color', 'white')
        //     .attr('x', function (d) {
        //         //calculate x position to center text against line
        //         return ctx.xScale(d.x) - this.getBoundingClientRect().width / 2;
        //     })
        //     .attr('y', function () {
        //         return ctx.margin.top - 80;
        //     })
        // ;
    }

    function appendIndicator(ctx) {
        var args = {
            mousePosX: 0,
            translateY: (ctx.margin.bottom - ctx.margin.top + 7),
            indicatorXPos: ctx.xScale(ctx.lastPoint.x / 2),
            symbol: null,
            rect: null,
            textDist: null,
            textElev: null,
            gEl: null
        };

        args.gEl = ctx.view.append('svg:g')
            .style('fill', ctx.color.darkblue)
            .style('font', '10px sans-serif')
            .style('opacity', 1);

        args.symbol = args.gEl.append("path")
            .style('stroke', ctx.color.darkblue)
            .style('fill', ctx.color.darkblue)
            .attr('d', d3.svg.symbol().type("triangle-down"));

        args.rect = args.gEl.append('rect')
            .style('fill', ctx.color.blue)
            .style('stroke-width', 0.5)
            .style('opacity', 0.4)
            .style('stroke', ctx.color.darkblue)
            .attr('width', 95)
            .attr('height', 40);

        args.textDist = args.gEl.append('text');

        args.textElev = args.gEl.append('text');

        moveIndicator(ctx, args);

        ctx.view.on('mousemove', function () {
            args.indicatorXPos = d3.mouse(this)[0];
            moveIndicator(ctx, args);
            callCbs(ctx, args.indicatorXPos, 'mousemoveCb');
        });

        ctx.view.on('mousedown', function () {
            args.mousePosX = d3.mouse(this)[0];

            if (!insideChartView(ctx, args.mousePosX)) return;

            callCbs(ctx, args.mousePosX, 'mousedownCb');
        });

        ctx.view.on('mouseleave', function () {
            callCbs(ctx, d3.mouse(this)[0], 'mouseleaveCb');
        });
    }

    function moveIndicator(ctx, args) {
        var xPos = args.indicatorXPos,
            x, y, yScale;

        if (!insideChartView(ctx, xPos)) return;

        x = getDataModelPointPosX(ctx, xPos);

        if (x === 0) return;

        y = getDataModelPointPosY(ctx, x);
        yScale = (ctx.yScale(y) - args.translateY);
        args.rect.attr('transform', 'translate(' + (xPos - 5) + ', ' + (yScale - 52) + ')');
        args.symbol.attr('transform', 'translate(' + xPos + ', ' + yScale + ')');
        args.textElev.attr('transform', 'translate(' + xPos + ', ' + (yScale - 35) + ')')
            .text('Elevation: ' + parseInt(y) + ctx.elevationUnit);
        args.textDist.attr('transform', 'translate(' + xPos + ', ' + (yScale - 20) + ')')
            .text('Distance: ' + parseInt(x) + ctx.distanceUnit);
    }

    function callCbs(ctx, mousePosX, cbName) {
        var pt,
            cb = ctx[cbName];

        if (typeof cb === 'undefined') return;

        pt = mousePosXToLinePoint(ctx, mousePosX);

        if (typeof pt === 'undefined') return;

        cb(pt);
    }

    function insideChartView(ctx, posX) {
        return posX >= ctx.minChartViewX && posX <= ctx.maxChartViewX;
    }

    function mousePosXToLinePoint(ctx, mousePosX) {
        var pt = findPrevAndNextPoint(ctx, getDataModelPointPosX(ctx, mousePosX));

        if (typeof pt !== 'undefined') {
            return pt.next;
        }
    }

    function getDataModelPointPosX(ctx, mousePosX) {
        if (insideChartView(ctx, mousePosX)) {
            return parseFloat(((mousePosX - ctx.xScale(ctx.firstPoint.x)) * ctx.lastPoint.x) / ctx.widthChartView);
        }
    }

    function getDataModelPointPosY(ctx, x) {
        var slope, diffY, diffX,
            points = findPrevAndNextPoint(ctx, x);

        if (typeof points === 'undefined' || typeof points.prev === 'undefined') {
            return ctx.lowestPoint;
        }

        diffY = (points.next.y - points.prev.y);
        diffX = (points.next.x - points.prev.x)

        if (diffX === 0) {
            return points.next.y;
        }

        slope = diffY / diffX;

        return (slope * (x - points.prev.x)) + points.prev.y;
    }

    function parseKml(ctx) {
        var kml, coordinates, kmlPts;

        if (!ctx.kml || ctx.kml === '') return;

        coordinates = Array.prototype.map.call(
            ctx.kml.querySelectorAll('LineString coordinates'),
            function (node) {
                return node.textContent.trim().split(' ')
            });

        kmlPts = coordinates.reduce(
            function (acc, current, idx, arr) {
                current.forEach(
                    function (el) {
                        acc.push(el);
                    });
                return acc;
            }, []);

        kmlPts.forEach(function (pointToupleString, i) {
            var point = pointToupleString.split(','),
                lng = parseFloat(point[0]),
                lat = parseFloat(point[1]),
                alt = ctx.elevationInFeet ? mToFt(parseFloat(point[2])) : parseFloat(point[2]),
                prevPoint, dist, minMaxAlt;

            if (i === 0) {
                ctx.linePoints = [{
                    x: 0,
                    y: alt,
                    gps: { lng: lng, lat: lat, alt: alt }
                }];

                return;
            }

            prevPoint = ctx.linePoints[i - 1];

            dist = getDist(prevPoint.gps.lat, prevPoint.gps.lng, lat, lng);

            ctx.linePoints.push({
                x: prevPoint.x + dist,
                y: alt,
                gps: { lng: lng, lat: lat, alt: alt }
            });
        });

        // correct points x to desired distance, aproximation is linear [very rough]
        if (ctx.scaleDistanceTo) {
            rescaleDistance(ctx.linePoints, ctx.scaleDistanceTo);
        }

        ctx.fixPoints[1].x = ctx.linePoints[ctx.linePoints.length - 1].x;

        // sanity, to have chart starting at lowest point level
        minMaxAlt = findMinMaxAltitudes(ctx.linePoints);
        ctx.lowestPointOnGraph = Math.floor(minMaxAlt.min / 100) * 100;
        ctx.highestPointOnGraph = Math.ceil(minMaxAlt.max / 100) * 100;

        ctx.linePoints = pullDownStartAndEnd(ctx.linePoints, ctx.lowestPointOnGraph);

        updateParticipantsPosition();
    }

    function rescaleDistance(points, toDistance) {
        var pointsCount = points.length,
            distanceCorrectionFactor = toDistance / points[pointsCount - 1].x;

        // 1st one is point 0 so don't touch it
        for (var i = 1; i < pointsCount; i++) {
            var point = points[i];
            point.x = point.x * distanceCorrectionFactor;
        }
    }

    function pullDownStartAndEnd(points, lowestPoint) {
        var firstPoint = points[0],
            lastPoint = points[points.length - 1],
            frontDuplicate = {
                x: 0,
                y: lowestPoint,
                gps: {
                    lng: firstPoint.gps.lng,
                    lat: firstPoint.gps.lat,
                    alt: lowestPoint
                }
            },
            rearDuplicate = {
                x: lastPoint.x,
                y: lowestPoint,
                gps: {
                    lng: lastPoint.gps.lng,
                    lat: lastPoint.gps.lat,
                    alt: lowestPoint
                }
            };

        points.unshift(frontDuplicate);
        points.push(rearDuplicate);
        return points;
    }

    function findMinMaxAltitudes(points) {
        return points.reduce(function(minMax, point) {
            if (point.y < minMax.min) minMax.min = point.y;
            if (point.y > minMax.max) minMax.max = point.y;

            return minMax;
        }, { min: Number.POSITIVE_INFINITY, max: Number.NEGATIVE_INFINITY });
    }

    function getDist(lat1, lon1, lat2, lon2) {
        var R = 6371, // radius of the earth in km
            dLat = deg2rad(lat2 - lat1),
            dLon = deg2rad(lon2 - lon1),
            a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        if (ctx.distanceInMiles) {
            return kmToMi(R * c);
        } else {
            return R * c;
        }
    }

    function deg2rad(deg) {
        return deg * (Math.PI / 180);
    }

    function updateParticipantsPosition() {
        if (ctx.linePoints.length === 0) return;

        ctx.participants = ctx.participantsPositions.map(function (pp) {
            return {
                x: pp.dist,
                gps: pp,
                color: 'orange'
            };
        });
    }

    function appendXUnit(ctx) {
        ctx.view.append('svg:g')
            .append('text')
            .text('Distance [' + (ctx.distanceInMiles ? 'mi' : 'km') + ']')
            .style('font', '14px sans-serif')
            .style('font-weight', 'bold')
            .style('fill', ctx.color.darkblue)
            .attr('x', function () {
                //calculate x position to center text against line
                return ctx.width / 2 - this.getComputedTextLength() / 2;
            })
            .attr('y', ctx.height - ctx.margin.bottom / 2 + 7);
    }

    function appendYUnits(ctx) {
        var fontSize = 14,
            x = ctx.margin.left / 2 - fontSize,
            y = 0,
            text = 'Elevation [' + (ctx.elevationInFeet ? 'ft' : 'm') + ']';

        ctx.view.append('svg:g')
            .append('text')
            .text(text)
            .style('font', fontSize + 'px sans-serif')
            .style('font-weight', 'bold')
            .style('fill', ctx.color.darkblue)
            .attr('x', x)
            .attr('y', function () {
                y = ctx.height / 2 + this.getComputedTextLength() / 2;
                return y;
            })
            .attr('transform', 'rotate(-90, ' + x + ', ' + y + ')');
    }

    function kmToMi(dist) {
        return (dist * 1000) * 0.000621371192;
    }

    function mToFt(dist) {
        return dist * 3.2808399;
    }

    return {
        draw: function () {
            // console.log('elevation chart: draw');
            draw();
        },
        updatePositions: function () {
            updateParticipantsPosition();
            appendParticipants(ctx);
        },
        ctx: ctx
    }
})();