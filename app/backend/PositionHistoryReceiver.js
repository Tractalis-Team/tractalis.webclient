/**
 * TODO: describe what is happening here
 */
Ext.define('TractalisWebclient.backend.PositionHistoryReceiver', {
    singleton: true,
    alternateClassName: 'PositionHistoryReceiver',

    config: {
        nextTimeout: null,

        /**
         * @cfg {Number} updateInterval
         */
        updateInterval: 0,

        /**
         * @cfg {TractalisWebclient.store.PositionHistories} store
         * The store which contains the positions
         */
        store: null
    },

    /**
     * @private
     * @returns {undefined}
     */
    constructor: function() {
        var me = this;

        // create the automatic properties
        me.initConfig(me.config);
    },

    /**
     * Starts fetching continously the data
     */
    startFetching: function() {
        this.fetch();
    },

    /**
     * Fetches the data and prepares the next schedule to do so.
     * **NOTE**: This method should be called only once from outside, otherwhise we have multiple processes trying to fetch positions
     * This would cause a lot of unessecary traffic and would not resepect the update Interval time
     */
    fetch: function() {
        var me = this;

        // prepare the timeout for the next loading process
        var timeout = setTimeout(function() {
            var store = me.getStore();

            if (!store.isLoading()) { // still loading after last time. Try next time
                store.load()
            }

            me.fetch();
        }, me.getUpdateInterval());

        me.setNextTimeout(timeout);
    },

    /**
     * Stops the current fetching process, if one is going on, otherwhise does nothing.
     */
    stopFetching: function() {
        if (this.getNextTimeout() !== null) {
            clearTimeout(this.getNextTimeout());
        }
    }
});
