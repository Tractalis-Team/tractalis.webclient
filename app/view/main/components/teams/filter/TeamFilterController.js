/**
 * Viewcontroller for the Teamfilter.
 */
Ext.define('TractalisWebclient.view.main.components.teams.filter.TeamFilterController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.teamfilter',

	control: {
		'#': {
			fulltext: "performFulltextFilter",
			clearFulltext: "clearFulltextFilter",

			top5: "performTop5Filtering",
			categoryFilter: "openCategoryFilter",
			clearAll: "clearAllFilters",

			toggleAllTeams: "toggleAllTeams"
		}
	},

	config: {
		fulltextFilter: Ext.create('FulltextFilter'),
		top5Filter: Ext.create('Top5Filter'),
		categoryFilter: Ext.create('CategoryFilter'),
		teamEnabledFilter: Ext.create('TeamEnabledFilter')
	},

	init: function() {
		var me = this;

		// listen for global bus fulltext filtering message
		Ext.on('restoreFulltextFilter', function(fulltextSearchQuery) {
			// change the value of the textfield
			Ext.getCmp('fulltextElement').setValue(fulltextSearchQuery);
		});

		Ext.on('restoreTop5', function(toggled) {
			Ext.getCmp('top5Button').toggle(toggled !== false, true);
		});

		Ext.on('restoreTeamEnabled', function(config) {
			var teamToggleButton = Ext.getCmp('teamToggleButton');

			teamToggleButton.setRawValue(!config.positiveFiltering);

			me.getTeamEnabledFilter().setConfig(config);
		});

		me.getView().getTeamStore().on('update', me.onTeamStoreUpdate, me);
	},

	/**
	 * Toggles the enabled state of all teams
	 *
	 * @param {Boolean} enabled
	 */
	toggleAllTeams: function(enabled) {
		var me = this,
			store = me.getView().getTeamStore(),
			filter = me.getTeamEnabledFilter();

		// empty array means all teams were selected
		filter.setTeams([]);
		filter.setPositiveFiltering(!enabled);
		filter.setEnabled(!enabled);

		FilterHelper.applyFilter(filter);
		FilterHelper.sync();

		store.enableAll(enabled);
	},

	/**
	 * Is called when the top5-Filtering is switched on / off
	 */
	performTop5Filtering: function(enabled) {
		var filter = this.getTop5Filter();

		filter.setEnabled(enabled);

		FilterHelper.applyFilter(filter);
		FilterHelper.sync();
	},

	/**
	 * Performs the actual full-text filtering
	 *
	 * @param {String} value
	 */
	performFulltextFilter: function(value) {
		var filter = this.getFulltextFilter(),
			value = value.toLowerCase();

		filter.setEnabled(true);
		filter.setSearch(value);

		FilterHelper.applyFilter(filter);
		FilterHelper.sync();
	},

	/**
	 * Opens asynchronously the category filter control
	 * and waits for the filtering.
	 */
	openCategoryFilter: function() {
		var categoryFilter = Ext.create({
			xtype: 'category-filter',

			teamCategoryStore: this.getView().getTeamCategoryStore(),

			// sizes & positioning
			height: 300,
			width: 300,
			x: 100,
			y: 100
		});

		categoryFilter.on('filtered', this.performCategoryFiltering, this);
		categoryFilter.show();
	},

	/**
	 * Adds a filter which filters out all the categories which were **NOT SPECIFIED** in the categories array passed.
	 *
	 * @param {Array} categories Array of category names which should be INCLUDED in the result, all others are filtered out
	 * @param {Number} totalCategoriesCount The total count of availble categories in the current run, independent of the filtering
	 */
	performCategoryFiltering: function(categories, totalCategoriesCount) {
		var me = this,
			filter = me.getCategoryFilter();

		// this filter is only enabled when the selected categories are less than the total amount of them
		// got the club goooing uuuuup on a tuesdayyyy (qifshadrake)
		filter.setEnabled(categories.length < totalCategoriesCount);
		filter.setCategoryNames(categories);

		FilterHelper.applyFilter(filter);
		FilterHelper.sync();
	},

	/**
	 * Remnoves all filters on the linked store of the view.
	 */
	clearAllFilters: function() {
		FilterHelper.clearAll();

		// remove all filters from the team store
		this.getView().getTeamStore().clearFilter();
	},

	/**
	 * Clears the fulltext-filter on the store.
	 */
	clearFulltextFilter: function() {
		var filter = this.getFulltextFilter();

		filter.setEnabled(false);

		FilterHelper.applyFilter(filter);
		FilterHelper.sync();
	},

	/**
	 * Is called when teams records are modified from outside
	 *
	 * @param {TractalisWebclient.store.Teams} store
	 * @param {TractalisWebclient.model.Team} team
	 * @param {Ext.data.Operation} operation
	 * @param {Array} modifiedFieldNames List of changed fields as string
	 */
	onTeamStoreUpdate: function(store, team, operation, modifiedFieldNames) {
		if (modifiedFieldNames.indexOf('Enabled') > -1) {
			var filter = this.getTeamEnabledFilter();

			filter.onTeamEnabledChange(team);
			if (filter.getTeams().length > 0) {
				filter.setEnabled(true);
			} else {
				var enable = !Ext.getCmp('teamToggleButton').getValue();
				filter.setEnabled(enable);
				filter.setPositiveFiltering(enable);
			}

			FilterHelper.applyFilter(filter);
			FilterHelper.sync();
		}
	}
});