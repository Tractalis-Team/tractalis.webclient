Ext.define("TractalisWebclient.view.main.components.socialmedia.SocialMediaPanel", {
    extend: "Ext.panel.Panel",
    layout: 'card',

    alias: 'widget.social-media',

    id: 'social-media-panel',

    config: {
        minWidth: 180,
        maxWidth: 500,

        split: {
            collapsible: true
        }
    },

    header: false,
    activeItem: 0,
    coverHackStarted: false,

    initComponent: function () {
        var me = this,
            header = {
                xtype: 'panel',
                width: '100%',
                layout: 'hbox',
                items: []
            },
            panels = [],
            configHadDefult = false;

        me.callParent(arguments);

        var facebookConfig = Configuration.get('socialMedia/facebook', null);
        if (facebookConfig) {
            // Facebook
            header.items.push({
                xtype: 'button',
                flex: 1,
                text: 'Facebook',
                iconCls: 'x-fa fa-facebook-official fa-lg',
                toggleGroup: 'social-media-switch-buttons-group',
                pressed: !!facebookConfig.active,
                listeners: {
                    toggle: me.handleToggle.bind(me, 0) // hardcoded panel idx
                }
            });
            panels.push({
                id: 'facebookPanel',
                xtype: 'facebook',

                facebookUrl: facebookConfig.url
            });

            configHadDefult = !!facebookConfig.active;
        }

        var twitterConfig = Configuration.get('socialMedia/twitter', null);
        if (twitterConfig && twitterConfig.widgetId) {
            // Twitter
            header.items.push({
                xtype: 'button',
                flex: 1,
                text: 'Twitter',
                iconCls: 'x-fa fa-twitter fa-lg',
                toggleGroup: 'social-media-switch-buttons-group',
                pressed: !!twitterConfig.active,
                listeners: {
                    toggle: me.handleToggle.bind(me, 1) // hardcoded panel idx
                }
            });
            panels.push({
                id: 'twitterPanel',
                xtype: 'twitter',

                widgetId: twitterConfig.widgetId,
                showWidgetHeader: !!twitterConfig.showWidgetHeader
            });
            
            if (!configHadDefult && twitterConfig.active) {
                configHadDefult = true;
                me.activeItem = 1; // hardcoded panel idx
            }
        }

        if (header.items.length > 1) {
            if (!configHadDefult) {
                // there was no active one so we will end up with 1st one so 1st button has to be pressed
                header.items[0].pressed = true;
            }

            // docs say there is setHeader method but it is not present on runtime,  ext panel source doesn't have it neither
            me.header = header;
        }

        if (panels.length > 0) {
            me.add(panels);

            // both twitter and facebook uses iframe wrapper which results in crappy resize behavior,
            // when resizing by pulling over the panel (reducing size), iframe eats up mouse events
            // and nothing happens until mose is moved over map and back over the panel :P
            // To address this, class with ::before pseudo-element covering entire panel has been introduced.
            // This class is applied only while resizing.
            me.setUpCoverIframeHack();
        } else {
            console && console.warn('It seems like social-media secion is present in config but it is invalid or empty... :P')
            me.hide();
        }
    },

    handleToggle: function (idx, button, toggled) {
        if (toggled) {
            this.getLayout().setActiveItem(idx);
        }
    },

    addCoverHack: function () {
        this.addCls('onDragIframeCover');
        this.coverHackStarted = true;
    },

    removeCoverHack: function () {
        if (this.coverHackStarted) {
            this.removeCls('onDragIframeCover');
            this.coverHackStarted = false;
        }
    },

    setUpCoverIframeHack: function () {
        var me = this,
            config = me.getConfig();

        config.split.listeners = {
            mouseDown: {
                element: 'el',
                fn: me.addCoverHack.bind(me)
            }
        };

        // btn release can happen everywhere
        document.addEventListener('mouseup', me.removeCoverHack.bind(me));
    }
});