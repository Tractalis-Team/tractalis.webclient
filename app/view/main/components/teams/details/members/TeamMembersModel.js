Ext.define('TractalisWebclient.view.main.components.teams.details.members.TeamMembersModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.team-members',

    data: {
    	team: null
    }
});
