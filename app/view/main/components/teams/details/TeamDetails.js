/**
 * Teamdetails view which represents details for a specific team
 */
Ext.define("TractalisWebclient.view.main.components.teams.details.TeamDetails", {
	extend: "Ext.panel.Panel",
	alias: "widget.team-details",

	requires: [
		'TractalisWebclient.view.main.components.teams.details.TeamDetailsModel',
		'TractalisWebclient.view.main.components.teams.details.TeamDetailsController'
	],

	controller: "team-details",
	viewModel: {
		type: "team-details"
	},

	FORM_CONFIG: {
		RAW: [
			'race data',
			['Distance', '{teamRawDistance}'],
			['Speed', '{teamSpeed}'],
			['Battery', '{team.BatteryPercentage}%']
		],
		SNAP: [
			'race data',
			['Distance', '{teamDistance}'],
			['Distance Left', '{teamDistanceLeft}'],
			['Speed', '{teamSpeed}'],
			'next station',
			['Distance left', '{teamDistanceToNextTs}'],
			['Battery', '{team.BatteryPercentage}%']
		],
		POSITION: [
			'position',
			['Time stamp', '{teamTimeStamp}'],
			['Longitude', '{team.Longitude}'],
			['Latitude', '{team.Latitude}'],
			['Altitude', '{teamAltitude}']
		],
		SWISS_GRID: [
			['Swissgrid X', '{teamSwissX}'],
			['Swissgrid Y', '{teamSwissY}']
		]
	},

	/**
	 * @event showTeamMember
	 * @param {TractalisWebclient.model.Team} team The team for which the teammembers were requested
	 */
	config: {
		/**
		 * @cfg {TractalisWebclient.model.Team} team
		 * Contains the team which should be represented by this view.
		 */
		team: null,

		/**
		 * @cfg {TractalisWebclient.model.Position} teamPosition
		 * Contains the associated position record which should be represented for this team.
		 * **NOTE**: This will be replaced lateron with a model association.
		 */
		teamPosition: null
	},

	layout: 'hbox',
	align: 'stretchmax',
	bodyPadding: 5,
	cls: 'team-details',

	tools: [{
		iconCls: 'fa fa-users team-members',
		tooltip: 'Team Members',
		
		/**
		 * @private
		 * Callback for this tool button
		 *
		 * @param {Ext.Component} owner
		 * @param {Ext.panel.Tool} tool
		 * @param {Ext.event.Event} event
		 */
		callback: function(owner, tool, event) {
			owner.fireEvent('showTeamMember', owner.getViewModel().get('team'));
		}
	}],

	bind: {
		title: (Configuration.get('race/includeRanks') ? '{teamRankCaption} - ' : '') + '{team.StartNumber}: {team.Name}'
	},

	/**
	 * Is called after the team property is updated.
	 * @param  {TractalisWebClient.model.Team} team
	 */
	updateTeam: function(team) {
		this.getViewModel().set('team', team);
	},

	/**
	 * Is called after the position property is updated.
	 * @param  {TractalisWebClient.model.Position} position
	 */
	updateTeamPosition: function(position) {
		this.getViewModel().set('teamPosition', position);
	},

	/**
	 * @method initComponent
	 * @inheritdoc
	 * @return {void}
	 */
	initComponent: function() {
		var me = this;

		me.callParent(arguments);
		me.add([
			me.getPropertyContainer(),
			me.getRightContainer()
		]);
	},

	/**
	 * Gets the form label
	 * @return {Ext.panel.Panel}
	 */
	getPropertyContainer: function() {
		var itemConfiguration = [],
			activeFormConf = Configuration.get('details/isRaw', false) ? this.FORM_CONFIG.RAW : this.FORM_CONFIG.SNAP,	
			formConfiguration = [...activeFormConf];

			

		// add positions sttuff which is common
		formConfiguration.push(...this.FORM_CONFIG.POSITION);

		// add an additional row for swiss grid coordinates
		if (Configuration.get('race/includeSwissgrid')){
			formConfiguration.push(...this.FORM_CONFIG.SWISS_GRID);
		}

		formConfiguration.forEach(function(component) {
			if (Array.isArray(component)) {
				// normal array
				itemConfiguration.push({
					html: component[0] + ':',
					cls: 'title'
				});
				itemConfiguration.push({
					bind: {
						html: component[1]
					},
					cls: 'value'
				});
			} else if(typeof(component) === 'string') {
				// is a single label object
				itemConfiguration.push({
					html: '- ' + component + ' -',
					colspan: 2,
					cls: 'caption'
				});
			} else {
				itemConfiguration.push(component);
			}
		});

		return {
			xtype: 'panel',
			cls: 'properties-container',
			height: '100%',
			layout: {
				type: 'table',
				columns: 2
			},
			defaults: {
				xtype: 'component'
			},
			flex: 10,
			items: itemConfiguration
		};
	},

	/**
	 * Gets the right container configuration
	 *
	 * @return {Object} configuration
	 */
	getRightContainer: function() {
		return {
			xtype: 'panel',
			flex: 4,
			layout: 'vbox',
			height: '100%',
			width: '100%',
			items: [
				this.getPositionWidget(), {
					xtype: 'component',
					cls: 'status-label',
					flex: 1,
					bind: {
						html: '{team.StatusName}'
					}
				}, {
					xtype: 'checkbox',
					flex: 1,
					boxLabel: 'Follow team',
					bind: '{team.IsFollowed}'
				},
				this.getGpxTrackLink()
			]
		};
	},

	getGpxTrackLink: function(){
		return {
			xtype: 'button',
			text: 'GPX Track',
			iconCls: 'x-fa fa-save',
			scale: 'small',
			flex: 1,
			bind: {
				hidden: '{!hqMode}',
				href: '{teamGpxTrackLink}'
			}
		};
	},

	/**
	 * Gets the position widget configuration
	 *
	 * @return {Object} configuration
	 */
	getPositionWidget: function() {
		return {
			xtype: 'panel',
			layout: 'vbox',
			width: '100%',
			cls: 'position-widget',
			flex: 4,
			defaults: {
				xtype: 'component'
			},
			items: [{
				cls: 'distance-next-team',
				bind: {
					html: '- {teamToNextTeam} '
				},
				flex: 2
			}, {
				cls: 'rank',
				bind: {
					html: '{teamRankCaption}'
				},
				flex: 6
			}, {
				cls: 'distance-previous-team',
				bind: {
					html: '+ {teamToPrevTeam}'
				},
				flex: 2
			}]
		};
	}
});
