/**
 * This Helper class provides various functionallities for The Azure Webservices and the Certificates which are mandatory to access them.
 *
 * Example usage for this manager in the application could be the following
 *
 *     // We want to generate the single Shared Key for a 'GET'-Request on a specific table
 *     CertificateHelper.generatedSharedKey(new Date().toUTCString(), 'GET', 'Event12TeamData);
 *     // keep in mind that the specified date has to be UTC and NOT LOCAL TIME!
 *
 *
 *     // We can also generate all the headers which are nessecary for azure
 *     store.getProxy().setHeaders(
 *          CertificateHelper.generateAuthorizationHeader('GET', 'Event12TeamData')
 *     );
 */
Ext.define('TractalisWebclient.helper.AzureCertificateHelper', {
	singleton: true,
	alternateClassName: 'CertificateHelper',

	mixins: {
		observable: 'Ext.util.Observable'
	},

	config: {
		/**
		 * @cfg {String} storageAccountName
		 * The Name of the Azure Storage Account to which the connection should be etablished.
		 */
		storageAccountName: 'tractalisstorage',

		/**
		 * @cfg {Boolean} isLoading
		 * Is true when the certificates are loaded currently
		 */
		isLoading: false
	},

	/**
	 * @private
	 * @returns {undefined}
	 */
	constructor: function() {
		var me = this;

		me.mixins.observable.constructor.call(me);

		// create the automatic properties
		me.initConfig(me.config);
	},

	/**
	 * Removes all SAS certificate keys from local storage
	 */
	clear: function() {
		var keys = [];
		for (var i = 0; i < localStorage.length; i++) {
			var key = localStorage.key(i);

			if (key.indexOf('SAS_') > -1) {
				keys.push(key);
			}
		}

		keys.forEach(function(key) {
			localStorage.removeItem(key);
		});
	},

	/**
	 * Fetches the SAS Configuration from the server and stores it into local storage in each case.
	 */
	fetchSASConfiguration: function() {
		var me = this;

		// only load if not loading already
		// otherwhise we have three or four paralell requests to the SAS generating service
		if (me.getIsLoading()) {
			return;
		}

		me.setIsLoading(true);

		Ext.Ajax.request({
			// build the URL based on the partition and row
			url: 'https://data-access.tractalis.com/api/DataAccess/getsasfortablesclient?eventId=' + Configuration.get('race/id') + '&minutes=120',

			// to fetch data we always use GET
			method: 'GET',

			headers: {
				'Content-Type': 'Application/json',
				'Accept': 'Application/json'
			},

			disableCaching: false,

			/**
			 * @private
			 * Ajax retrieval was a success
			 * @param {Ext.data.Response} response
			 */
			success: function(response) {
				var result = JSON.parse(response.responseText),
					mine = this;

				// store the sas keys without the ? which is retrieved
				// we try to store it in a url-position independent format so we can put it in the beginning
				// or the end of the url-query lateron
				localStorage.setItem(mine.resolveIdentifier('FixPoints'), result.FixPoints.replace("?", ""));
				localStorage.setItem(mine.resolveIdentifier('Positions'), result.Positions.replace("?", ""));
				localStorage.setItem(mine.resolveIdentifier('TeamData'), result.TeamData.replace("?", ""));
				localStorage.setItem(mine.resolveIdentifier('History'), result.History.replace("?", ""));

				mine.setIsLoading(false);
				mine.fireEvent('sasLoaded');
			},

			scope: me
		});
	},

	/**
	 * Gets the specified SAS key or NULL if not found
	 * @param {String} key For example 'Positions'
	 * @returns {String} the sas key or NULL
	 */
	getSasUrl: function(key) {
		return localStorage.getItem(this.resolveIdentifier(key));
	},

	/**
	 * Resolves the identifier for a SAS key
	 * @param {String} key The key
	 * @returns {String}
	 */
	resolveIdentifier: function(key) {
		return 'SAS_' + Configuration.get('race/id') + '_' + key;
	}
});
