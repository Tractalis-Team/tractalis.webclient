Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.BaseLayersCreator", {
	singleton: true,
	alternateClassName: 'BaseLayersCreator',

	config: {
		defaultBaseLayers: [{
			name: 'Topographic'
		}],

		// Layers which esri provided with aditional label layers
		labelledLayers: ['Oceans', 'Gray', 'DarkGray', 'Imagery', 'ShadedRelief', 'Terrain']
	},

	constructor: function () {
		var me = this;

		me.initConfig(me.config);
	},

	setUpBaseLayers: function (map, layersControl) {
		var me = this,
			baseLayers = Configuration.get('gis/baseLayers'),
			activeLayer;

		baseLayers || (baseLayers = me.getDefaultBaseLayers());

		baseLayers.forEach(function (layerCfg, idx) {
			var layer = me.toLayer(layerCfg);

			// Mark it active in case it is explicitly marked or if it is 1st one so that if
			// none is marked as an active at least 1st one will be.
			if (layerCfg.active || idx === 0) {
				activeLayer = layer
			}

			layersControl && layersControl.addBaseLayer(layer, layerCfg.name);
		});

		activeLayer.addTo(map);
	},

	toLayer: function (layerCfg) {
		if (layerCfg.labels && this.getLabelledLayers().indexOf(layerCfg.name) > -1) {
			return L.layerGroup([
				L.esri.basemapLayer(layerCfg.name),
				L.esri.basemapLayer(layerCfg.name + 'Labels')
			]);
		}

		let layer;

		if (layerCfg.kind === 'wms') {
			layer = L.tileLayer.wms(layerCfg.url, layerCfg.config);
		} else {
			layer = L.esri.basemapLayer(layerCfg.name);
		}

		return layer;
	}
});