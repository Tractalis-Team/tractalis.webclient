/**
 * This class is used as Base-Class for each Command which should be able to synchronize via
 * the Global Message Queue across different instances of the client.
 */
Ext.define('TractalisWebclient.helper.teamfilters.BaseFilter', {
	alternateClassName: 'BaseFilter',

	config: {
		/**
		 * @cfg {Boolean} enabled
		 * True or false to specifiy whether the filter is enabled or not
		 */
		enabled: true,

		/**
		 * @cfg {String} id
		 * The id which is used to identify this filter instance
		 */
		id: ''
	},

	/**
	 * Compose message
	 *
	 * @param {Object} value
	 */
	constructor: function() {
		var me = this;

		// make sure the configuration is initialized properly
		// otherwhise the values of the config object are not transfered
		// correctly into the corresponding getters / setters
		me.initConfig(me.config);
		me.setId(Ext.getClassName(me));
	},

	/**
	 * Serializes the command so it can be sent through the persistance for example
	 *
	 * @returns {Object} Serialized object wwhich is ready to be packaged to be stored
	 */
	serialize: function() {
		var me = this;

		return {
			enabled: me.getEnabled()
		};
	},

	/**
	 * Restores the filter
	 */
	restore: function() {},

	/**
	 * Clears this filter and reset the state before the filter was applied
	 */
	clear: function() {
		return;
	},

	/**
	 * Executes the actual request request
	 *
	 * @returns {Boolean} true when filter was applied, otherwhise false
	 */
	execute: function() {},

	/**
	 * Add Properties to the specified object and retrieves imediately
	 * Used for object serialization
	 *
	 * @param {Object} object the Object to which the properties will be added
	 * @param {Object} properties Object with key value properties which will be copied into object
	 *
	 * @returns {Object} ready to use object
	 */
	addProperties: function(object, properties) {
		for (var propertyName in properties) {
			object[propertyName] = properties[propertyName];
		}

		return object;
	},

	toOdataFilter: function(){
		return null;
	}
});