/**
 * This store represents all Fix Points which are active for the current specified race.
 */
Ext.define('TractalisWebclient.store.FixPoints', {
    extend: 'Ext.data.Store',
    model: 'TractalisWebclient.model.FixPoint',
    autoLoad: true,
    pageSize: 0,
    
	proxy: {
		type: 'azure-tables',
		tableName: AzureTablesProxy.buildTableName('Event', Configuration.get('race/id'), 'FixPoints'),
		sasKey: 'FixPoints',

		reader: {
			type: 'json',
			rootProperty: 'value',
			successProperty: false
		}
	}
});


