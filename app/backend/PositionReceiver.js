/**
 * This backend class is responsible for updating the Positions in the Positions Store.
 * This Helper class provides various functionallities for The Azure Webservices and the Certificates which are mandatory to access them.
 *
 * Example usage for this manager in the application could be the following
 *
 *     // We want to generate the single Shared Key for a 'GET'-Request on a specific table
 *     CertificateHelper.generatedSharedKey(new Date().toUTCString(), 'GET', 'Event12TeamData);
 *     // keep in mind that the specified date has to be UTC and NOT LOCAL TIME!
 *
 *
 *     // We can also generate all the headers which are nessecary for azure
 *     store.getProxy().setHeaders(
 *          CertificateHelper.generateAuthorizationHeader('GET', 'Event12TeamData')
 *     );
 */
Ext.define('TractalisWebclient.backend.PositionReceiver', {
	singleton: true,
	alternateClassName: 'PositionReceiver',

	config: {
		nextTimeout: null,

		/**
		 * @cfg {Number} updateInterval
		 */
		updateInterval: 0,

		/**
		 * @cfg {TractalisWebclient.store.Positions} store
		 * The store which contains the positions
		 */
		store: null,

		/**
		 * @cfg {Array} synchronizedFields
		 * This array contains the names of all fields which should be synchronized from **Positions** to the **Teams**
		 */
		synchronizedFields: [
			'Rank',
			'Distance',
			'DistanceLeft',
			'Altitude',
			'Longitude',
			'Latitude',
			'ToNextTeam',
			'ToPrevTeam',
			'Speed',
			'DistanceToNextTs',
			'BatteryPercentage',
			'StatusName',
			'Status',
			'TimeStamp',
			'OdoMileage'
		]
	},

	/**
	 * @private
	 * @returns {undefined}
	 */
	constructor: function() {
		var me = this;

		// create the automatic properties
		me.initConfig(me.config);

		Ext.GlobalEvents.on('globalFilteringChanged', me.filteringChanged, me);
	},

	filteringChanged: function(odataFilter) {
		var storePorxy = this.getStore().getProxy(),
			initialFilter = storePorxy.getInitialConfig().filter,
			resultingFilter = initialFilter ? initialFilter + (odataFilter ? ' and ' + odataFilter : '') : odataFilter;

		if (storePorxy.getFilter() !== resultingFilter) {
			this.stopFetching();
			storePorxy.setFilter(resultingFilter);
			this.fetchNow();
			this.fetch();
		}
	},

	/**
	 * Starts fetching continously the data
	 */
	startFetching: function() {
		this.fetch();

		// when teams are reloaded, we need to stop the current fetching process
		// and initialize a new one
		Ext.getStore('Teams').on('load', function() {
			this.stopFetching();
			this.fetch();
		}, this, {
			single: true
		});
	},

	/**
	 * Makes sure that the positions are fetched in that particular moment when this method is called. The process is inizialized as well.
	 */
	fetchNow: function() {
		this.getStore().on('load', function(records) {
			this.storeHasLoad(records);
		}, this, {
			// remove after one execution
			single: true,

			// make sure this handler gets called before all others
			priority: 100
		});

		this.getStore().load();
	},

	/**
	 * Fetches the data and prepares the next schedule to do so.
	 * **NOTE**: This method should be called only once from outside, otherwhise we have multiple processes trying to fetch positions
	 * This would cause a lot of unessecary traffic and would not resepect the update Interval time
	 */
	fetch: function() {
		var me = this;

		// prepare the timeout for the next loading process
		var timeout = setTimeout(function() {
			// the actual loading is done via the stores load() method
			// so we must keep in mind that the store data is exchanged completely
			// dirty records are overwritten!!
			me.fetchNow();
			me.fetch();
		}, me.getUpdateInterval());

		me.setNextTimeout(timeout);
	},

	/**
	 * Stops the current fetching process, if one is going on, otherwhise does nothing.
	 */
	stopFetching: function() {
		if (this.getNextTimeout() !== null) {
			clearTimeout(this.getNextTimeout());
		}
	},

	/**
	 * Initially copies the ranks from the Positions to the Teams
	 *
	 * @param {Array} positions
	 */
	copyRanks: function(positions) {
		var me = this,
			teams = Ext.getStore('Teams');

		if (teams.isLoaded()) {
			me.storeHasLoad(positions);
		} else {
			// wait for the teams to load, and then start copy the ranks
			teams.on('load', function(store, records) {
				teams.beginUpdate();

				store.each(function(record) {
					var position = this.getStore().getById(record.get('TeamId'));

					this.synchronizeFromPosition(record, position);
				}, me);

				teams.endUpdate();
			}, me, {
				single: true
			});
		}
	},

	/**
	 * Store was loaded properly
	 *
	 * @param {Array} records The list of loaded records
	 */
	storeHasLoad: function(records) {
		var me = this,
			positions = me.getStore(),
			teams = Ext.getStore('Teams');

		teams.beginUpdate();

		// if team was loaded, we synchronize the positions into teams
		if (teams.isLoaded()) {
			positions.each(me.handlePositionUpdates, me);
		}

		teams.endUpdate();
	},

	/**
	 * Handle Position Updates
	 *
	 * @param {TractalisWebclient.model.Position} positionUpdate
	 */
	handlePositionUpdates: function(positionUpdate) {
		var teamId = positionUpdate.get('TeamId'),
			team = Ext.getStore('Teams').getById(teamId);

		this.synchronizeFromPosition(team, positionUpdate);
	},

	/**
	 * Synchronizes the specified team entity with values from the position entity
	 *
	 * @param  {TractalisWebclient.model.Team} team
	 * @param  {TractalisWebclient.model.Position} position
	 */
	synchronizeFromPosition: function(team, position) {
		if (team === null || position === null) return;

		// team.set('position', position);
		position.set('team', team, {
			silent: true
		});
		position.set('StartNumber', team.get('StartNumber'), {
			silent: true
		});

		const fields = this.getSynchronizedFields();

		const change = fields.reduce((acc, field) => {
			acc[field] = position.get(field);
			return acc;
		}, {});

		team.set(change);
	}
});
