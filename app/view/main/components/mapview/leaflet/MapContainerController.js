/**
 * Viewcontroller for {TractalisWebclient.view.main.components.mapview.leaflet.MapContainer}
 */
Ext.define('TractalisWebclient.view.main.components.mapview.leaflet.MapContainerController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.L-map-container',

	requires: [
		'TractalisWebclient.helper.teamfilters.SoleTeamFilter'
	],

	config: {
		/**
		 * TODO: rewrite it
		 * @cfg {EsriMap} mapView
		 * Contains the Mapview Instance for ESRI-Operations
		 */
		map: null,

		fixPointsCreator: null,
		positionsLayerController: null,

		soleTeamFilter: Ext.create('SoleTeamFilter'),

		/**
		 * Tells whether map is fully intialised. It is state rather for internal use only.
		 */
		mapReady: false,

		/**
		 * Listen to incoming events
		 */
		control: {
			//'#' -> view
			'#': {
				mapSetUp: 'onMapSetUp',
				teamSelected: 'focusOnTeam',
				showCoords: 'focusOnCoords'
			}
		},

		// TODO: consider moving it somewhere higher in hierarchy od app.
		// As it may intorduce app wide state changes which fall outside of resposibility of map itself
		routes: {
			'gis/timestation/:id': {
				action: 'focusOnFixPoint',
				before: 'ensureMapIsReady'
			},
			'gis/teams/:starterId': {
				action: 'focusOnStarter',
				before: 'ensureMapIsReady'
			},
			'gis/team/:starterId': {
				action: 'focusOnSoleStarter',
				before: 'ensureMapIsReady'
			}
		}
	},

	_closeZoomLevel: 14,

	/**
	 * TODO: write it down
	 */
	onMapSetUp: function (map) {
		var me = this,
			view = me.getView(),
			// create things which will set up the layers
			externalLayersCreator = Ext.create('ExternalLayersCreator', map, view.getLayersControl(), me.getViewModel()),
			positionsLayerController = Ext.create('PositionsLayerController', {mapContainer: view, map: map}),
			fixPointsCreator = Ext.create('FixPointsCreator', {mapContainer: view}),
			ghostLayer = Ext.create('GhostLayer', {mapContainer: view, map: map}),
			historicalPositionsLayer = Ext.create('HistoricalPositionsLayer',
				{
					map: map,
					layer: view.getPositionHistoryLayer(),
					teams: Ext.getStore('Teams'),
					positionHistories: Ext.getStore('PositionHistories')
				}
			),
			youAreHereLayer = Ext.create('YouAreHereLayer', {mapContainer: view, map: map}),
			// tell them to start setting things up
			externalLayersPromise = externalLayersCreator.setUpLayers(),
			positionsPromise = positionsLayerController.setUpLayer(view.getPositionsLayer()),
			fixpointsPromise = fixPointsCreator.setUpLayer(view.getFixpointsLayer()),
			ghostPromise = ghostLayer.setUpLayer(),
			historyPromise = historicalPositionsLayer.setUpLayer(),
			youAreHerePromise = youAreHereLayer.setUpLayer(),
			// collect layer set up promises in array, on very end we will hook up to it with Promise.all, so when they
			// finish we will spit out mapReady event on the component
			layersAreInitialised = [externalLayersPromise, positionsPromise, fixpointsPromise, ghostPromise, historyPromise, youAreHerePromise];

		youAreHerePromise.catch(err => {
			Ext.GlobalEvents.fireEvent('gelocation-unavailable');
		});

		this._closeZoomLevel = Configuration.get('gis/closeZoomLevel')

		me.setMap(map);

		me.setPositionsLayerController(positionsLayerController);
		me.setFixPointsCreator(fixPointsCreator);
		map.on('zoom', me.toggleLabels.bind(me, view, map, this._closeZoomLevel));

		// fire mapReady event when all promises of layers are resolved
		Promise.all(layersAreInitialised).then(me.fireMapReady.bind(me));
	},

	/**
	 * Fire mapReady event through ExtJS Event handler
	 */
	fireMapReady: function () {
		this.getView().fireEvent('mapReady');
		this.setMapReady(true);
	},

	/**
	 * TODO: drop here sentence or two.
	 */
	ensureMapIsReady: function (id, action) {
		if (!this.getMapReady()) {
			var view = this.getView(),
				timeoutId,
				resumeFn = function () {
					clearTimeout(timeoutId);
					action.resume();
				},
				stopFn = function () {
					view.un('mapReady', resumeFn);
					action.stop();
				};

			// 15sec for map loading it is more than enough, later abandon and unblock queued actions
			timeoutId = setTimeout(stopFn, 15000);

			view.on('mapReady', resumeFn, {single: true});
		} else {
			action.resume();
		}
	},

	toggleLabels: function (view, map, closeZoom) {
		var curentZoom = map.getZoom();

		if (closeZoom <= curentZoom) {
			view.fireEvent('showLabels');
		} else {
			view.fireEvent('hideLabels');
		}
	},

	/**
	 * Tries to center and zoom to single team position
	 *
	 * @param {Number} teamId
	 */
	focusOnTeam: function (teamId) {
		if(typeof teamId === 'undefined') return;

		const position = Ext.getStore('Positions').getById(teamId);

		// teams which are not active anymore
		// must not be centered
		// same goes for teams which do not have positions attached to them
		if (position === null || !position.isActive()) return;

		this.panAndZoomTo(
			position.get('Longitude'),
			position.get('Latitude'),
			this._closeZoomLevel
		);
	},

	/**
	 * Tries to center and zoom to single team position based on satrter number
	 *
	 * @param {Number} starterNo
	 */
	focusOnStarter: function (startNo) {
		var position = Ext.getStore('Positions').findRecord('StartNumber', startNo, 0, false, true, true);

		// teams which are not active anymore
		// must not be centered
		// same goes for teams which do not have positions attached to them
		if (position === null) return;

		var teamId = position.get('TeamId'),
			team = Ext.getStore('Teams').getById(teamId);

		team.set('IsFollowed', true);
		team.set('Enabled', true);

		this.getView().fireEvent('teamDetailsRequested', teamId);

		if (!position.isActive()) return;

		this.panAndZoomTo(
			position.get('Longitude'),
			position.get('Latitude'),
			this._closeZoomLevel
		);
	},

	/**
	 * Tries to center and zoom to single team position based on satrter number.
	 * Rest of teams gets filtered out and chosen one will be followed.
	 *
	 * @param {Number} starterNo
	 */
	focusOnSoleStarter: function (startNo) {
		var position = Ext.getStore('Positions').findRecord('StartNumber', startNo, 0, false, true, true);

		if (position === null) return;

		var teamId = position.get('TeamId'),
			teams = Ext.getStore('Teams'),
			team = teams.getById(teamId),
			filter = this.getSoleTeamFilter();

		team.set('IsFollowed', true);
		team.set('Enabled', true);

		filter.setTeamId(teamId);
		FilterHelper.applyFilter(filter);

		this.getView().fireEvent('teamDetailsRequested', teamId);

		if (!position.isActive()) return;

		this.panAndZoomTo(
			position.get('Longitude'),
			position.get('Latitude'),
			this._closeZoomLevel
		);
	},

	focusOnFixPoint: function (fixPointId) {
		var fixPoint = Ext.getStore('FixPoints').getById(fixPointId);

		if (fixPoint === null) return;

		this.panAndZoomTo(
			fixPoint.get('Longitude'),
			fixPoint.get('Latitude'),
			this._closeZoomLevel
		);
	},

	focusOnCoords: function (lng, lat) {
		this.panAndZoomTo(lng, lat, this._closeZoomLevel);
	},

	/**
	 * Centers and zooms to the specified point
	 *
	 * @param {Number} lng
	 * @param {Number} lat
	 * @param {Number} targetZoom
	 */
	panAndZoomTo: function (lng, lat, targetZoom) {
		var map = this.getMap(),
			currentZoom = map.getZoom();

		// if the user has already zoomed closer than the target zoom
		// we keep this zoom level
		map.setView([lat, lng], (currentZoom >= targetZoom) ? currentZoom : targetZoom, {
			animate: true
		})
	}
});
