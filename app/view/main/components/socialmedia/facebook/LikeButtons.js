Ext.define("TractalisWebclient.view.main.components.socialmedia.facebook.LikeButtons", {
    extend: "Ext.container.Container",
    alias: 'widget.facebook-like-buttons',

    id: 'facebook-like-buttons',

    config: {
        cls: 'facebook-like-buttons',
        html: '<div id="fb-like-us-buttons" class="fb-like" data-href="https://facebook.com/tractalis" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>'
    },

    initComponent: function () {
        var me = this;

        me.callParent(arguments);

        me.on('afterrender', function() {
            FacebookApiResolver.resoveApi()
                .catch(me.onError.bind(me));
        });
    },

    onError: function (msg) { // well... shiet... it won't load
        console && console.error('Facebook like buttons won\'t load')
    }
});