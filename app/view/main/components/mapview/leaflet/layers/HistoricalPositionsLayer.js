// TODO: maybe rename history to track
Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.HistoricalPositionsLayer", {
	alternateClassName: 'HistoricalPositionsLayer',

	config: {
		/**
		 * Store holding position histories of each team.
		 */
		positionHistories: null,

		/**
		 * Store holding teams.
		 */
		teams: null,

		layer: null,

		visiblePaths: {},

		/**
		 * @cfg {Object} visibleWaypoints
		 * Container for visible waypoints to show the historical positions
		 */
		visibleWaypoints: {},

		/**
		 * @cfg {Object} waypointMarkerOptions
		 * Visual properties for waypoint marker object
		 */
		waypointMarkerOptions: {
			className: 'tractalis-team-position-marker',
			color: '#FFFFFF',
			weight: 1.5,
			pane: 'markerPane',
			radius: 4,
			fillOpacity: 1,
			fillColor: '#009700'
		},

		/**
		 * @cfg {Object} waypointPopupOptions
		 * Options for waypoints popup
		 */
		waypointPopupOptions: {
			className: 'tractalis-historical-waypoint-popup'
		},

		layerOptions: {
			className: 'tractalis-historical-positions-path',
			/**
			 * Function converting feature to style.
			 */
			style: function (feature) {
				var style = {},
					props = feature.properties;

				if (props) {
					if (props['stroke']) style.color = '#' + props['stroke'];
					if (props['stroke-width']) style.width = props['stroke-width'];
					if (props['stroke-opacity']) style.opacity = props['stroke-opacity'];
				}
				return style;
			}
		},

		popupOptions: {
			className: 'tractalis-historical-positions-popup'
		}
	},

	_showWaypointsAtLevel: Number.MAX_VALUE,
	_waypointsShown: false,
	_map: null,

	constructor: function (options) {
		this.initConfig(this.config);

		this.setLayer(options.layer);
		this.setPositionHistories(options.positionHistories);
		this.setTeams(options.teams);

		this._map = options.map;
		this._showWaypointsAtLevel = Configuration.get('gis/showWaypointsAtLevel', -1);
		this._waypointsShown = this._showWaypointsAtLevel <= this._map.getZoom();

	},

	/**
	 * Sets up this particular layer
	 * @returns {*}
	 */
	setUpLayer: function () {
		var me = this;

		return new Promise(function (resolve, reject) {
			try {
				var initialyVisibleTeams = me.collectVisibleTeams(me.getTeams());

				for (var id in initialyVisibleTeams) {
					me.showTeamHistory(initialyVisibleTeams[id]);
				}

				var teams = me.getTeams();
				var positionHistories = me.getPositionHistories();

				teams.on('update', me.updateFiltering, me);
				teams.on('filterchange', me.teamFilteringChanged, me);

				positionHistories.on('load', me.onHistoriesLoaded, me);

				me._map.on('zoom', () => {
					if(me._showWaypointsAtLevel <= me._map.getZoom()){
						me._waypointsShown || me.showWaypoints();
						me._waypointsShown = true;
						
					} else {
						me._waypointsShown && me.hideWaypoints();
						me._waypointsShown = false;
					}
				});

				resolve(true);
			} catch (err) {
				// TODO: introduce some error msg instead of this crappy log
				console && console.error('HistoricalPositionsLayer.setUpLayer(): Error', err);
			}
		});
	},

	showWaypoints: function() {
		const layer = this.getLayer();
		const waypoints = this.getVisibleWaypointMarkers();
		const len = waypoints.length;

		for (let i = 0; i < len; i++) {
			waypoints[i].addTo(layer);
		}
	},

	hideWaypoints: function () {
		const waypoints = this.getVisibleWaypointMarkers();
		const len = waypoints.length;

		for (let i = 0; i < len; i++) {
			waypoints[i].remove();
		}
	},

	getVisibleWaypointMarkers: function () {
		const visibleWaypoints = this.getVisibleWaypoints();

		return Object.getOwnPropertyNames(visibleWaypoints)
			.reduce((acc, teamId) => {
				const w = Object.entries(visibleWaypoints[teamId]).reduce((_, [k, v]) => {
					acc.push(v);
					return acc;
				});
				Array.prototype.push.apply(acc, w);
				return acc;
			}, []);
	},

	onHistoriesLoaded: function (store, records, successfull) {
		if (!successfull) return;

		const visiblePaths = this.getVisiblePaths();
		const visibleRecs = records.filter(function (rec) {
			return visiblePaths[rec.getId()] !== undefined;
		});
		const len = visibleRecs.length;

		for (let i = 0; i < len; i++) {
			this.showTeamHistoryRec(visibleRecs[i]);
		}
	},

	// just hacking my way through... don't judge me :P
	updateFiltering: function (store, team, operation, modifiedFieldNames) {
		if (modifiedFieldNames.indexOf('HistoryVisible') > -1) {
			const histories = Ext.getStore('PositionHistories');
			const teamId = team.getId();
			const proxy = histories.getProxy();
			let teams = Object.getOwnPropertyNames(this.getVisiblePaths());
			let filter = '';

			if (histories.isLoading()) {
				setTimeout(() => {
					this.updateFiltering(store, team, operation, modifiedFieldNames);
				}, 0);
				proxy.abort();

				return;
			}

			if (team.get('HistoryVisible')) {
				teams.push(teamId);
				filter = '(TrackableObjectId eq ' + teams.join(' or TrackableObjectId eq ') + ')';

				proxy.resetCache();
				proxy.setFilter(filter);

				histories.load();

				this.showTeamHistory(team);
			} else {
				this.hideTeamHistory(teamId);
				teams = teams.filter(id => id != teamId);

				if (teams.length > 0) {
					filter = '(TrackableObjectId eq ' + teams.join(' or TrackableObjectId eq ') + ')';
				} else {
					proxy.resetCache();
					PositionHistoryReceiver.stopFetching();
				}

				proxy.setFilter(filter); // no need to reload here
			}
		}
	},

	showTeamHistory: function (team) {
		var positionHistory = this.getPositionHistories().getById(team.getId());
		if (positionHistory) {
			this.showTeamHistoryRec(positionHistory, team);
		} else {
			this.addEmptyPath(team);
		}
	},

	showTeamHistoryRec: function (history, team) {
		var me = this,
			visiblePaths = me.getVisiblePaths(),
			teamId = history.getId(),
			latLons = me.toPolyLines(history),
			teamPath = visiblePaths[teamId],
			popup;

		if (!team) {
			team = me.getTeams().getById(history.getId());
		}

		if (teamPath !== undefined) {
			// update
			// it is easier from code to replace entire path
			teamPath.setLatLngs(latLons);
		} else {
			// simply show
			teamPath = L.polyline(latLons);

			popup = L.popup(me.getPopupOptions());
			popup.setContent('<strong>' + team.get('StartNumber') + ' ' + team.get('Name') + '</strong>');

			teamPath.bindPopup(popup);

			visiblePaths[teamId] = teamPath;
			teamPath.addTo(me.getLayer());
		}

		const visibleWaypoints = me.getVisibleWaypoints();

		history.get('HistoricalPositions').forEach(function (historicalPosition) {
			if (historicalPosition.TrackerState === 0 && (me.isValid(historicalPosition) &&
					(typeof visibleWaypoints[teamId] === 'undefined' || typeof visibleWaypoints[teamId][historicalPosition.Id] === 'undefined'))) {
				me.addWaypoint(historicalPosition, team);
			}
		});
	},

	/**
	 * Add waypoint for the history
	 * @param history
	 * @param team
	 */
	addWaypoint: function (waypoint, team) {
		var me = this,
			visibleWaypoints = me.getVisibleWaypoints(),
			coordinates = [
				waypoint.Latitude,
				waypoint.Longitude
			],
			teamId = team.getId(),
			marker = L.circleMarker(coordinates, me.getWaypointMarkerOptions()),
			timeStamp = moment(waypoint.TimeStamp).utc().format('D.M HH:mm:ss');

		popup = L.popup(this.getWaypointPopupOptions());
		popup.setContent('<p><strong>' + team.get('StartNumber') + ': ' + team.get('Name') + '</strong></p>' + timeStamp + ' UTC');

		marker.bindPopup(popup);

		if (typeof visibleWaypoints[teamId] === 'undefined') {
			// not existing yet, create empty array for that particular team
			visibleWaypoints[teamId] = {};
		}

		if (typeof visibleWaypoints[teamId][waypoint.Id] === 'undefined') {
			visibleWaypoints[teamId][waypoint.Id] = marker;

			if (this._showWaypointsAtLevel <= this.getCurentZoom()) {
				marker.addTo(me.getLayer());
			}
		}
	},

	hideTeamHistory: function (teamId) {
		var me = this,
			visiblePaths = me.getVisiblePaths(),
			teamPath = visiblePaths[teamId],
			visibleWaypoints = me.getVisibleWaypoints(),
			teamWaypoints = visibleWaypoints[teamId],
			layer = this.getLayer();

		if (teamPath) {
			me.getLayer().removeLayer(teamPath);
			delete visiblePaths[teamId];
		}

		if (typeof teamWaypoints !== 'undefined') {
			Object.getOwnPropertyNames(teamWaypoints).forEach(propName => {
				layer.removeLayer(teamWaypoints[propName]);
				delete teamWaypoints[propName];
			});

			delete visibleWaypoints[teamId];
		}
	},

	isValid: function (historicalPosition) {
		return historicalPosition.Latitude !== 0 || historicalPosition.Longitude !== 0;
	},

	toPolyLines: function (history) {
		var me = this,
			polies = history.get('HistoricalPositions').reduce(function (acc, histPos) {
				if (histPos.TrackerState === 0) {
					if (me.isValid(histPos)) {
						acc.polies[acc.currentPoly].push([histPos.Latitude, histPos.Longitude]);
						acc.currLen++;
					}
					// switched ON/OFF
				} else if ((histPos.TrackerState === 1 || histPos.TrackerState === 2) && acc.currLen > 0) { // don't start new poly when current is empty
					acc.polies.push([]);
					acc.currLen = 0;
					acc.currentPoly++;
				}

				return acc;
			}, {
				polies: [
					[]
				],
				currentPoly: 0,
				currLen: 0
			});

		return (polies.currentPoly === 0) ? polies.polies[0] : polies.polies;
	},

	addEmptyPath: function (team) {
		//simply show
		var visiblePaths = this.getVisiblePaths(),
			teamPath = L.polyline([]),
			popup = L.popup(this.getPopupOptions());

		popup.setContent('<strong>' + team.get('StartNumber') + ' ' + team.get('Name') + '</strong>');

		teamPath.bindPopup(popup);

		visiblePaths[team.getId()] = teamPath;
		teamPath.addTo(this.getLayer());
	},

	teamFilteringChanged: function (store, filters) {
		var me = this,
			teamsToShow = me.collectVisibleTeams(store),
			visiblePaths = me.getVisiblePaths(),
			id;

		for (id in visiblePaths) {
			// not anymore in teams which should be shown
			if (teamsToShow[id] === undefined) {
				me.hideTeamHistory(id);
			} else { // it is already out there, visible. So remove it from teams to show
				delete teamsToShow[id];
			}
		}

		// add what left to show
		for (id in teamsToShow) {
			me.showTeamHistory(teamsToShow[id]);
		}
	},

	collectVisibleTeams: function (teamsStore) {
		var visibleTeams = {};

		teamsStore.each(function (team) {
			if (team.get('HistoryVisible')) {
				visibleTeams[team.getId()] = team;
			}
		});

		return visibleTeams;
	},

	getCurentZoom: function () {
		return this._map ? this._map.getZoom() : -1
	}
});