/**
 * Grid used to render Teams in a hierarchical view.
 */
Ext.define("TractalisWebclient.view.main.components.teams.list.TeamList", {
	extend: "Ext.grid.Panel",
	alias: "widget.team-list",
	requires: [
		'TractalisWebclient.view.main.components.teams.list.TeamListController',
		'TractalisWebclient.view.main.components.teams.list.TeamListModel',

		"Ext.grid.column.Check"
	],
	id: 'teamList',

	controller: "main-components-teams-list-teamlist",
	viewModel: {
		type: "main-components-teams-list-teamlist"
	},

	viewConfig: {
		/**
		 * @cfg {Boolean} preserveScrollOnRefresh
		 * Specifies that the grid should keep it scrolling position when store is refreshed
		 */
		preserveScrollOnRefresh: true,

		/**
		 * @cfg {Boolean} markDirty
		 * Specifies that the grid should not mark the edited values
		 */
		markDirty: false
	},

	features: [{
		ftype: 'grouping',
		groupHeaderTpl: '{name}'
	}],

	store: 'Teams',

	header: {
		xtype: 'team-filter'
	},

	columns: [{
		xtype: 'checkcolumn',
		// text: 'Enable',
		dataIndex: 'Enabled',
		width: '10%',
		menuDisabled: true,
		sortable: false,
		draggable: false
	}, {
		xtype: 'checkcolumn',
		text: 'Hist',
		dataIndex: 'HistoryVisible',
		width: '10%',
		menuDisabled: true,
		sortable: false,
		draggable: false,

		bind: {
			hidden: '{!historicalWaypointsVisible}'
		}
	}, {
		text: 'R',
		dataIndex: 'Rank',
		width: '10%',
		hidden: !Configuration.get('race/includeRanks'),

		/**
		 * @private
		 * Renders the column
		 * @param {Number} rank
		 */
		renderer: function (rank) {
			if (rank === 0 || rank === Number.MAX_VALUE) return '-';

			return rank;
		}
	}, {
		text: 'Nr',
		dataIndex: 'StartNumberSortable',

		/**
		 * @private
		 * Renders the Start number as actual value
		 * @param {Number} value
		 * @param {Object} metaData
		 * @param {Ext.data.Record} record
		 */
		renderer: function (value, metaData, record) {
			return record.get('StartNumber');
		},
		width: '20%'
	}, {
		text: 'Name',
		dataIndex: 'Name',
		flex: 1
	}]
});
