/*
 * This file contains the whole configuration for this instance of the client
 */
var TractalisWebConfiguration = {
	app: {
		// path to app logo (upper left corner)
		logo: 'https://tractalisstorage.blob.core.windows.net/events/Resources/Common/html5/Trans-100-256-01-ws-html5.png'
	},

	race: {
		// name of the race
		name: '',

		// path to Racelogo
		logo: 'https://tractalisstorage.blob.core.windows.net/events/2013_4/Gigathlon2016/Resources/logo.png',

		// url of race website, will be set to link under race logo
		website: '',

		// the RaceID - this is used for webservice requests
		// The ID of the Demo-Event is 12
		id: 84,

		// defines whether the race should use
		// the metric system to render distances, speed, etc.
		isMetric: true,

		// true if additionally the swiss grid coordinates should be included into team details
		// this is only relevant for swiss events
		includeSwissgrid: true,

		// true if ranks should be included, otherwhise false
		includeRanks: true,

		// a list of sponsors which are rendered in the sponsorview
		// on the right side
		sponsors: [{
			name: 'Main Sponsorzzzz',
			url: 'https://www.sierranevada.com/',
			image: 'https://tractalisstorage.blob.core.windows.net/events/Resources/RAAM2014/sponsors/Sierra-Nevada_website.png',

			// main sponsor is added below headerbar and is always visible.
			// the others will be only visible on an additional window which will be positioned
			// on the right side
			isMain: true
		}, {
			name: 'Sierra Nevada',
			url: 'https://www.sierranevada.com/',
			image: 'https://tractalisstorage.blob.core.windows.net/events/Resources/RAAM2014/sponsors/Sierra-Nevada_website.png'
		}, {
			name: 'Sierra Nevada',
			url: 'https://www.sierranevada.com/',
			image: 'https://tractalisstorage.blob.core.windows.net/events/Resources/RAAM2014/sponsors/Sierra-Nevada_website.png'
		}, {
			name: 'Sierra Nevada',
			url: 'https://www.sierranevada.com/',
			image: 'https://tractalisstorage.blob.core.windows.net/events/Resources/RAAM2014/sponsors/Sierra-Nevada_website.png'
		}, {
			name: 'Sierra Nevada',
			url: 'https://www.sierranevada.com/',
			image: 'https://tractalisstorage.blob.core.windows.net/events/Resources/RAAM2014/sponsors/Sierra-Nevada_website.png'
		},
		{
			"name": "swisstopo: Landeskarten (farbig)",
			kind: 'wms',
			url: 'https://wms.geo.admin.ch/?SERVICE=WMS',
			config: {
				layers: 'ch.vbs.ch.swisstopo.pixelkarte-farbe',
				format: 'image/jpeg',
				transparent: false,
				attribution: '©swisstopo',
				version: '1.3.0'
			},
		},
		{
			"name": "swisstopo: SWISSIMAGE",
			kind: 'wms',
			url: 'https://wms.geo.admin.ch/?SERVICE=WMS',
			config: {
				layers: 'ch.swisstopo.swissimage',
				format: 'image/jpeg',
				transparent: false,
				attribution: '©swisstopo',
				version: '1.3.0'
			},
		}],

		// when there is only one participant
		// this participant will automatically be followed ant the UI will be fit according to that
		hasSingleParticipant: false,

		hqDecoration: 'E!gerhq'
	},

	webservice: {
		// refresh interval in milliseconds
		interval: 30 * 1000
	},

	onStartup: {
		// disables all teams on initial load or when no filter was set
		disableAllTeams: false
	},

	historyWaypoints: {
		// defines which permissions are nessecary
		// to have access to history waypoints
		permissions: [
			'public',
			'headquarter'
		]
	},

	elevation: {
		// Controls whether elevation chart should be shown at all.
		// default: true
		enabled: true,

		// overrides total distance shown on x axis,
		// units: km
		scaleDistanceTo: 198.459, // good practice: take race track distance from track for server
		bgImage: 'https://tractalisstorage.blob.core.windows.net/events/Resources/Common/html5/palegray_barelogo.svg'
	},

	socialMedia: {
		twitter: {
			widgetId: '828999336017195013',
			showWidgetHeader: true,
			active: true
		},
		facebook: {
			url: 'https://www.facebook.com/tractalis'
		}
	},

	details: {
		// switches details view into raw tracking mode
		isRaw: false
	},

	gis: {
		// base layers which should be used on map, each entry is composed of:
		// name - name of esri.leaflet base layer name.
		// At the time being esri exposes follwing base layers:
		// [ Streets, Topographic, NationalGeographic, Oceans, Gray, DarkGray, Imagery, ShadedRelief, Terrain ]
		// active - [optional] whether automatically set it as active one, do not set more than one can be true/false
		// labels - [optional] whether to add labels to this layer.
		// At the time being esri provides labels for layers:
		// [ Oceans, Gray, DarkGray, Imagery, ShadedRelief, Terrain ]
		baseLayers: [{
			name: 'Topographic',
			active: true
		}, {
			name: 'Gray',
			labels: true
		}, {
			name: 'DarkGray',
			labels: true
		}, {
			name: 'Imagery',
			labels: true
		}, {
			name: 'NationalGeographic'
		}],

		// the zoom level which causes the closeup icons to show
		closeZoomLevel: 14,
		showWaypointsAtLevel: 10,
		animateMovement: true,
		animationLength: 300,

		// the scale value which is used to render
		// closeup stuff based on scale instead of zooms
		closeScale: 73000,

		// defines the initial position & zoom on GIS
		initial: {
			position: {
				lng: 8.713685,
				lat: 46.5062716
			},
			zoom: 9
		},

		// dynamic layering
		layers: [{
			name: "wolkenlayer",
			tilesUrl: ""
		}],

		enableExternalLayers: true,

		// All KML Layers are defined here,
		// supported format types are: 'csv', 'kml', 'gpx', 'wkt', 'topojson', 'polyline', 'geojson'
		// If type not provided KML will be assumed
		// only4Hq if set to true it will be visible only in hq mode in layers picker. By default it will be not selected.
		layers: [{
			name: 'Race Track Sat',
			type: 'kml',
			only4Hq: false,
			url: '//tractalisstorage.blob.core.windows.net/events/2013_4/Gigathlon2016/route_sat.kml'
		}]
	}
};