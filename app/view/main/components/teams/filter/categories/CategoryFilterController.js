Ext.define('TractalisWebclient.view.main.components.teams.filter.categories.CategoryFilterController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.main-components-teams-filter-categories-categoryfilter',

	control: {
		'#uiToggleAll': {
			toggleAll: 'toggleAllCategories'
		}
	},

	/**
	 * Initializes this view controller
	*/
	init: function() {
		var me = this;

		// attach the update handler to listen to 'disable' / 'enable'-changes
		me.getView().getTeamCategoryStore().on('endupdate', me.storeEndUpdate, me);
	},

	/**
	 * Toggles all categories per once
	 * @param {Boolean} enabled True if all categories should be enabled at once
	*/
	toggleAllCategories: function(enabled){
		this.getView().getTeamCategoryStore().enableAll(enabled);
	},

	/**
	 * A specific entry in the team category store has changed
	 *
	 * @param {Ext.data.Store} store The store which was changed, in this case the teamCategoryStore
	*/
	notifyChanges: function(store) {
		var me = this,
			categories = [];

		store.each(function(record) {
			if (record.get('Enabled')) {
				// we use the Name aka ID to identify it lateron
				// so only add this one, not the whole record instance
				categories.push(record.getId());
			}
		});

		me.getView().fireEvent('filtered', categories, store.getCount());
	},

	/**
	 * A batch update was done on the store, handle it
	*/
	storeEndUpdate: function(){
		var me = this;

		me.notifyChanges(me.getView().getTeamCategoryStore());
		me.reloadUi();
	},

	/**
	 * Reloads the UI because of external changes
	*/
	reloadUi: function(){
		// make sure that the grid is recreated again
		// bc. the store is not updated field per field
		Ext.getCmp('uiCategories').reconfigure();
	}
});