Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.FixPointsCreator", {
	alternateClassName: 'FixPointsCreator',

	requires: [
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.Labeller'
	],

	config: {
		mapContainer: null,
		markerOptions: {
			className: 'tractalis-fix-point-marker',
			color: '#221e1e',
			pane: 'markerPane',
			opacity: 0.8,
			fillOpacity: 0.5,
			radius: 5,
			weight: 2
		},
		labelOptions: {
			direction: 'auto',
			noHide: true,
			interactive: true,
			pane: 'markerPane',
			className: 'fixpoint-label',
			permanent: true
		}
	},

	constructor: function(options) {
		this.initConfig();
		this.setMapContainer(options.mapContainer);
	},

	setUpLayer: function(fixPointsLayer) {
		return new Promise(function(resolve, reject) {
			var loadBarrierInfo = [{
				identifier: 'fixpoints',
				stores: [Ext.getStore('FixPoints')],
				fnc: function(fixPointsStore, fixPointLayer) {
					this.generateFixPointMarkers(fixPointsStore, fixPointLayer);
					resolve(true);
				}
			}];

			try {
				StoreLoadingBarier.mapStores(loadBarrierInfo, this, fixPointsLayer);
			} catch (err) {
				console && console.er('FixPointsCreator.setUpLayer(): Error', err);
			}
		}.bind(this));
	},

	generateFixPointMarkers: function(fixPointsStore, fixPointLayer) {
		fixPointsStore.each(this.addFixPointMarker.bind(this, fixPointLayer));
	},

	addFixPointMarker: function(fixPointLayer, fixPoint) {
		this.fixPointToMarker(fixPoint).addTo(fixPointLayer);
	},

	fixPointToMarker: function(fixPoint) {
		var position = [fixPoint.get('Latitude'), fixPoint.get('Longitude')],
			marker = L.circleMarker(position, this.getMarkerOptions());

		// TODO: popup with fancy text
		var popup = L.popup({
			className: 'tractalis-fix-point-popup fix-point-popup',
			minWidth: 600
		});

		popup.setLatLng(position);
		popup.setContent(this.fixpointTemplate(fixPoint));

		marker.bindPopup(popup);

		Labeller.attachLabel(marker, this.getLabelContents(fixPoint), this.getLabelOptions(), this.getMapContainer());

		marker.getTooltip().on('click', function(e) {
			marker.openPopup();
		});

		return marker;
	},

	getLabelContents: function(fixPoint) {
		const name = fixPoint.get('ShortName');
		if (name) return name;

		var fpType = fixPoint.get('Type');
		if (fpType === 0) {
			return 'TS' + fixPoint.get('Id');
		}

		return fixPoint.get('TypeName');
	},

	/**
	 * Renders a HTML representation of the specified fixpoint
	 *
	 * @param {TractalisWebclient.model.FixPoint} fixPoint
	 * @returns {String} String representation as HTML
	 */
	fixpointTemplate: function(fixPoint) {
		var template = new Ext.XTemplate(
			'<h2>{name}</h2>' +

			'<table>' +
			'<tr>	<td>Altitude</td>	<td>{altitude}</td>	</tr>' +
			'<tr>	<td>Name</td> 		<td>{name}</td>		</tr>' +
			'</table>' +

			'<iframe frameborder="0" height="245" width="100%" src="https://forecast.io/embed/#lat={lat}&lon={long}&name={name}&color=#00aaff&font=Georgia&units=uk"></iframe>'
		);

		return template.apply({
			name: fixPoint.get('Name'),

			altitude: fixPoint.get('Altitude'),
			long: fixPoint.get('Longitude'),
			lat: fixPoint.get('Latitude')
		});
	}
});