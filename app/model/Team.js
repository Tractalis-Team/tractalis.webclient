/**
 * This Model represents single Teams which are rendered in the Tree.
 */
Ext.define('TractalisWebclient.model.Team', {
	extend: 'Ext.data.Model',

	idProperty: 'TeamId',

	fields: [
		{ name: 'TeamId', type: 'int' },
		{ name: 'Name', type: 'auto' },
		{ name: 'CategoryName', type: 'string' },
		{ name: 'CategoryType', type: 'int' },
		{ name: 'Rank', type: 'int', defaultValue: Number.MAX_VALUE },
		{ name: 'IsFollowed', type: 'boolean' },
		{ name: 'Public', type: 'boolean' },
		{ name: 'SortingPriority', type: 'int' },
		{ name: 'Enabled', type: 'boolean', defaultValue: !Configuration.get('onStartup/disableAllTeams') },
		{ name: 'position', type: 'auto', defaultValue: null },
		{ name: 'StartNumberSortable', mapping: 'StartNumber',  type: 'int', defaultValue: 0 },
		{ name: 'Distance', type: 'float', defaultValue: 0 },
		{ name: 'BacksTeam', type: 'int', mapping: 'Description', defaultValue: -1 },
		{ name: 'HistoryVisible', type: 'boolean', defaultValue: false }

		// associated values which are derived from position updates are added on the fly
		// check app/backend/PositionReceiver.js the method called 'synchronizeFromPosition'
	],

	/**
	 * Tells whether this teams is backup for other team.
	 */
	isBackup: function() {
		return this.get('BacksTeam') > 0;
	}
});
