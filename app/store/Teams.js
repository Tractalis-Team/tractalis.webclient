/**
 * This store represents all Teams which are active for the current specified race.
 */
Ext.define('TractalisWebclient.store.Teams', function() {
	const sortByField = Configuration.get('race/includeRanks') ? 'Rank' : 'StartNumberSortable';
	let resortRaf = null;

	return {
		extend: 'Ext.data.Store',
		model: 'TractalisWebclient.model.Team',
		autoLoad: true,
		pageSize: 0,

		/**
		 * @event changeEnableState
		 * Is fired when a the enabled state OF ALL elements is changed ina row
		 * @param {Boolean} enabled True if enabled, otherwhise false
		 */
		grouper: {
			property: 'CategoryName',
			/**
			 * @private
			 * Is called to sort between two teams for the grouping
			 *
			 * @param {TractalisWebclient.model.Team} selfTeam The team nr 1
			 * @param {TractalisWebclient.model.Team} otherTeam The team nr 2
			 * @return {Number} 1 if team 1 is higher positioned, 0 if equal, -1 if team 1 is lower positioned
			 */
			sorterFn: function(lhs, rhs) {
				return this.compareBy('SortingPriority', lhs, rhs) || this.compareBy(sortByField, lhs, rhs);
			},
	
			compareBy: function (field, lhs, rhs) {
				const lVal = lhs.get(field),
					rVal = rhs.get(field);
	
				return (lVal > rVal) ? 1 : (lVal === rVal ? 0 : -1);
			}
		},
		sorters: sortByField,
		autoSort: true,
	
		proxy: {
			type: 'azure-tables',
			tableName:  AzureTablesProxy.buildTableName('Event', Configuration.get('race/id'), 'TeamData'),
			filter: '(Public eq true)',
			sasKey: 'TeamData',
	
			reader: {
				type: 'json',
				rootProperty: 'value',
				successProperty: false
			}
		},
	
		listeners: {
			/**
			 * Extracts the members into single
			 * 
			 * @param {Ext.data.Store} store
			 * @param {Array} records
			 * @param {Boolean} successfull
			 * @param {Ext.data.Operation} operation
			 */
			load: function(store, records, successfull, operation) {
				var participants = Ext.getStore('Participants'),
					currentYear = new Date().getFullYear(),
					teamCategories = Ext.getStore('TeamCategories'),
					members = [];
	
				// go through each team-record
				Ext.each(records, function(record) {
					// first of all try to extract the category to create a new entity in the teamcategories-store
					let catName = record.get('CategoryName');
					if (teamCategories.getById(catName) === null) {
						// not added yet ... add it initially and gweh
						teamCategories.add({
							Name: catName,
							SortingPriority: record.get('SortingPriority')
						});
					}
	
					// we store the names of all members
					// to have the fulltext-search lateron possible
					var memberNames = '';
	
					// we can have maximum 10 2D entries in one
					// team entry
					for (var i = 1; i <= 10; i++) {
						var memberName = record.get('Member' + i + 'Name');
	
						if (typeof memberName !== 'undefined') {
							var memberBirthday = record.get('Member' + i + 'BirthDate'),
								countryCode = record.get('Member' + i + 'CountryShort');
	
							var member = Ext.create('TractalisWebclient.model.Participant', {
								Name: memberName,
								Birthday: memberBirthday,
								CountryCode: countryCode,
	
								// keep reference to team
								TeamName: record.get('Name'),
								TeamId: record.get('TeamId')
							});
	
							// we cache the age of the member at this point already
							// because this won't change and it doesn't make sense to calculate it later
							memberBirthday = member.get('Birthday');
							// it's possible that the birthday field is null
							if (memberBirthday !== null) {
								member.set('age', currentYear - memberBirthday.getFullYear(), {
									silent: true
								});
							}
	
							memberNames += memberName;
							members.push(member);
						} else {
							// skip execution, no following members coming
							break;
						}
					}
	
					// assign the names into one single field
					// for the fulltext-search lateron
					record.set('memberNames', memberNames.toLowerCase(), {
						silent: true
					});
				});
	
				if (members.length > 0) {
					participants.add(members);
				}
			}
			,
			update: {
				// hack... seems like edits no longer cause store to resort
				fn: function (me, rec, op, modifiedFieldNames) {
					if (!modifiedFieldNames || modifiedFieldNames.indexOf(sortByField) > -1) {
						resortRaf = resortRaf || requestAnimationFrame(() => {
							me.sort();
							resortRaf = null;
						});
					}
				},
				options: {
					buffer: 300
				}
			}
		},
	
		/**
		 * This methods toggles the 'Enabled'-Field of all entries to true / false at once
		 *
		 * @param {Boolean} enabled True if all should be enabled, otherwhise false
		 */
		enableAll: function(enabled) {
			var me = this;
	
			me.beginUpdate();
	
			me.each(function(record) {
				// if not set yet, we set it
				if (record.get('Enabled') !== enabled) {
					record.set('Enabled', enabled, {
						silent: true
					});
				}
			}, me);
	
			me.endUpdate();
			me.fireEvent('changeEnableState', me, enabled);
		}
	};
});