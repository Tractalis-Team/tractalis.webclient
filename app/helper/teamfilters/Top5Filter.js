/**
 * This class is used as Base-Class for each Command which should be able to synchronize via
 * the Global Message Queue across different instances of the client.
 */
Ext.define('TractalisWebclient.helper.teamfilters.Top5Filter', {
	extend: 'StoreFilter',
	alternateClassName: 'Top5Filter',

	config: {
		storeName: "Teams",

		fnc: function(record) {
			return record.get('Rank') <= 5;
		}
	},

	restore: function() {
		Ext.GlobalEvents.fireEvent('restoreTop5', this.getEnabled());
	},

	toOdataFilter: function(){
		return '(Rank gt 0) and (Rank lt 6)';
	}
});
