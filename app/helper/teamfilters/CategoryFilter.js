/**
 * This class is used as Base-Class for each Command which should be able to synchronize via
 * the Global Message Queue across different instances of the client.
 */
Ext.define('TractalisWebclient.helper.teamfilters.CategoryFilter', {
	extend: 'StoreFilter',
	alternateClassName: 'CategoryFilter',

	config: {
		/**
		 * @cfg {Array} categoryNames
		 * Contains a list of category names which should be visible after applying filter
		 */
		categoryNames: [],

		storeName: "Teams",

		fnc: function(record) {
			var categories = this.getCategoryNames();

			// when no categories were specified, we dont need to check
			// because we know for sure that the category is not listed
			if (categories.length === 0) {
				return false;
			}

			// check if the categoryName of the record could be found in the
			// categories array which tells us which categories are enabled rightnow
			return categories.indexOf(record.get('CategoryName')) > -1;
		}
	},

	/**
	 * Restores this filter
	 */
	restore: function() {
		var store = Ext.getStore('TeamCategories'),
			categories = this.getCategoryNames();

		store.each(function(record) {
			var enabled = categories.indexOf(record.get('Name')) > -1;

			record.set('Enabled', enabled, {
				silent: true
			});
		});
	},

	serialize: function() {
		var me = this;

		return me.addProperties(me.callParent(), {
			categoryNames: me.getCategoryNames()
		})
	}
});