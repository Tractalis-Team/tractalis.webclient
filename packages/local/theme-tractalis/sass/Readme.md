# theme-tractalis/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    theme-tractalis/sass/etc
    theme-tractalis/sass/src
    theme-tractalis/sass/var
