/**
 * Handles the 'follow team'-usecase
 */
Ext.define('TractalisWebclient.helper.TeamFollower', {
	singleton: true,
	alternateClassName: 'TeamFollower',

	config: {
		/**
		 * @cfg {TractalisWebclient.model.Team} followedTeam
		 * The team which is followed rightnow.
		 */
		followedTeam: null,

		/**
		 * @cfg {Ext.data.Store} teamStore
		 * Store which contains the team store.
		*/
		teamStore: null
	},

	/**
	 * @private
	 * @returns {undefined}
	 */
	constructor: function() {
		var me = this;

		// create the automatic properties
		me.initConfig(me.config);
	},

	/**
	 * Is called when the team store is updated
	 *
	 * @param {Ext.data.Store} store
	*/
	updateTeamStore: function(store){
		store.on('update', this.teamWasUpdated, this);
	},

	/**
	 * Team record was updated
	 *
	 * @param {Ext.data.Store} store
	 * @param {Ext.data.Model} record
	 * @param {String} operation
	 * @param {String[]} modifiedFieldNames
	*/
	teamWasUpdated: function(store, record, operation, modifiedFieldNames){
		if (modifiedFieldNames.indexOf('IsFollowed') > -1){
			this.setFollowedTeam(record.get('IsFollowed') ? record : null);
		}
	},

	/**
	 * @private
	 * Applies the value for the followed team property
	 *
	 * @param {TractalisWebclient.model.Team} team
	 * @returns {TractalisWebclient.model.Team}
	 */
	applyFollowedTeam: function(team) {
		// unfollow the old team which was followed
		// ... if there was one
		if (this.getFollowedTeam() !== null) {
			this.getFollowedTeam().set('IsFollowed', false);
		}

		return team;
	},

	/**
	 * @private
	 */
	updateFollowedTeam: function(team) {
		if (team !== null) {
			team.set('IsFollowed', true);
		}
	},

	/**
	 * Checks whether the specified team belongs to the current followed team
	 *
	 * @param {Number} teamId
	 * @returns {Boolean} true if yes, false if not
	 */
	isFollowed: function(teamId) {
		if (this.getFollowedTeam() === null) return false;

		return this.getFollowedTeam().getId() === teamId;
	}
});