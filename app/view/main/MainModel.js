/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TractalisWebclient.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'TractalisWebclient',
        menuCollapsed: true,
        selectedTeamA: null,

        selectedTeam: null,
        selectedTeamPosition: null,

        hqMode: false,

        geolocationAvailable: true,
        geolocationFailed: false
    }
});
