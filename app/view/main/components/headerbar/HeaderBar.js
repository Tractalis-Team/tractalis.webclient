/**
 * Headerbar View which represents the Microsoft Tile-Like Navigation on top.
 */
Ext.define('TractalisWebclient.view.main.components.headerbar.HeaderBar', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.main-header-bar',
	controller: 'main-components-headerbar',
	viewModel: {
		type: 'main-components-headerbar'
	},
	requires: [
		'TractalisWebclient.view.main.components.headerbar.HeaderBarController',
		'TractalisWebclient.view.main.components.headerbar.HeaderBarModel'
	],

	// some default properties
	config: {
		height: 50,
		cls: 'main-header-bar',

		layout: {
			type: 'fit',
			align: 'middle'
		}
	},

	initComponent: function () {
		var me = this;

		me.callParent(arguments);
		me.add([me.getPanelConfiguration()]);
	},

	/**
	 * Gets the panel configuration
	 *
	 * @returns {Object} configuration
	 */
	getPanelConfiguration: function () {
		var me = this,
			// only one main sponsor
			mainSponsors = Configuration.get('race/sponsors').filter(function (sponsor) {
				return sponsor.isMain;
			}),
			mainSponsor = (mainSponsors && mainSponsors.length > 0 ) ? mainSponsors[0] : null,
			panelsConfig = {
				xtype: 'panel',
				height: 50,
				layout: 'hbox',

				items: [{
					xtype: 'panel',
					layout: {
						type: 'hbox'
					},
					items: [{
						xtype: 'image',
						cls: 'tractalis-logo',
						width: 120,
						height: 50,
						bind: {
							src: '{tractalisLogoUrl}'
						},
						alt: 'Tractalis Logo',
						autoEl: {
							tag: 'a',
							href: 'https://www.tractalis.com/',
							target: '_blank'
						}
					}, {
						xtype: 'facebook-like-buttons',
						padding: '13 0 0 20',
						height: 50
					}, {
						xtype: 'image',
						src: 'resources/images/loading-indicator.svg',
						width: 30,
						height: 30,
						margin: '10 0 0 20',
						bind: {
							hidden: '{!isLoading}'
						}
					}],
					flex: 4
				}, {
					xtype: 'panel',
					layout: {
						type: 'hbox',
						align: 'middle'
					},
					height: '100%',
					cls: 'center-container',
					items: [{
							xtype: 'image',
							cls: 'event-logo',
							width: 'auto',
							margin: '0 30 0 0',
							height: '45px',
							autoEl: {
								tag: 'a',
								target: '_blank',
								href: '' // afterrender set this up, change it if you know how to do this through bind
							},
							listeners: {
								afterrender: function(view) { 
									var url = me.getViewModel().get('raceWebsiteUrl');
									if(url) {
										view.getEl().dom.href = url;
									}
								},
								load: {
									element: 'el',
									delegate: 'img',
									fn: function () {
										me.updateLayout();
									}
								}
							},
							bind: {
								src: '{raceLogoUrl}'
							}
						}, {
							cls: 'event-name-label',
							xtype: 'panel',
							height: 50,

							bind: {
								html: '{raceName}'
							}
						}],
					flex: 6
				}]
			};

		if (mainSponsor) {
			// assumption accepted by CF: if more than 1, 1st one wins as there should be only one
			panelsConfig.items.push({
				xtype: 'panel',
				cls: 'right-container',
				width: 'auto',
				minWidth: 50,
				items: [{
					xtype: 'image',
					cls: 'main-sponsor',
					height: 50,
					width: 'auto',
					src: mainSponsor.image,
					alt: mainSponsor.name,
					listeners: {
						load: {
							element: 'el',
							fn: function () {
								// we need to rescale if image is bigger
								me.updateLayout();
							}
						},
						click: {
							element: 'el',
							fn: function (url) {
								ga('send', 'event', 'sponsors', 'click', 'banner', url);
								// open new tab
								window.open(url, '_blank');
							}.bind(me, mainSponsor.url)
						}
					}
				}]
			});
		}

		return panelsConfig;
	}
});
