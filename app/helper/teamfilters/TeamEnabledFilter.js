/**
 * This class is used as Base-Class for each Command which should be able to synchronize via
 * the Global Message Queue across different instances of the client.
 */
Ext.define('TractalisWebclient.helper.teamfilters.TeamEnabledFilter', {
	extend: 'BaseFilter',
	alternateClassName: 'TeamEnabledFilter',

	config: {
		/**
		 * @cfg {Array} teams
		 * Contains the list of teams which should fall under filter positive or negative
		 */
		teams: [],

		positiveFiltering: false
	},

	onTeamEnabledChange: function (team) {
		var me = this,
			teamEnabled = team.get('Enabled');

		if (me.getPositiveFiltering()) {
			if (teamEnabled) {
				me.addTeam(team.getId());
			} else {
				me.removeTeam(team.getId());
			}
		} else {
			if (teamEnabled) {
				me.removeTeam(team.getId());
			} else {
				me.addTeam(team.getId());
			}
		}
	},

	/**
	 * Add the specific team into the list of the teams which are sorted out by this filter.
	 * Team is only added when not added yet.
	 *
	 * @param {Number} teamId
	 */
	addTeam: function (teamId) {
		var teams = this.getTeams();

		// add the element only if not existing yet
		if (teams.indexOf(teamId) === -1) {
			teams.push(teamId);
		}
	},

	/**
	 * Remove the specified team from the list of teams.
	 *
	 * @param {Number} teamId
	 */
	removeTeam: function (teamId) {
		var teams = this.getTeams(),
			index = teams.indexOf(teamId);

		if (index > -1) {
			teams.splice(index, 1);
		}
	},

	/**
	 * Restores this filter
	 */
	restore: function () {
		var me = this,
			store = Ext.getStore('Teams'),
			teams = me.getTeams(),
			positiveFiltering = me.getPositiveFiltering();

		store.beginUpdate();

		if (teams.length === 0 && positiveFiltering) {
			// set all to disabled
			store.each(me.disableTeam, me);
		} else {
			if (!positiveFiltering) {
				teams.forEach(function (teamId) {
					me.disableTeam(store.getById(teamId));
				});
			} else {
				store.each(function (team) {
					if (teams.indexOf(team.get('TeamId')) === -1) {
						me.disableTeam(team);
					}
				});
			}
		}

		store.endUpdate();
		store.fireEvent('refresh', store);

		Ext.GlobalEvents.fireEvent('restoreTeamEnabled', me.serialize());
	},

	disableTeam: function (team) {
		team.set('Enabled', false, {
			silent: true
		});
	},

	/**
	 * Serializes this filter
	 *
	 * @returns {Object} Serialized object ready to persist
	 */
	serialize: function () {
		var me = this;

		return me.addProperties(me.callParent(), {
			teams: me.getTeams(),
			positiveFiltering: me.getPositiveFiltering()
		})
	},

	toOdataFilter: function () {
		var cmp = ' ne ',
			glue = ' and ',
			teams = this.getTeams(),
			positiveFiltering = this.getPositiveFiltering();

		if (positiveFiltering) {
			cmp = ' eq ';
			glue = ' or ';
		}

		if (teams.length === 0 && positiveFiltering){
			return '(TrackableObjectId eq '+ Number.NaN +')';
		}

		return teams.map(function (teamId) {
			return '(TrackableObjectId' + cmp + teamId + ')';
		}).join(glue);
	}
});