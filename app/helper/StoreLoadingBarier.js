/**
 * This Helper class provides various functionallities to handle Store Updates.
 *
 */
Ext.define('TractalisWebclient.helper.StoreLoadingBarier', {
	singleton: true,
	alternateClassName: 'StoreLoadingBarier',

	mixins: {
		observable: 'Ext.util.Observable'
	},

	config: {},

	/**
	 * @private
	 * @returns {undefined}
	 */
	constructor: function() {
		var me = this;

		me.mixins.observable.constructor.call(me);

		// create the automatic properties
		me.initConfig(me.config);
	},

	/**
	 * Attaches to all stores and checks whether they were loaded
	 */
	attachAllStores: function() {
		var loadedStores = 0,
			stores = [
				'Positions',
				'Teams',
				'FixPoints'
			],
			storesLength = stores.length;

		stores.forEach(function(storeId) {
			var store = Ext.getStore(storeId);

			store.on('load', function() {
				loadedStores++;

				if (loadedStores === storesLength) {
					Ext.GlobalEvents.fireEvent('allStoresLoad');
				}
			}, this, {
				single: true
			});
		});
	},

	/**
	 * Map the specific store settings into a single function
	 *
	 * @param {Array} storeMappings
	 * @param {Object} scope
	 * @param {Object} mappingFunctionParameter Can be any object which will be passed as parametter to the mapping function
	 */
	mapStores: function(storeMappings, scope, mappingFunctionParameter) {
		var me = this;

		storeMappings.forEach(function(mapping) {
			var wereStoresLoaded = me.wereStoresLoaded(mapping.stores);

			console.log('StoreLoadingBarier.mapStores(): Check if stores were loaded', mapping.identifier, wereStoresLoaded);

			if (wereStoresLoaded) {
				// stores were loaded all
				// so immediately call the mapping funcion and we're done
				var result = [];
				mapping.stores.forEach( function(el){ result.push(el); } );
				result.push(mappingFunctionParameter);
				mapping.fnc.apply(scope, result);
				return;
			}

			var storePromises = [],
				allPromises = null;

			// go through the stores for the current mapping and add the loading promise to the array
			mapping.stores.forEach(function(store) {
				storePromises.push(me.waitForStoreLoaded(store));
			});

			console.log('StoreLoadingBarier.mapStores(): Promises', storePromises, storePromises.length);

			allPromises = Promise.all(storePromises);

			// not loaded yet, attach load events via promises
			// and wait until all of them were resolved
			allPromises.then(function(results) {
				console.log('StoreLoadingBarier.mapStores(): Promises were resolved, promises:', allPromises);

				// we resolve always with the first store as reference
				// because this is the 'main'-storage
				try {
					// addtion to the end... as wished
					results.push(mappingFunctionParameter);
					mapping.fnc.apply(scope, results);
				} catch (err) {
					// we catch errors here because it's possible that
					// the mapping delegate contains errors and they are very hard to track down
					// so we keep this here
					console.error(err);
				}
			});

		});
	},

	/**
	 * Goes through all stores passed as a list and checks whether they were loaded together yet
	 *
	 * @param {Array} stores
	 * @return {Boolean} True if stores were all loaded
	 */
	wereStoresLoaded: function(stores) {
		var storesLoaded = true;

		// go through all stores and check whether they were loaded
		stores.forEach(function(store) {
			storesLoaded = storesLoaded && store.isLoaded();
		});

		return storesLoaded;
	},

	/**
	 * Asynchronously wait for the specified store to be loaded and map the process in a Promise
	 *
	 * @params {Ext.data.Store} store
	 *
	 * @returns {Promise} promise which resolves when the store was loaded properly
	 */
	waitForStoreLoaded: function(store) {
		return new Promise(function(resolve, reject) {
			if (store.isLoaded() && !store.isLoading()) {
				// if store is loaded and not loading rightnow, we can resolve imediately
				resolve(store);
			} else {
				// if store was not loaded yet, or is still loading
				// we attach a single-eventhandler to listen for the load message
				// of the store, and resolve it then
				store.on('load', function(store) {
					resolve(store);
				}, this, {
					single: true
				});
			}
		});
	}
});