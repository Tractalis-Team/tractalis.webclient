/**
 * Represents a window with a list of team members represented by it.
 */
Ext.define("TractalisWebclient.view.main.components.teams.details.members.TeamMembers", {
	extend: "Ext.window.Window",
	alias: 'widget.team-members',

	requires: [
		'TractalisWebclient.view.main.components.teams.details.members.TeamMembersController',
		'TractalisWebclient.view.main.components.teams.details.members.TeamMembersModel'
	],

	controller: "team-members",
	viewModel: {
		type: "team-members"
	},

	config: {
		/**
		 * @cfg {TractalisWebclient.model.Team} team
		 * The team which is represented by this view.
		 */
		team: null
	},

	layout: 'fit',
	closable: true,

	bind: {
		title: 'Members of {team.Name}'
	},

	items: {
		xtype: 'grid',
		border: false,

		viewConfig: {
			/**
			 * @cfg {Boolean} markDirty
			 * Specifies that the grid should not mark the edited values
			 */
			markDirty: false
		},


		columns: [{
			text: 'Name',
			dataIndex: 'Name',
			width: '50%'
		}, {
			text: 'Country',
			dataIndex: 'CountryCode',
			width: '20%',
			renderer: function(value) {
				if(!value) {
					return '';
				}

				return '<img src="http://tractalisstorage.blob.core.windows.net/events/Resources/Common/CountryFlags/png/' + value.toLowerCase() + '.png" />';
			}
		}, {
			text: 'Age',
			dataIndex: 'age',
			width: '25%'
		}],

		store: 'Participants'
	},

	/**
	 * Is called after the team property is updated.
	 * @param  {TractalisWebClient.model.Team} team
	 */
	updateTeam: function(team) {
		this.getViewModel().set('team', team);
	}
});