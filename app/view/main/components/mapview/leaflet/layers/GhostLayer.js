Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.GhostLayer", {
	alternateClassName: 'GhostLayer',

	config: {
		mapContainer: null,
		map: null,
		ghostMarker: null,

		markerOptions: {
			className: 'tractalis-fix-ghost-marker',
			color: '#221e1e',
			opacity: 0.5,
			fillOpacity: 0.2,
			radius: 8,
			weight: 2
		}
	},

	constructor: function(options){
		this.initConfig();

		this.setMapContainer(options.mapContainer);
		this.setMap(options.map);

		this.setGhostMarker(L.circleMarker(options.map.getCenter(), this.getMarkerOptions()));
	},

	setUpLayer: function(){
		return new Promise(function(resolve, reject){
			var mapContainer = this.getMapContainer();
			mapContainer.on('showGhostOn', this.showGhost, this);
			mapContainer.on('hideGhost', this.hideGhost, this);

			resolve(true);
		}.bind(this));
	},

	showGhost: function(lng, lat){
		var ghostMarker = this.getGhostMarker(),
			map = this.getMap();

		ghostMarker.setLatLng([lat,lng]);

		if (!map.hasLayer(ghostMarker)) {
			map.addLayer(ghostMarker);
		}
	},

	hideGhost: function(){
		var ghostMarker = this.getGhostMarker(),
			map = this.getMap();

		if (map.hasLayer(ghostMarker)) {
			map.removeLayer(ghostMarker);
		}
	}
});
