Ext.define('TractalisWebclient.view.main.components.mapview.leaflet.MapContainerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.L-map-container',
    data: {
        name: 'TractalisWebclient'
    }
});
