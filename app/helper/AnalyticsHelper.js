/**
 * This helper class provides some functionallities for GAL (Google Analytics) which can be used in the Sencha Application.
 */
Ext.define('TractalisWebclient.helper.AnalyticsHelper', {
	singleton: true,
	alternateClassName: 'Analytics',

	config: {},

	/**
	 * @private
	 * @returns {undefined}
	 */
	constructor: function() {
		var me = this;

		// create the automatic properties
		me.initConfig(me.config);
	},

	/**
	 * Tracks the specific event in the Analytics Dashboard
	 *
	 * @param {String} category The category name of the event, for example 'GIS'
	 * @param {String} action The name of the action, for example 'click'
	 * @param {String} label The event Label for example 'showDetailsButton'
	 * @param {Number} value
	 */
	trackEvent: function(category, action, label, value) {
		if (typeof ga !== 'undefined') {
			ga('send', 'event', category, action, label, value);
		}
	}
});