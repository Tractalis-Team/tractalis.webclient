/**
 * Viewmodel for the Headerbar.
 */
Ext.define('TractalisWebclient.view.main.components.headerbar.HeaderBarModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.main-components-headerbar',

    data: {
        title: 'Tractalis Webclient',
        raceName: 'RAAM 2012',
        raceLogoUrl: '',
        tractalisLogoUrl: '',
        raceWebsiteUrl: '',

        /**
         * @cfg {Boolean} isLoading
         * Specifies whether there is any network activity rightnow
         */
        isLoading: false
    }
});