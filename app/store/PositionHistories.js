/**
 * This store represents all Team Positions which are active for the current specified race.
 */
Ext.define('TractalisWebclient.store.PositionHistories', {
	extend: 'Ext.data.Store',
	model: 'TractalisWebclient.model.TeamPositionHistory',
	autoLoad: false,
	pageSize: 0,

	proxy: {
		type: 'azure-tables',
		tableName: AzureTablesProxy.buildTableName('Event', Configuration.get('race/id'), 'PositionHistory'),
		sasKey: 'History',
		columns: ['TrackableObjectId', 'TimeStamp', 'RawPositionLon', 'RawPositionLat', 'TrackerState', 'PositionHistoryId'],
		incremental: true,

		reader: {
			type: 'json',
			rootProperty: 'value',
			successProperty: false,
			transform: function (data) {
				var result = [];

				data.sort(this.byGpsTimestamp)
					.reduce(function (acc, pos) {
						var teamId = pos.TrackableObjectId,
							rawPositionHistory = acc[teamId];

						if (!rawPositionHistory) {
							rawPositionHistory = {
								TeamId: teamId,
								HistoricalPositions: []
							};
							// store it under result as well
							// yeah i know that it leaks in from outside
							result.push(rawPositionHistory);
							acc[teamId] = rawPositionHistory;
						}

						rawPositionHistory.HistoricalPositions.push(this.recToHistoricalPosition(pos));

						return acc;
					}.bind(this), {});

				return result;
			},

			recToHistoricalPosition: function (rec) {
				return {
					Id: rec.PositionHistoryId,
					TimeStamp: rec.TimeStamp,
					Longitude: rec.RawPositionLon,
					Latitude: rec.RawPositionLat,
					// null comes if viewing histories made by older server and then there were only normal(0) ones in the table
					TrackerState: (rec.TrackerState !== null) ? rec.TrackerState : 0
				};
			},

			byGpsTimestamp: function (l, r) {
				return +(l.TimeStamp > r.TimeStamp) || +(l.TimeStamp === r.TimeStamp) - 1;
			}
		}
	}
});
