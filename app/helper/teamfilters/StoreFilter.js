/**
 * This class is used as Base-Class for each Command which should be able to synchronize via
 * the Global Message Queue across different instances of the client.
 */
Ext.define('TractalisWebclient.helper.teamfilters.StoreFilter', {
	extend: 'TractalisWebclient.helper.teamfilters.BaseFilter',
	alternateClassName: 'StoreFilter',

	config: {
		storeName: "",
		fnc: Ext.emptyFn
	},

	/**
	 * Serializes the command so it can be sent through the persistance for example
	 *
	 * @returns {Object} Serialized object wwhich is ready to be packaged to be stored
	 */
	serialize: function() {
		var me = this,
			serialized = me.callParent();

		serialized.storeName = me.getStoreName();

		return serialized;
	},

	/**
	 * Gets the store instance based on this filter ID
	 * @returns {Ext.data.Store} store
	 */
	getStore: function() {
		return Ext.getStore(this.getStoreName());
	},

	/**
	 * Clears this filter and reset the state before the filter was applied
	 */
	clear: function() {
		this.callParent();
		this.getStore().removeFilter(this.getId());
	},

	/**
	 * Executes the actual request request
	 *
	 * @returns {Boolean} true when filter was applied, otherwhise false
	 */
	execute: function() {
		var me = this;

		me.callParent();

		// adds the filter to the store
		me.getStore().addFilter(new Ext.util.Filter({
			id: me.getId(),
			filterFn: me.getFnc(),
			scope: me
		}));

		return true;
	}
});
