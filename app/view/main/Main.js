/**
 * This view represents the Tractalis Main Stage
 */
Ext.define('TractalisWebclient.view.main.Main', {
	extend: 'Ext.container.Container',
	requires: [
		'TractalisWebclient.view.main.MainController',
		'TractalisWebclient.view.main.MainModel'
	],
	xtype: 'app-main',

	controller: 'main',
	viewModel: {
		type: 'main'
	},

	layout: {
		type: 'border'
	},

	items: [{
		xtype: 'main-header-bar',
		frame: false,
		region: 'north',
		hidden: Ext.os.is.Phone
	}, {
		xtype: 'header-metro-navigation-container',
		cls: 'metro-tile-navigation-panel',
		region: 'north',
		collapsed: true,
		collapseMode: 'mini',
		bind: {
			collapsed: '{menuCollapsed}'
		}
	}, {
		xtype: 'panel',
		cls: 'west-panel-container',
		region: 'west',
		collapsible: true,
		width: Ext.os.is.Phone ? '98%' : 400,
		collapsed: Ext.os.is.Phone || Configuration.get('race/hasSingleParticipant'),
		split: true,

		title: 'Teams',
		layout: 'vbox',

		items: [{
			xtype: 'team-list',
			id: 'uiTeamList',
			scrollable: true,
			flex: 20,
			width: '100%'
		}, {
			xtype: 'team-details',
			id: 'teamDetailsPanel',
			width: '100%',
			height: Configuration.get('race/includeSwissgrid') ? 290 : 260,
			closable: true,
			closeAction: 'hide',
			hidden: true,

			bind: {
				team: '{selectedTeam}',
				teamPosition: '{selectedTeamPosition}'
			}
		}]
	}, {
		xtype: 'panel',
		region: 'center',
		layout: 'border',
		height: '100%',
		itemId: 'central-container',
		items:[{
			id: 'mapViewContainer',
			xtype: 'L-map-container',
			region: 'center'
		}]
	}],

	initComponent: function () {
		var me = this,
			socialMedia = Configuration.get('socialMedia', null),
			useElevationChart = Configuration.get('elevation/enabled', true);

		this.callParent(arguments);

		if(useElevationChart) {
			me.getComponent('central-container').add([{
				id: 'elevationChart',
				xtype: 'elevation-chart',
				region: 'south',
				collapseMode: "mini"
			}]);
		}

		if (socialMedia !== null) {
			me.add([{
				xtype: 'social-media',
				region: 'east',
				collapseMode: "mini",
				collapsed: !!Ext.os.is.Phone,
				width: Ext.os.is.Phone ? 200 : 400
			}]);
		}
	}
});