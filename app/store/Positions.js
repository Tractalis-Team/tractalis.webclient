/**
 * This store represents all Team Positions which are active for the current specified race.
 */
Ext.define('TractalisWebclient.store.Positions', {
	extend: 'Ext.data.Store',
	model: 'TractalisWebclient.model.Position',
	autoLoad: true,
	pageSize: 0,

	proxy: {
		type: 'azure-tables',
		tableName: AzureTablesProxy.buildTableName('Event', Configuration.get('race/id'), 'PositionsV2'),
		sasKey: 'Positions',
		filter: '(TrackableObjectId ne 0)',

		reader: {
			type: 'json',
			rootProperty: 'value',
			successProperty: false
		}
	}
});