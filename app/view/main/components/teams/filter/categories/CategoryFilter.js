Ext.define("TractalisWebclient.view.main.components.teams.filter.categories.CategoryFilter", {
	extend: "Ext.window.Window",
	alias: 'widget.category-filter',

	requires: [
		"TractalisWebclient.view.main.components.teams.filter.categories.CategoryFilterController",
		"TractalisWebclient.view.main.components.teams.filter.categories.CategoryFilterModel",

		"Ext.grid.column.Check"
	],

	config: {
		/**
		 * @cfg {Ext.data.Store} teamCategoryStore
		 * Represents the store which contains all team categories
		 */
		teamCategoryStore: null
	},

	controller: "main-components-teams-filter-categories-categoryfilter",
	viewModel: {
		type: "main-components-teams-filter-categories-categoryfilter"
	},

	closable: true,
	title: 'Categories',
	scrollable: 'y',
	layout: 'fit',

	items: {
		xtype: 'grid',
		border: false,

		id: 'uiCategories',

		header: {
			xtype: 'checkbox',
			id: 'uiToggleAll',
			fieldLabel: 'Toggle all',
			value: true,
			labelClsExtra: 'category-filter',
			handler: function(checkbox, checked){
				console.log(this);
				this.fireEvent('toggleAll', checked);
			}
		},

		viewConfig: {
			/**
			 * @cfg {Boolean} markDirty
			 * Specifies that the grid should not mark the edited values
			 */
			markDirty: false
		},

		columns: [{
			text: 'Name',
			dataIndex: 'Name',
			width: '70%'
		}, {
			xtype: 'checkcolumn',
			text: 'Enable',
			dataIndex: 'Enabled',
			width: '29%'
		}],

		store: 'TeamCategories'
	}
});