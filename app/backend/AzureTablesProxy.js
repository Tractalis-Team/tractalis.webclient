/**
 * Special Proxy used to fetch Data from Azure-Tables. It includes the SAS Security-Mechanism and continuotity to fetch big data.
 */
Ext.define('TractalisWebclient.backend.AzureTablesProxy', {
	requires: ['Ext.Ajax'],
	extend: 'Ext.data.proxy.Ajax',
	alias: 'proxy.azure-tables',
	alternateClassName: 'AzureTablesProxy',

	statics: {
		buildTableName: function (prefix, raceId, sufix) {
			return prefix + (raceId < 0 ? "Minus" + -raceId : raceId) + sufix;
		}
	},

	config: {
		/**
		 * @cfg {String} tableName
		 * The name of the table which should be accessed, for example "Event12Positions"
		 */
		tableName: "",

		/**
		 * @cfg {String} filter
		 * Specifies the filter which should be used to fetch the data
		 */
		filter: "",

		/**
		 * @cfg {String} sasKey
		 * The key of the SAS Key which should be used, for example 'TeamData' or 'Positions', ...
		 */
		sasKey: "",

		/**
		 * @cfg {Array} columns
		 * Specifies the columns which should be fetched. If not set, entire table will be fetched.
		 */
		columns: [],

		incremental: false,

		/**
		 * @cfg {Object} headers
		 * Specify HTTP-Headers which should be added to the Requests
		 */
		headers: {
			Accept: 'application/json;odata=nometadata'
		},

		/**
		 * @cfg {Array} results
		 * Property containing all records which weren't passed to the store yet
		 */
		results: []
	},

	// times in iso forat are comparable and sort well, further details see:
	// http://www.ecma-international.org/ecma-262/5.1/#sec-11.8.5
	lastFetchedTsTemp: '',
	lastFetchedTs: '',

	lastXhr: null,
	abortedReqResultSet: new Ext.data.ResultSet({
		total: 0,
		count: 0,
		records: [],
		success: false
	}),

	constructor: function (config) {
		this.callParent(arguments);

		const columns = this.getColumns();

		if (this.getIncremental() && columns.indexOf('Timestamp') < 0) {
			columns.push('Timestamp');
		}
	},

	//@inheritdoc
	read: function (operation) {
		var me = this,
			sasUrl = CertificateHelper.getSasUrl(me.getSasKey());

		// check if SAS Url for the specific resource could be retrieved
		if (sasUrl === null) {
			// no SAS Url for the specific resource, so start to fetch & attach event handler
			CertificateHelper.fetchSASConfiguration();
			CertificateHelper.on('sasLoaded', function () {
				me.fetch(operation, null, null, CertificateHelper.getSasUrl(me.getSasKey()));
			}, me, {
				single: true
			});
		} else {
			// sas was retrieved already, fetch
			me.fetch(operation, null, null, sasUrl);
		}
	},

	abort: function (req) {
		req = req || this.lastXhr;
		if (req) {
			Ext.Ajax.abort(req);
		}
	},

	/**
	 * Fetches the data through the REST Webservice
	 *
	 * @param {Ext.data.Operation} operation
	 * @param {String} nextPartitionKey
	 * @param {String} nextRowKey
	 * @param {String} sasUrl
	 */
	fetch: function (operation, nextPartitionKey, nextRowKey, sasUrl) {
		var me = this;

		this.lastXhr = Ext.Ajax.request({
			// build the URL based on the partition and row
			url: me.buildUrl(nextPartitionKey, nextRowKey, sasUrl),

			// to fetch data we always use GET
			method: 'GET',

			// pass the headers which were defined in the store
			headers: me.getHeaders(),

			// azure services should be accessed with disabled caching
			// otherwhise they will throw errors
			disableCaching: false,

			// defines the scope
			scope: me,

			/**
			 * @private
			 * Ajax retrieval was a success
			 * @param {Ext.data.Response} response
			 */
			success: function (response) {
				var nextPartitionKey = response.getResponseHeader("x-ms-continuation-nextpartitionkey"),
					nextRowKey = response.getResponseHeader("x-ms-continuation-nextrowkey"),
					needsMoreRequests = typeof nextPartitionKey !== "undefined" && typeof nextRowKey !== "undefined",
					results = me.getResults();

				// add the current response into result-data
				var objects = JSON.parse(response.responseText),
					rootProperty = me.getReader().getRootProperty(),
					rawRecords = objects[rootProperty];

				// go through the raw records and add them
				// one after another to the temporary results list
				rawRecords.forEach(function (record) {
					results.push(record);
				});

				if (me.getIncremental()) {
					me.updateLatestTs(rawRecords);
				}

				if (needsMoreRequests) {
					// create the next request already
					// we do this until the response does not contain the continuation-headers
					me.fetch(operation, nextPartitionKey, nextRowKey, sasUrl);
				} else {
					// last request, we're done
					// mark the stuff as done as well
					me.fetchingDone(operation);
				}
			},

			/**
			 * @private
			 * Ajax retrieval was a failure
			 * @param {Ext.data.Response} response
			 */
			failure: function (response, opts) {
				if (response.aborted) {
					operation.setResultSet(this.abortedReqResultSet);
					operation.setSuccessful(false);
					operation.setCompleted(true);
					return;
				};

				if (response.status == 404) {
					console.log('AzureTablesProxy.failure(): It seems like table does not exist, check config file', this.getTableName());

					// there is no table at all, Greg claims that it can be completely normal ;)
					me.fetchingDone(operation);
					return;
				}

				console.warn('AzureTablesProxy(): Failure while fetching data from tables', response);

				if (me.getIncremental()) { // reset temp one
					me.lastFetchedTsTemp = me.lastFetchedTs;
				}

				// it's probably the case that the SAS keys are not valid anymore
				// so initialize a new SAS Key retrieval
				CertificateHelper.fetchSASConfiguration();
				CertificateHelper.on('sasLoaded', function () {
					me.fetch(operation, nextPartitionKey, nextRowKey, CertificateHelper.getSasUrl(me.getSasKey()));
				}, me, {
					single: true
				});
			}
		});
	},

	/**
	 * Read the records through the Reader and closes the operation as a success.
	 *
	 * @param {Ext.data.Operation} operation
	 */
	fetchingDone: function (operation) {
		var me = this,
			resultSet = me.getReader().readRecords(me.getResults());

		operation.setResultSet(resultSet);
		operation.setSuccessful(true);

		if (me.getIncremental()) {
			me.lastFetchedTs = me.lastFetchedTsTemp;
		} else {
			// clean the temporary list for further updates
			me.setResults([]);
		}
	},

	updateLatestTs: function (rawResults) {
		this.lastFetchedTsTemp = rawResults.reduce(this.maxTs, this.lastFetchedTsTemp);
	},

	maxTs: function (prevTimestamp, cur) {
		return (prevTimestamp < cur.Timestamp) ? cur.Timestamp : prevTimestamp;
	},

	/**
	 * Builds URL for the table Request
	 *
	 * @param {String} nextPartitionKey
	 * @param {String} nextRowKey
	 * @param {String} sasUrl
	 *
	 * @returns {String} URL to perform the request on
	 */
	buildUrl: function (nextPartitionKey, nextRowKey, sasUrl) {
		var me = this,
			filter = me.buildFilter(),
			columns = me.getColumns(),
			hasPartitionKey = typeof nextPartitionKey !== 'undefined' && nextPartitionKey !== null,
			hasRowKey = typeof nextRowKey !== "undefined" && nextRowKey !== null,
			// array contains all parameters
			parameters = {};

		if (hasPartitionKey) {
			parameters.NextPartitionKey = nextPartitionKey;
		}

		if (hasRowKey) {
			parameters.NextRowKey = nextRowKey;
		}

		if (filter) {
			parameters['$filter'] = filter;
		}

		if (columns && columns.length) {
			parameters['$select'] = columns.join(',');
		}

		return "https://tractalisstorage.table.core.windows.net/" + me.getTableName() + "?" + sasUrl + "&" + me.generateQueryString(parameters);
	},

	buildFilter: function () {
		var me = this,
			filter = me.getFilter(),
			hasFilter = filter !== '';

		if (me.needsIncrementFilter()) {
			filter += (hasFilter ? ' and ' : '') + '(Timestamp gt datetime\'' + me.lastFetchedTs + '\')'
		}

		return filter;
	},

	needsIncrementFilter: function () {
		return this.getIncremental() && this.lastFetchedTs !== '';
	},

	/**
	 * Generates a query string from the specied variables
	 *
	 * @param {Array} parameters A list of parameters to add to the query
	 *
	 * @returns {String} querystring
	 */
	generateQueryString: function (parameters) {
		if (Object.keys(parameters).length === 0) {
			return "";
		}

		return Ext.Object.toQueryString(parameters);
	},

	resetCache: function () {
		this.setResults([]);
		this.lastFetchedTsTemp = '';
		this.lastFetchedTs = '';
	}
});