Ext.define('TractalisWebclient.view.main.components.teams.list.TeamListModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.main-components-teams-list-teamlist',
	data: {
		name: 'TractalisWebclient'
	},

	formulas: {
		/**
		 * Checks whether the historical waypoints are visible
		 * @param get
		 * @returns {boolean}
		 */
		historicalWaypointsVisible: function (get) {
			/**
			 * The permissions-configuration defines WHO sees the waypoints column
			 */
			const permissions = Configuration.get('historyWaypoints/permissions', ['headquarter']);

			// when empty, we add the default to it
			if (permissions.length === 0) {
				permissions.push('headquarter');
			}

			/**
			 * Provides whether the permission configuration contains headquarters
			 * @type {boolean}
			 */
			const containsHeadquarterPermission = permissions.indexOf('headquarter') > -1;

			/**
			 * Provides whether the permission configuration contains public permissions
			 * @type {boolean}
			 */
			const containsPublicPermission = permissions.indexOf('public') > -1;

			/**
			 * Checks whether the user is running client in HQ Mode
			 */
			const isHqMode = get('hqMode');

			if (permissions.length === 1) {
				if (containsHeadquarterPermission) return isHqMode;
				if (containsPublicPermission) return !isHqMode;
			}

			return containsPublicPermission && containsHeadquarterPermission;
		}
	}
});
