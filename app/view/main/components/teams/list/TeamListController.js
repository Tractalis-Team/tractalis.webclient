Ext.define('TractalisWebclient.view.main.components.teams.list.TeamListController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.main-components-teams-list-teamlist',

	control: {
		'#': {
			rowkeyDown: 'teamListRowKeyDown'
		}
	},

	init: function () {
		var me = this,
			store = me.getView().getStore();

		store.on('changeEnableState', me.changeEnableState, me);

		me.getViewModel().bind('{selectedTeam}', me.selectedTeamChanged, me);
	},

	teamListRowKeyDown: function (grid, team, el, idx, evt, eOpts) {
		if (evt.keyCode === 27) {
			const selModel = this.getView().getSelectionModel();
			selModel.deselectAll();
		}
	},

	selectedTeamChanged: function (team) {
		const selModel = this.getView().getSelectionModel();

		if (team) {
			selModel.select(team)
		} else {
			selModel.deselectAll();
		}
	},

	/**
	 * A batch update was done on the store, handle it
	 */
	changeEnableState: function () {
		// make sure that the grid is recreated again
		// bc. the store is not updated field by field
		this.getView().reconfigure();
	}
});