Ext.define('TractalisWebclient.helper.teamfilters.SoleTeamFilter', {
	extend: 'StoreFilter',
	alternateClassName: 'SoleTeamFilter',

	config: {
		teamId: null,

		storeName: "Teams",

		fnc: function(record) {
			return record.get('TeamId') === this.getTeamId();
		}
	},

	serialize: function() {
		// this one wont be persisted
		return;
	},

	toOdataFilter: function() {
		return '(TrackableObjectId eq ' + this.getTeamId() + ')';
	}
});