/**
 *
 */
Ext.define('TractalisWebclient.model.TeamPositionHistory', {
	extend: 'Ext.data.Model',

	idProperty: 'TeamId',

	fields: [{
		name: 'TeamId',
		type: 'auto'
	}, {
		name: 'HistoricalPositions',
		type: 'auto'
	}]

	// TODO: doesn't work, figure it out somehow, maybe it will work with schema
	// hasMany: {
	// 	model: 'TractalisWebclient.model.HistoricalPosition',
	// 	name: 'HistoricalPositions'
	// }
});

// This one is only for information purposes as currently mapping is not working
Ext.define('TractalisWebclient.model.HistoricalPosition', {
	extend: 'Ext.data.Model',

	fields: [{
		name: 'id',
		type: 'number'
	},{
		name: 'TimeStamp',
		type: 'string'
	}, {
		name: 'Longitude',
		type: 'number'
	}, {
		name: 'Latitude',
		type: 'number'
	}, {
        name: 'TrackerState',
        type: 'int', // 0 - normal, 1 - went ON, 2 - went OFF
        defaultValue: 0
    }]
});
