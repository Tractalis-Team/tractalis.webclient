Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.Labeller", {
	singleton: true,
	alternateClassName: 'Labeller',

	attachLabel: function(marker, content, options, mapContainer) {
		marker.bindTooltip(content, options);
		marker.closeTooltip();

		// we replace default behavior of Label on add event
		marker.off('add', marker._onMarkerAdd);
		marker.on('add', this.showLabelOnProperZoom.bind(marker));

		mapContainer.on('showLabels', () => marker.openTooltip());

		// Leaflet has a bug that they don't check whether marker is still bound
		// to map when removing interactive target so we have to do this check
		// on our own to prevent explosions from internals of Leaflet
		mapContainer.on('hideLabels', () => marker._map && marker.closeTooltip());
	},

	showLabelOnProperZoom: function() {
		var closeZoom = Configuration.get('gis/closeZoomLevel'),
			curentZoom = this._map ? this._map.getZoom() : -1;

		if (closeZoom <= curentZoom) {
			this.openTooltip();
		}
	}
});