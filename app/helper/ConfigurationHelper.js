 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('TractalisWebclient.helper.ConfigurationHelper', {
    singleton: true,
    alternateClassName: 'Configuration',

    config: {
        /**
         * last version specifies the current 'master' or trunk version of this code-base
         */
        codeBaseVersion: new Ext.Version('0.0.1'),

        /**
         * current app version specifies the last version to which the local app -instance- was updatet
         */
        currentAppVersion: null
    },

    mixins : {
        observable : 'Ext.util.Observable'
    },

    /**
     * Constructs the instance of the Versioning Helper
     *
     * @returns {undefined}
     */
    constructor : function(){
        var me = this;

        me.mixins.observable.constructor.call(me);

        // create the automatic properties
        me.initConfig(me.config);
    },

    /**
     * Returns the configuration value of the specified key-path
     *
     * @param {type} key
     * @param {type} defaultValue optional, default vaule which should be returned in case config under key was not found
     * @returns {undefined}
     */
    get: function(key, defaultValue){
        // the configuration stuff is stored in a global object
        var me = this,
            properties = TractalisWebConfiguration,
            notFound = (typeof defaultValue === 'undefined') ? '@Property "'+key+'" not found' : defaultValue ;

        if (key.indexOf('/') > -1){
            // tree structure, split and dive into it
            var branches = key.split('/'),
                currentObject = properties;

            // loop through all branches and go with each step deeper
            // into the array until reaching the end or the specific property is not found
            // we do not use 'Ext.each' or 'Array.forEach' because for is way faster
            // see http://jsperf.com/array-foreach-ext-array-foreach-ext-array-each
            // and this is performance wise a critical part because it gets executed very often
            for (var i = 0, length = branches.length; i < length; i++){
                var branchName = branches[i];

                // select next object as active processed target
                currentObject = currentObject[branchName];

                // compare if could be found
                // if not skip the execution
                // yusuf du riese gjup
                if (currentObject === null || currentObject === undefined){
                    return notFound;
                }
            }

            // retrieve the value normally to caller
            return currentObject;
            // just plain simple top property
          } else {
            return properties.hasOwnProperty(key) ? properties[key] : notFound;
        }

        return notFound;
    }
});
