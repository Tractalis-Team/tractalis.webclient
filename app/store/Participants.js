/**
 * This store represents all Teams which are active for the current specified race.
 */
Ext.define('TractalisWebclient.store.Participants', {
    extend: 'Ext.data.Store',
    model: 'TractalisWebclient.model.Participant',
    autoLoad: true,
    pageSize: 0,
    
    proxy: {
        type: 'memory'
    }
});


