/**
 * This Model represents single Teams which are rendered in the Tree.
 */
Ext.define('TractalisWebclient.model.TeamCategory', {
	extend: 'Ext.data.Model',

	idProperty: 'Name',

	fields: [{
		name: 'Name',
		type: 'auto'
	}, {
		name: 'SortingPriority',
		type: 'int'
	}, {
		name: 'Enabled',
		type: 'boolean',
		defaultValue: true
	}]
});