Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.PositionsLayerController", {
	// FIXME: figure out how to fix SRP violation
	alternateClassName: 'PositionsLayerController',

	requires: [
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.Labeller'
	],

	markerDisplacer: null,
	markers: {},
	layer: null,
	mapContainer: null,
	focusedTeam: null,
	animateMovement: true,
	animationLength: 300,

	config: {
		markerOptions: {
			team: {
				className: 'tractalis-position-marker team',
				color: '#446CAF',
				weight: 1.5,
				pane: 'markerPane',
				radius: 4,
				path: 'm-4,0a4,4,0,0,0,8,0a4,4,0,0,0,-8,0z',
				fillOpacity: 0.8,
				fillColor: '#FFC800'
			},
			official: {
				className: 'tractalis-position-marker official',
				fillColor: '#446CAF',
				weight: 1.5,
				pane: 'markerPane',
				radius: 9,
				path: 'm-6-4.25 6-1.5 6 1.5c0 6 0 7.5-6 11.25-6-3.75-6-5.75-6-11.25z',
				fillOpacity: 0.8,
				color: '#FFC800'
			},
			medic: {
				className: 'tractalis-position-marker medic',
				color: '#FF0000',
				opacity: 0.8,
				lineCap: 'butt',
				lineJoin: 'miter',
				weight: 3,
				pane: 'markerPane',
				radius: 9,
				path: 'm-6,0h12m3,0m-9-6v12m-9-6a9,9,0,0,0,18,0a9,9,0,0,0-18,0z',
				fillOpacity: 0.8,
				fillColor: '#FFFFFF'
			},
			motomarshall: {
				className: 'tractalis-position-marker motomarshall',
				fillColor: '#446CAF',
				weight: 1.5,
				pane: 'markerPane',
				radius: 9,
				path: 'm0-9.4,2.2,6.4h6.8l-5.2,4.1,1.8,6.4-5.6-3.8-5.6,3.8,1.8-6.4-5.2-4.1h6.8z',
				fillOpacity: 0.8,
				color: '#FFC800'
			}
		},
		labelOptions: {
			direction: 'auto',
			noHide: true,
			interactive: true,
			pane: 'markerPane',
			className: 'position-label',
			permanent: true
		},
		popupOptions: {
			className: 'tractalis-position-popup',
			offset: [0, -5]
		}
	},

	constructor: function (options) {
		this.initConfig(this.config);
		this.mapContainer = options.mapContainer;
		this.markerDisplacer = new OverlappingMarkerSpiderfier(options.map, {
			circleFootSeparation: 40,
			legColors: {
				usual: '#446CAF',
				highlighted: '#FFC800'
			}
		});

		this.animateMovement = Configuration.get('gis/animateMovement', true);
		if(this.animateMovement) {
			this.animationLength = Configuration.get('gis/animationLength', 300);
			let markerOptions = this.getMarkerOptions();

			for (var markerType in markerOptions) {
				if (Object.hasOwnProperty(markerType)) {
					markerOptions[markerType].animationLength = animationLength;
				}
			}
		}

		this.markerDisplacer.addListener('click', marker => {
			this.requestDetails(this.mapContainer, marker._teamId);
			marker.bringToFront();
			marker.getTooltip().bringToFront();
		});
	},

	setUpLayer: function (positionsLayer) {
		return new Promise(function (resolve, reject) {
			// FIXME: place of this guy is in ctor
			this.layer = positionsLayer;

			var loadBarrierInfo = [{
				identifier: 'positions-n-teams',
				stores: [Ext.getStore('Positions'), Ext.getStore('Teams')],
				fnc: function (positions, teams, positionsLayer) {
					this.generatePositionMarkers(positions, teams, positionsLayer);

					this.mapContainer.on('teamSelected', (teamId) => {
						if (this.focusedTeam && this.focusedTeam !== teamId) {
							this.applySelectedDecoration(this.focusedTeam, false);
						}

						if (teamId && this.focusedTeam !== teamId) {
							this.focusedTeam = teamId;
							this.applySelectedDecoration(teamId, true);
						}
					});
					resolve(true);
				}
			}];

			try {
				StoreLoadingBarier.mapStores(loadBarrierInfo, this, positionsLayer);
			} catch (err) {
				console && console.er('PositionsLayerController.setUpLayer(): Error', err);
			}
		}.bind(this));
	},

	generatePositionMarkers: function (positions, teams, positionsLayer) {
		var me = this;
		positions.each(this.addPositionMarker.bind(me, positionsLayer));

		// we have positions on map so now is time to start listening for updates
		positions.on('update', me.positionUpdated, me);
		positions.on('load', me.positionsLoaded, me);

		teams.on('refresh', me.teamsVisibilityChanged.bind(me, teams, true), me);
		teams.on('update', me.teamsUpdated, me);
		teams.on('changeEnableState', me.teamsVisibilityChanged, me);
	},

	addPositionMarker: function (positionsLayer, position) {
		var me = this,
			positionMarker = this.positionToMarker(position);

		if (!positionMarker) return;

		me.markers[position.get('TeamId')] = positionMarker;

		if (me.shouldBeShown(position)) {
			positionMarker.addTo(positionsLayer);
			this.markerDisplacer.addMarker(positionMarker);
		}
	},

	positionToMarker: function (position) {
		var me = this,
			teamId = position.get('TeamId'),
			team = Ext.getStore('Teams').getById(teamId),
			popupOptions = Object.assign({}, this.getPopupOptions()),
			labelOptions = Object.assign({}, me.getLabelOptions()),
			label, type, markerOptions, marker;

		if (!team) return null;

		type = (team.get('CategoryTypeName') || 'team').toLowerCase();
		markerOptions = me.getMarkerOptions()[type];
		// TODO: introduce starategy for markers
		marker = L.pathMarker([position.get('Latitude'), position.get('Longitude')], markerOptions);

		popupOptions.className += ' ' + type;
		labelOptions.className += ' ' + type;

		var popup = L.popup(popupOptions);
		popup.setLatLng(position);
		var startNumber = team.get('StartNumber')
		popup.setContent('<strong>' + startNumber + ' ' + team.get('Name') + '</strong>');

		marker._teamId = teamId;

		marker.bindPopup(popup);
		marker.on('mouseover', function (e) {
			this.openPopup();
		});

		Labeller.attachLabel(marker, startNumber, labelOptions, me.mapContainer);

		label = marker.getTooltip();
		marker.on('add', () => {
			me.applyStatusDecoration(marker, teamId)
			me.applyFollowDecoration(team);
			me.applySelectedDecoration(teamId, this.focusedTeam === teamId);
		});
		label.on('add', me.applyStatusDecoration.bind(me, label, teamId));
		label.on('mouseover', () => marker.openPopup());

		return marker;
	},

	requestDetails: function (mapContainer, teamId, e) {
		mapContainer.fireEvent('teamDetailsRequested', teamId);
		mapContainer.fireEvent('teamSelected', teamId);
	},

	positionUpdated: function (store, position, operation, modifiedFields, details) {
		var me = this,
			teamId = position.get('TeamId'),
			marker = me.markers[teamId],
			follow = false,
			shouldShow = me.shouldBeShown(position),
			layer = me.layer;

		if (!marker) {
			if (shouldShow) {
				me.addPositionMarker(layer, position);
				follow = shouldShow && TeamFollower.isFollowed(teamId);
			}
		} else {
			let wasOnMap = layer.hasLayer(marker);

			if (modifiedFields.some(me.areLocationFields)) {
				var isDisplaced = !!marker._omsData,
					markerLatLon = isDisplaced ? marker._omsData.usualPosition : marker.getLatLng(),
					positionLatLon = L.latLng(position.get('Latitude'), position.get('Longitude'));

				if (!markerLatLon.equals(positionLatLon)) {
					if (isDisplaced) {
						me.markerDisplacer.unspiderify();
					}

					if (wasOnMap && shouldShow) {
						marker.setLatLng(positionLatLon, this.animateMovement);
					} else {
						marker.setLatLng(positionLatLon);
					}

					follow = shouldShow && TeamFollower.isFollowed(teamId);
				}
			}

			if (wasOnMap && !shouldShow) { // was on map but got deactivated
				me.markerDisplacer.removeMarker(marker);
				layer.removeLayer(marker);
			} else if (!wasOnMap && shouldShow) { // was NOT on map, but got activated
				layer.addLayer(marker);
				me.markerDisplacer.addMarker(marker);

				if (this.focusedTeam === teamId) {
					me.applySelectedDecoration(teamId, this.focusedTeam === teamId);
					me.mapContainer.fireEvent('teamSelected', teamId);
				}
			} else if (wasOnMap && shouldShow) { // was on map, and should stay on it (redundant condition left to tell it explicitly)
				if (modifiedFields.some(f => f === 'Status')) {
					me.applyStatusDecoration(marker, teamId);
					me.applyStatusDecoration(marker.getTooltip(), teamId);
				}
			}
		}

		if (follow) {
			marker.bringToFront();
			marker.getTooltip().bringToFront();
			me.mapContainer.fireEvent('teamSelected', teamId);
		}
	},

	shouldBeShown: function (position) {
		return position.isActive() && !this.isFilteredOut(position.get('TeamId'));
	},

	isFilteredOut: function (teamId) {
		var teams = Ext.getStore('Teams');

		// store is filtered, findExact cannot find it as it searches through
		// filtered data but team can retrieved byId as it searches through entire data set
		return (teams.isFiltered() && teams.findExact('TeamId', teamId) === -1 && teams.getById(teamId) !== null);
	},

	areLocationFields: function (filedName) {
		return filedName === 'Longitude' || filedName === 'Latitude';
	},

	positionsLoaded: function (store, records, successfull) {
		// FIXME: lan, lot here is abuse, try to frix it on level of PositionReceiver
		var fields = ['Longitude', 'Latitude'];
		Ext.each(records, function (position) {
			this.positionUpdated(store, position, null, fields)
		}, this)
	},

	applyStatusDecoration: function (layer, teamId) {
		var el = !layer._path ? layer._container : layer._path;

		if (el) {
			var position = Ext.getStore('Positions').getById(teamId),
				lostSignal = position.get('Status') === 8;

			el.classList.toggle('no-signal', lostSignal);
		}
	},

	applySelectedDecoration: function (teamId, selected) {
		const layer = this.markers[teamId];
		if (layer) {
			layer.setScale(selected ? 1.5 : 1);

			if (selected) {
				layer.bringToFront();
				layer.getTooltip().bringToFront();
			}
		}
	},

	applyFollowDecoration: function (team) {
		const layer = this.markers[team.getId()];
		const el = !layer._path ? layer._container : layer._path;

		el && el.classList.toggle('followed', team.get('IsFollowed'));
	},

	teamsUpdated: function (store, team, operation, modifiedFieldNames) {
		if (modifiedFieldNames.indexOf('Enabled') > -1) {
			this.toggleTeam(team);
			const teamId = team.getId();
			this.applySelectedDecoration(teamId, this.focusedTeam === teamId);
		}

		if (modifiedFieldNames.indexOf('IsFollowed') > -1) {
			this.applyFollowDecoration(team);
		}
	},

	teamsVisibilityChanged: function (teamsStore, showTeamsFromStore) {
		this.layer.clearLayers();

		if (showTeamsFromStore) {
			teamsStore.each(function (team) {
				this.toggleTeam(team);
			}, this);
		}
	},

	toggleTeam: function (team) {
		var me = this,
			teamId = team.getId(),
			teamMarker = me.markers[teamId],
			position = Ext.getStore('Positions').getById(teamId);

		if (!teamMarker || !position) return;

		if (me.shouldBeShown(position)) {
			me.layer.addLayer(teamMarker);
			me.markerDisplacer.addMarker(teamMarker);
		} else {
			me.markerDisplacer.removeMarker(teamMarker);
			me.layer.removeLayer(teamMarker);
		}
	}
});