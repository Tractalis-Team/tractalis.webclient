/**
 * View representing the Metro Navigation Container on Top.
 */
Ext.define('TractalisWebclient.view.main.components.headerbar.MetroNavigationContainer',{
    extend: 'Ext.panel.Panel',
    alias: 'widget.header-metro-navigation-container',
    controller: 'main-components-headerbar-metronavigationcontainer',
    viewModel: {
        type: 'main-components-headerbar-metronavigationcontainer'
    },
    id: 'headerMetroNavigationContainer',
    
    requires: [
        'TractalisWebclient.view.main.components.headerbar.MetroNavigationContainerController',
        'TractalisWebclient.view.main.components.headerbar.MetroNavigationContainerModel'
    ],

    config: {
        height: 170
    },

    defaults: {
        xtype: 'metro-button',
        margin: '20 0 20 20',
        listeners: {
            /**
             * is called when button is clicked
             * 
             * @param button the component instance
             * @param event instance
             */
            click: function(button, event){
                Ext.getCmp('headerMetroNavigationContainer').fireEvent('buttonClicked', button.getId());
                
                // stop event from bubbling
                event.stopEvent();
            }
        }
    },
    
    /**
     * Add all Menu Items here!
     */
    items: [
        {
            cls: 'metro-button red',
            id: 'contact',
            icon: 'resources/images/icons/Contact-128.png'
        },
        {
            cls: 'metro-button green',
            icon: 'resources/images/icons/Contact-128.png'
        },
        {
            cls: 'metro-button blue',
            icon: 'resources/images/icons/Contact-128.png'
        }
    ],
    
    /**
     * Initializes the component
     */
    initComponent: function(){
        var me = this;
        
        me.callParent(arguments);
    }
});
