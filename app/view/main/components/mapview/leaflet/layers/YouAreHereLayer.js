Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.YouAreHereLayer", {
    alternateClassName: 'YouAreHereLayer',

    map: null,
    mapContainer: null,
    marker: null,
    watchId: null,
    geolocationOptions: null,
    panOptions: { animate: true },
    follow: false,

    config: {
        markerOptions: {
            className: 'tractalis-you-are-here-marker',
            color: '#ffffff',
            opacity: 1,
            fillColor: '#D50000', // '#D50000',
            fillOpacity: 0.7,
            radius: 8,
            weight: 2,
            interactive: false
        }
    },

    constructor: function (options) {
        this.initConfig();

        this.mapContainer = options.mapContainer;
        this.map = options.map;
        this.marker = L.circleMarker(options.map.getCenter(), this.getMarkerOptions());
    },

    setUpLayer: function () {
        return new Promise(function (resolve, reject) {
            if ("geolocation" in navigator) {
                this.geolocationOptions = {
                    enableHighAccuracy: true
                };

                this.mapContainer.on('showYouAreHere', this.show, this, { buffer: 500 });
                this.mapContainer.on('hideYouAreHere', this.hide, this);

                resolve(true);
            } else {
                reject("geolocation IS NOT available")
            }
        }.bind(this));
    },

    show: function (follow) {
        this.follow = follow;
        if (this.watchId === null) {
            this.watchId = navigator.geolocation.watchPosition(this.update.bind(this), this.handlePosUpdateErr.bind(this, false), this.geolocationOptions);
        }
    },

    update: function (pos) {
        if (!this.map.hasLayer(this.marker)) {
            this.map.addLayer(this.marker);
        }
        const coords = L.latLng(pos.coords.latitude, pos.coords.longitude);
        this.marker.setLatLng(coords);
        this.follow && this.map.panTo(coords, this.panOptions);
    },

    handlePosUpdateErr: function (err) {
        console && console.error('Goelocation scored hiccup...', JSON.stringify(err));
        if (err.code !== 3) {
            Ext.GlobalEvents.fireEvent('gelocation-failed');
            this.hide();
        }
    },

    hide: function () {
        if (this.watchId !== null) {
            navigator.geolocation.clearWatch(this.watchId);
            this.watchId = null;
        }

        if (this.map.hasLayer(this.marker)) {
            this.map.removeLayer(this.marker);
        }
    }
});