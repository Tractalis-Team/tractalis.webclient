 /**
  * Helper for Units
  * @type {Boolean}
  */
 Ext.define('TractalisWebclient.helper.UnitHelper', {
 	singleton: true,
 	alternateClassName: 'UnitHelper',

 	config: {
 		/**
 		 * @cfg {Boolean} isMetric
 		 * Specifies whether the client instance is using a metric system
 		 */
 		isMetric: Configuration.get('race/isMetric'),

 		/**
 		 * @cfg {String} meterSuffix
 		 * The suffix which is added for meters
 		 */
 		meterSuffix: 'm',

 		/**
 		 * @cfg {String} kilometerSuffix
 		 * The suffix which is added for kilometers
 		 */
 		kilometerSuffix: 'km',

 		/**
 		 * @cfg {String} speedFormat
 		 * The unit which is used to specify speed.
 		 */
 		speedFormat: 'km/h'
 	},

 	/**
 	 * Constructs the instance
 	 */
 	constructor: function() {
 		var me = this;

 		// create the automatic properties
 		me.initConfig(me.config);
 	},

 	/**
 	 * @private
 	 * Updates metric value
 	 * @param {Boolean} isMetric
 	 */
 	updateIsMetric: function(isMetric) {
 		var me = this;

 		if (isMetric) {
 			me.setMeterSuffix('m');
 			me.setKilometerSuffix('km');
 			me.setSpeedFormat('km/h')
 		} else {
 			me.setMeterSuffix('ft');
 			me.setKilometerSuffix('mi.');
 			me.setSpeedFormat('mp/h')
 		}
 	},

 	/**
 	 * Fetch meters or feet.
 	 *
 	 * @param {Number} value
 	 * @return {Number} the actual replacement value
 	 */
 	meters: function(value) {
 		if (!this.getIsMetric()) {
 			return value / 0.3048;
 		}

 		return value;
 	},

 	/**
 	 * Fetch kilometers or miles.
 	 *
 	 * @param {Number} value
 	 * @return {Number} the actual replacement value
 	 */
 	kilometers: function(value) {
 		// is imperial system?
 		if (!this.getIsMetric()) {
 			return value / 1.609;
 		}

 		return value;
 	}
 });