/**
 * Leaflet Map Container which manages the whole Main GIS Functionallity.
 */
Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.MapContainer", {
	extend: "Ext.panel.Panel",
	alias: 'widget.L-map-container',
	controller: "L-map-container",
	viewModel: {
		type: "L-map-container"
	},

	requires: [
		'TractalisWebclient.view.main.components.mapview.leaflet.MapContainerController',
		'TractalisWebclient.view.main.components.mapview.leaflet.MapContainerModel',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.BaseLayersCreator',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.PositionsLayerController',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.FixPointsCreator',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.HistoricalPositionsLayer',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.GhostLayer',
		'TractalisWebclient.view.main.components.mapview.leaflet.layers.YouAreHereLayer'
	],

	config: {
		/**
		 * @cfg {L.map} map
		 */
		map: null,

		/**
		 * @cfg {L.control.layers} layers selector control
		 */
		layersControl: null,

		/**
		 * @cfg {L.featureGroup} race track layer
		 */
		trackLayer: null,

		/**
		 * @cfg {L.featureGroup} fixpoints layer
		 */
		fixpointsLayer: null,

		/**
		 * @cfg {L.featureGroup} positions layer
		 */
		positionsLayer: null,

		/**
		 * @cfg {L.featureGroup} historical positions layer
		 */
		positionHistoryLayer: null,


		/**
		 * @cfg {String} html
		 * The div component which will contain the map lateron
		 */
		html: '<div id="mapDomContainer" style="width: 100%; height: 100%;"></div>'
	},

	/**
	 * Initializes the component
	 */
	initComponent: function() {
		var me = this;

		me.callParent(arguments);

		this.enableBubble([ 'teamDetailsRequested' ]);

		// show a loading message when dom is ready
		me.on('render', me.showLoading, me);
		me.on('afterrender', me.generateMap.bind(me));
		me.on('mapReady', me.hideLoading, me, {
			single: true,
			priority: 100
		});
	},

	/**
	 *
	 */
	showLoading: function() {
		this.setLoading('Loading Map...');
	},

	/**
	 *
	 */
	hideLoading: function() {
		this.setLoading(false);
	},

	/**
	 * TODO: write it down
	 */
	generateMap: function() {
		var me = this,
			initialView = Configuration.get('gis/initial'),
			map = L.map('mapDomContainer').setView([initialView.position.lat, initialView.position.lng], initialView.zoom),
			layersControl = L.control.layers().addTo(map);

		me.setMap(map);

		me.setLayersControl(layersControl);
		BaseLayersCreator.setUpBaseLayers(map, layersControl);
		me.generateOverlays(map, layersControl);

		map.whenReady(function() {
			me.fireEvent('mapSetUp', map);
		});

		map.on('layerLoadStart', me.showLoading, me);
		map.on('layerLoadEnd', me.hideLoading, me);
	},

	/**
	 * Generate all layers
	 *
	 * @param map
	 * @param layersControl
	 */
	generateOverlays: function(map, layersControl) {
		var me = this,
			positionsLayer, fixpointsLayer, positionHistoryLayer;

		// positions
		positionsLayer = L.layerGroup().addTo(map);

		me.setPositionsLayer(positionsLayer);
		layersControl.addOverlay(positionsLayer, "Positions");

		// fixpoints
		fixpointsLayer = L.layerGroup().addTo(map);

		me.setFixpointsLayer(fixpointsLayer);
		layersControl.addOverlay(fixpointsLayer, "Fixpoints");

		// historical positions
		// TODO: consider featureGroup instead as it emits events when children are added/removed
		positionHistoryLayer = L.layerGroup().addTo(map);

		me.setPositionHistoryLayer(positionHistoryLayer);
		layersControl.addOverlay(positionHistoryLayer, "Position History");
	},

	/**
	 * Handles resize event of the container. Tells Leaflet map to adjust itself.
	 *
	 * @param {Number} w new width
	 * @param {Number} h new height
	 * @param {Number} oW old width
	 * @param {Number} oH old height
	 */
	onResize: function(w, h, oW, oH) {
		this.callParent(arguments);
		var map = this.getMap();
		if (map) {
			map.invalidateSize();
		}
	}
});
