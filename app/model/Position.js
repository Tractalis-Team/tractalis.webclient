/**
 * This Model represents single Team Position Data Entries which are rendered in the map.
 */
Ext.define('TractalisWebclient.model.Position', {
	extend: 'Ext.data.Model',

	idProperty: 'TeamId',

	fields: [{
		name: 'TeamId',
		type: 'auto',
		mapping: 'TrackableObjectId'
	}, {
		name: 'Longitude',
		type: 'number',
		mapping: 'CorrectedPositionLon'
	}, {
		name: 'Latitude',
		type: 'number',
		mapping: 'CorrectedPositionLat'
	}, {
		name: 'Altitude',
		type: 'number',
		mapping: 'CorrectedPositionAlt'
	}, {
		name: 'team',
		type: 'auto',
		defaultValue: null
	}, {
		name: 'Rank',
		type: 'int',
		defaultValue: Number.MAX_VALUE,
		convert: function(value, record) {
			return value || Number.MAX_VALUE;
		}
	}],

	/**
	 * Checks whether the position entity is in active state
	 *
	 * @returns {Boolean}
	*/
	isActiveState: function(){
		var status = this.get('Status');

		// only Running and OffRoute... well NoSgnal as well
		return status === 5 || status === 7 || status === 8;
	},

	/**
	 * Checks whether this position entity is active. The check is done based on the status AND the enabled state of the linked
	 * corresponding team record.
	 *
	 * @returns {Boolean}
	 */
	isActive: function() {
		if (typeof window.disableStatusCheck !== 'undefined') return true;

		// we not already based on state that this position is not active
		// no further checks are nessecary, it cannot get active anymore from that point
		// because all conditions have to be TRUE
		if (!this.isActiveState()) return false;

		// when active, try to check also the status of the team
		var team = this.get('team');

		// when team could not be joined, we can agree it is true because the state was proper
		// when team could be joined and the enabled state is true, it resolves to true
		return team === null || team.get('Enabled');
	}
});
