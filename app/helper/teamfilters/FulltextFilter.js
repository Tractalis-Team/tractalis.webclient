/**
 * This class is used as Base-Class for each Command which should be able to synchronize via
 * the Global Message Queue across different instances of the client.
 */
Ext.define('TractalisWebclient.helper.teamfilters.FulltextFilter', {
	extend: 'StoreFilter',
	alternateClassName: 'FulltextFilter',

	config: {
		/**
		 * @cfg {String} search
		 * Goes through all teams and searches through them
		 */
		search: "",

		storeName: "Teams",

		fnc: function(record) {
			var value = this.getSearch();

			// perform the initial filtering also
			return (record.get('Name').toLowerCase().indexOf(value) > -1
				|| record.get('StartNumber').toLowerCase().indexOf(value) > -1
				|| record.get('CategoryName').toLowerCase().indexOf(value) > -1
				|| record.get('memberNames').indexOf(value) > -1);
		}
	},

	restore: function() {
		// send a message through the global bus
		Ext.GlobalEvents.fireEvent('restoreFulltextFilter', this.getSearch());
	},

	serialize: function() {
		var me = this,
			serialized = me.callParent();

		serialized.search = me.getSearch();

		return serialized;
	}
});