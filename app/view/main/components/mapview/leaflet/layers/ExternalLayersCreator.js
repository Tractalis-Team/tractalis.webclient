Ext.define("TractalisWebclient.view.main.components.mapview.leaflet.layers.ExternalLayersCreator", {
    alternateClassName: 'ExternalLayersCreator',

    config: {
        layerOptions: {
            /**
             * Function converting feature to style.
             */
            style: function(feature) {
                var style = {},
                    props = feature.properties;

                if (props) {
                    if (props['stroke']) style.color = props['stroke'];
                    if (props['stroke-width']) style.weight = props['stroke-width'];
                    if (props['stroke-opacity']) style.opacity = props['stroke-opacity'];
                }
                return style;
            }
        },
        popupOptions: {
            className: 'tractalis-kml-layer-popup'
        }
    },

    SUPPORTED_TYPES: ['csv', 'kml', 'gpx', 'wkt', 'topojson', 'polyline', 'geojson'],

    _map: null,
    _layersControl: null,
    _isInHqMode: false,
    _hqModeBinding: null,
    _layers: {},

    constructor: function(map, layersControl, mapViewModel) {
        this.initConfig(this.config);

        this._map = map;
        this._layersControl = layersControl;
        this._isInHqMode = mapViewModel.get('hqMode');

        this._hqModeBinding = mapViewModel.bind('{hqMode}', this.onHqModeChange, this);
    },

    destroy: function() {
        this._hqModeBinding.destroy();
    },

    onHqModeChange: function(isInHqMode) {
        var me = this,
            hqModeLayers, len, i;

        me._isInHqMode = isInHqMode;

        if (!Configuration.get('gis/enableExternalLayers', false)) {
            return Promise.resolve(true);
        }

        hqModeLayers = Configuration.get('gis/layers').filter(function(cfg) {
            return cfg.only4Hq;
        });

        if (isInHqMode) {
            // we are adding layers

            // show loading
            Promise.all(hqModeLayers.map(me.addLayer, me));
            // .then done loading

        } else {
            // TODO: do i have to release somehow ducuments which were downloaded or gc will do its job?
            len = hqModeLayers.length;
            for (i = 0; i < len; i++) {
                me.removeLayer(hqModeLayers[i]);
            }
        }
    },

    setUpLayers: function() {
        var me = this,
            kmlLayers;

        if (!Configuration.get('gis/enableExternalLayers', false)) {
            return Promise.resolve(true);
        }

        kmlLayers = Configuration.get('gis/layers').filter(function(cfg) {
            return (me._isInHqMode) ? true : !cfg.only4Hq;
        });

        return Promise.all(kmlLayers.map(me.addLayer, me));
    },

    addLayer: function(cfg) {
        var me = this,
            geoJsonLayer, promise;

        if (me._layers[cfg.name] !== undefined) {
            return Promise.resolve(true);
        }

        geoJsonLayer = L.geoJson(null, me.getLayerOptions())
            .bindPopup(me.layerToPopupText, me.getPopupOptions());

        if (!cfg.only4Hq) {
            promise = me.loadLayer(cfg, geoJsonLayer);
            geoJsonLayer.addTo(me._map); // add to make it out-of-the-box visible
        } else {
            // on 1st add => 1st select from picker
            geoJsonLayer.once('add', function(){
                me._map.fire('layerLoadStart', { name: cfg.name }, true);
                me.loadLayer(cfg, geoJsonLayer)
                    .then(function() {
                        me._map.fire('layerLoadEnd', { success: true, name: cfg.name }, true);
                    })
                    .catch(function(reason) {
                        me._map.fire('layerLoadEnd', { success: false, name: cfg.name, msg: reason }, true);
                    });
            });
            promise = Promise.resolve(true);
        }

        me._layers[cfg.name] = geoJsonLayer;
        me._layersControl.addOverlay(geoJsonLayer, cfg.name);

        return promise;
    },

    loadLayer: function(cfg, layer) {
        var me = this,
            type = me.getType(cfg);

        return new Promise(function(resolve, reject) {
            omnivore[type](cfg.url, null, layer)
                .on('ready', function() {
                    resolve(true);
                })
                .on('error', function() {
                    reject("Cannot load preconfigured layer '" + cfg.name + "' from URL: " + cfg.url);
                })
        });
    },

    removeLayer: function(cfg) {
        var me = this,
            layer = me._layers[cfg.name];

        if (layer !== undefined) {
            me._layersControl.removeLayer(layer);
            layer.removeFrom(me._map);
            delete me._layers[cfg.name];
        }
    },

    getType: function(cfg) {
        var type = cfg.type;

        if (type) {
            type = type.toLowerCase();
            if (this.SUPPORTED_TYPES.indexOf(type) < 0) {
                return 'kml'; // best guess, before we started with different formats we had KML only
            }
        }

        return type;
    },

    layerToPopupText: function(layer) {
        return '<strong>' + layer.feature.properties.name + '</strong>';
    }
});
