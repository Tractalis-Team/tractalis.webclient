Ext.define("TractalisWebclient.view.main.components.socialmedia.twitter.TwitterPanel", {
	extend: "Ext.panel.Panel",
	alias: 'widget.twitter',

	id: 'twitter-panel',

	config: {
		widgetId: 0,
		showWidgetHeader: false,
		
		cls: 'twitter-panel',
		html: '<div class="tweetsContainer"></div>'
	},

	initComponent: function () {
		var me = this;

		me.callParent(arguments);

		me.loadTwitterJsApi()
			.then(me.generateContent.bind(me))
			.catch(me.onError.bind(me));
	},

	onError: function (msg) { // well... shiet... tweets won't load
		this.getEl()
			.selectNode('.tweetsContainer')
			.innerHTML = '<div class="load-error"><h3>Whoooops...</h3><p>Error occured while loading Twitter widget</p><p><small>If tracking protection is enabled in your browser, disable it to load social media streams as it blocks them.</small></p></div>';
	},

	loadTwitterJsApi: function () {
		return new Promise(function (resolve, reject) {
			window.twttr = (function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0],
					t = window.twttr || {};
				if (d.getElementById(id)) return t;
				js = d.createElement(s);
				js.id = id;
				js.src = 'https://platform.twitter.com/widgets.js';
				fjs.parentNode.insertBefore(js, fjs);

				js.onerror = function (evt) {
					reject(evt.message);
				}

				t._e = [];
				t.ready = function (f) {
					t._e.push(f);
				};

				return t;
			}(document, 'script', 'twitter-wjs'));

			window.twttr.ready(function (t) {
				resolve(true);
			});
		});
	},

	generateContent: function () {
		var me = this;

		me.twttrTimeline = twttr.widgets.createTimeline({
				sourceType: 'widget',
				widgetId: me.getWidgetId()
			},
			me.getEl().dom.querySelector('.tweetsContainer'), {
				chrome: me.getShowWidgetHeader() ? undefined : 'noheader',
				height: me.getHeight(),
				width: me.getWidth()
			});
	},


	/**
	 * Handles resize event of the container.
	 *
	 * @param {Number} w new width
	 * @param {Number} h new height
	 * @param {Number} oW old width
	 * @param {Number} oH old height
	 */
	onResize: function (w, h, oW, oH) {
		this.callParent(arguments);

		var frame = this.getEl().dom.querySelector('.tweetsContainer>iframe');
		if (frame) {
			frame.style.height = '' + h + 'px'
			frame.style.width = '' + w + 'px'
		}
	}
});