/**
 * This filter component is used as global application-wide search.
 */
Ext.define("TractalisWebclient.view.main.components.teams.filter.TeamFilter", {
	extend: "Ext.panel.Panel",

	alias: "widget.team-filter",
	requires: [
		'TractalisWebclient.view.main.components.teams.filter.TeamFilterController',
		'TractalisWebclient.view.main.components.teams.filter.TeamFilterModel'
	],
	controller: "teamfilter",

	viewModel: {
		type: "teamfilter"
	},


	config: {
		/**
		 * @cfg {Ext.data.Store} teamStore
		 *
		 * Store which represents the teams
		 */
		teamStore: null,

		/**
		 * @cfg {Ext.data.Store} teamCategoryStore
		 *
		 * Store which represents the team categories
		 */
		teamCategoryStore: null,

		layout: {
			type: 'vbox'
		}
	},

	/**
	 * Attaches all the event handlers which are used across this textfield
	 */
	initComponent: function () {
		var me = this;

		me.callParent();

		me.add([
			me.getTopContainer(),
			me.getBottomContainer()
		]);

		Ext.getCmp('fulltextElement').on('change', me.onFullTextFilterValueChange, me, {
			buffer: 300
		});

		me.setTeamCategoryStore(Ext.getStore('TeamCategories'));
		me.setTeamStore(Ext.getStore('Teams'));
	},

	/**
	 * Retrieves the top header which contain all the filtering settings
	 */
	getTopContainer: function () {
		var me = this,
			components = [
				me.getFulltextElement()
			];

		if (Configuration.get('race/includeRanks')) {
			components.push(me.getTop5Element());
		}

		components.push(
			me.getCategoryFilterElement(),
			me.getClearFilterElement()
		);

		return {
			xtype: 'panel',
			width: '100%',
			flex: 1,
			layout: {
				type: 'hbox'
			},
			items: components
		};
	},

	/**
	 * Gets the configuration of the bottom part of the whole filter facility
	 */
	getBottomContainer: function () {
		var me = this;

		return {
			xtype: 'panel',
			width: '100%',
			flex: 1,
			layout: {
				type: 'hbox'
			},
			items: [
				me.getTeamToggleAll(),
				{
					xtype: 'cycle',
					iconCls: 'fa fa-crosshairs',
					componentCls: 'geolocation-button',
					iconAlign: 'right',
					text: 'My Position:',
					menu: {
						componentCls: 'geolocation-item',
						items: [{
								text: 'Hide',
								checked: true,
								iconCls: 'fa fa-ban ',
								run: function () {
									Ext.GlobalEvents.fireEvent('hideMyPosition');
								},

								bind: {
									checked: '{geolocationFailed}'
								}
							},
							{
								text: 'Show & Follow',
								iconCls: 'fa fa-eye',
								run: function () {
									Ext.GlobalEvents.fireEvent('gelocation-failed-reset');
									Ext.GlobalEvents.fireEvent('showMyPosition', true);
								}
							},
							{
								text: 'Show',
								iconCls: 'fa fa-crosshairs',
								run: function () {
									Ext.GlobalEvents.fireEvent('gelocation-failed-reset');
									Ext.GlobalEvents.fireEvent('showMyPosition', false);
								}
							}
						]
					},
					changeHandler: function (cycleBtn, activeItem) {
						activeItem.run();
					},
					bind: {
						disabled: '{!geolocationAvailable}'
					}

				}
			]
		};
	},

	/**
	 * Retrieves the configuration of the toggle all teams-checkbox filtering stuff
	 */
	getTeamToggleAll: function () {
		return {
			xtype: 'checkbox',
			flex: 7,
			id: 'teamToggleButton',
			fieldLabel: 'Toggle all teams',
			padding: '0 10 0 10',
			checked: true,
			cls: 'team-toggle-all',
			scope: this,

			handler: function (checkbox, checked) {
				this.fireEvent('toggleAllTeams', checked);
			}
		};
	},

	/**
	 * Retrieves the configuration of the fulltext-search element which is basically a textfield
	 *
	 * @return {Object} configuration
	 */
	getFulltextElement: function () {
		return {
			id: 'fulltextElement',
			xtype: 'textfield',
			flex: 7,
			emptyText: 'Search ..',
			enableKeyEvents: true
		};
	},

	/**
	 * Gets a button-configuration which triggers the Top5-Filter
	 * @returns {Object} Configuration for a button
	 */
	getTop5Element: function () {
		return {
			scope: this,
			xtype: 'button',
			id: 'top5Button',
			text: 'Top 5',
			flex: 3,
			enableToggle: true,
			listeners: {
				toggle: {
					fn: function (button, toggled) {
						this.fireEvent('top5', toggled);
					},
					scope: this
				}
			}
		};
	},

	/**
	 * Gets the element configuration
	 *
	 * @return {Object} element configuration
	 */
	getCategoryFilterElement: function () {
		return {
			scope: this,
			xtype: 'button',
			text: 'Cat.',
			flex: 3,
			scope: this,
			handler: function () {
				this.fireEvent('categoryFilter');
			}
		};
	},

	/**
	 * Gets the element configuration
	 *
	 * @return {Object} element configuration
	 */
	getClearFilterElement: function () {
		return {
			scope: this,
			xtype: 'button',
			text: 'clear',
			flex: 3,
			scope: this,
			handler: function () {
				Ext.getCmp('fulltextElement').setValue('');

				var top5Button = Ext.getCmp('top5Button');
				if (top5Button) {
					top5Button.setPressed(false);
				}

				this.fireEvent('clearAll');
			}
		};

	},

	onFullTextFilterValueChange: function (filed, newVal, oldVal) {
		var me = this;

		if (Ext.isEmpty(newVal)) {
			me.fireEvent('clearFulltext');
		} else {
			me.fireEvent('fulltext', newVal);
		}
	}


});