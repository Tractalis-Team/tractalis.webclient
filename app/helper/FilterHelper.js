/**
 * This filter provides various functionallities for filtering
 */
Ext.define('TractalisWebclient.helper.FilterHelper', {
	singleton: true,
	alternateClassName: 'FilterHelper',

	config: {
		/**
		 * @cfg {Object} filters
		 * List of all applied filters as associative array
		 */
		filters: {}
	},

	lsFilterKey: 'filters',

	mixins: {
		observable: 'Ext.util.Observable'
	},

	/**
	 * Constructs the instance of the Versioning Helper
	 *
	 * @returns {undefined}
	 */
	constructor: function () {
		var me = this;

		me.mixins.observable.constructor.call(me);

		// create the automatic properties
		me.initConfig(me.config);

		me.lsFilterKey = 'filters_' + Configuration.get('race/id');
	},

	/**
	 * Gets filter by id from pool
	 *
	 * @param {String} filterId The ID of the filter
	 *
	 * @return {TractalisWebclient.helper.teamfilters.BaseFilter} filter
	 */
	getFilter: function (filterId) {
		return this.getFilters()[filterId];
	},

	/**
	 * Clears all filters from this list
	 */
	clearAll: function () {
		var me = this,
			filters = me.getFilters();

		for (var id in filters) {
			var filter = filters[id];

			filter.clear();
			me.removeFilter(filter);
		}

		me.sync();

		// FIXME: do this better once as this is hacky and relies on fact that load filters on clean localstorage will restore defaults properly
		if (Configuration.get('onStartup/disableAllTeams')) {
			me.loadFilters();
		}

		Ext.GlobalEvents.fireEvent('globalFilteringChanged', me.buildOdataFilter());
	},

	/**
	 * Applies the specified filter
	 *
	 * @param {TractalisWebclient.helper.teamfilters.BaseFilter} filter
	 */
	applyFilter: function (filter) {
		var me = this;

		if (!filter.getEnabled()) {
			filter.clear();
			me.removeFilter(filter);
		} else {
			me.addFilter(filter);
			filter.execute();
		}

		if (filter.toOdataFilter() != null) {
			Ext.GlobalEvents.fireEvent('globalFilteringChanged', me.buildOdataFilter());
		}
	},

	buildOdataFilter: function () {
		var filter = "",
			filters = this.getFilters();

		if (Object.getOwnPropertyNames(filters).length > 0) {
			for (var key in filters) {
				var odata = filters[key].getEnabled() ? filters[key].toOdataFilter() : null;

				if (odata) {
					if (!filter) {
						filter = odata
					} else {
						filter = '(' + filter + ' and ' + odata + ')';
					}
				}
			}
		}

		return filter;
	},

	/**
	 * Adds a specific filter to the list which contains all current running filters
	 *
	 * @param {TractalisWebclient.helper.teamfilters.BaseFilter} filter
	 */
	addFilter: function (filter) {
		var filters = this.getFilters();
		filters[filter.getId()] = filter;
	},

	/**
	 * Removes the specific filter from pool
	 *
	 * @param {TractalisWebclient.helper.teamfilters.BaseFilter} filter
	 */
	removeFilter: function (filter) {
		var filters = this.getFilters();

		delete filters[filter.getId()];
	},

	/**
	 * Persists the filters into storage
	 */
	sync: function () {
		var me = this,
			filters = me.getFilters(),
			serialized = {};

		if (Object.getOwnPropertyNames(filters).length > 0) {
			for (var key in filters) {
				var srlzd = filters[key].serialize();
				if (srlzd) {
					serialized[key] = srlzd;
				}
			}

			localStorage.setItem(me.lsFilterKey, JSON.stringify(serialized));
		} else {
			localStorage.removeItem(me.lsFilterKey);
		}
	},

	/**
	 * Load filters from persistance and put them into
	 */
	loadFilters: function () {
		var stored = localStorage.getItem(this.lsFilterKey),
			serialized = {};

		if (stored === null) {
			if (Configuration.get('onStartup/disableAllTeams')) {
				serialized = {
					'TractalisWebclient.helper.teamfilters.TeamEnabledFilter': {
						enabled: true,
						teams: [],
						positiveFiltering: true
					}
				}
			}
		} else {
			serialized = JSON.parse(stored);
		}

		for (var key in serialized) {
			var filter = Ext.create(key);

			// put all properties from serialized object
			// into the new created filter object
			filter.setConfig(serialized[key]);
			filter.setId(key);
			filter.restore();

			this.applyFilter(filter);
		}
	}
});