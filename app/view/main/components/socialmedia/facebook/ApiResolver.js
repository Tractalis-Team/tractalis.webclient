Ext.define('TractalisWebclient.view.main.components.socialmedia.facebook.ApiResolver', {
    singleton: true,
    alternateClassName: 'FacebookApiResolver',

    config: {
        appId: '1956363081301826',
        xfbml: true,
        version: 'v3.0'
    },

    resolveApiPromise: null,

    /**
     * Ctor.
     * @private
     */
    constructor: function () {
        var me = this;

        me.initConfig(me.config);
    },

    resoveApi: function () {
        var me = this;

        if (!me.resolveApiPromise) {
            me.resolveApiPromise = new Promise(function (resolve, reject) {
                window.fbAsyncInit = function () {
                    FB.init({
                        appId: me.getAppId(),
                        xfbml: me.getXfbml(),
                        version: me.getVersion()
                    });
                    FB.AppEvents.logPageView();

                    resolve();
                };

                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {
                        return;
                    }
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "https://connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);

                    js.onerror = function (evt) {
                        reject(evt.message);
                    }
                }(document, 'script', 'facebook-jssdk'));
            });
        }

        return me.resolveApiPromise;
    }
});